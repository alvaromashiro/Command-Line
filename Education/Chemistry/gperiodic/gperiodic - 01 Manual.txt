GPERIODIC(1)                               User manuals                              GPERIODIC(1)

NAME
       gperiodic - periodic table application

SYNOPSIS
       gperiodic [option(s)] [element_symbol | atomic_number]

DESCRIPTION
       gperiodic  is  a  small X/GTK+-based program which allows you to browse through a periodic
       table of chemical elements, and view somewhat detailed information on  each  of  the  ele‐
       ments.  118 elements are currently listed.

OPTIONS
       Zero options will start gperiodic.

       -h     Show the help screen.

       -c <Temp>
              Color the elements according to their phase at temperature <Temp> in Kelvins.

       -D     Dump the periodic table.

       -v     Be verbose.

       Possible arguments:

       element_symbol | atomic_number
              Display  information on the element with the given element symbol or atomic number.
              The output is UTF-8 encoded (see Debian bug #466231).

AUTHOR
       The program was originally written by Kyle R. Burton. The current author is  Jonas  Frantz
       <jonas.frantz@helsinki.fi>.

       This  manual  page  was  written  by  Sebastien Bacher <seb128@debian.org>, for the Debian
       GNU/Linux system (but may be used by others).

gperiodic                                 June 10, 2007                              GPERIODIC(1)
