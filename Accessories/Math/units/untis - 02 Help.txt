
Usage: units [options] ['from-unit' 'to-unit']

Options:
    -h, --help           show this help and exit
    -c, --check          check that all units reduce to primitive units
        --check-verbose  like --check, but lists units as they are checked
        --verbose-check    so you can find units that cause endless loops
    -d, --digits         show output to specified number of digits (default: 8)
    -e, --exponential    exponential format output
    -f, --file           specify a units data file (-f '' loads default file)
    -H, --history        specify readline history file (-H '' disables history)
    -L, --log            specify a file to log conversions
    -l, --locale         specify a desired locale
    -m, --minus          make - into a subtraction operator (default)
        --oldstar        use old '*' precedence, higher than '/'
        --newstar        use new '*' precedence, equal to '/'
    -n, --nolists        disable conversion to unit lists
    -S, --show-factor    show non-unity factor before 1|x in multi-unit output
    -o, --output-format  specify printf numeric output format (default: %.103)
    -p, --product        make '-' into a product operator
    -q, --quiet          suppress prompting
        --silent         same as --quiet
    -s, --strict         suppress reciprocal unit conversion (e.g. Hz<->s)
    -v, --verbose        show slightly more verbose output
        --compact        suppress printing of tab, '*', and '/' character
    -1, --one-line       suppress the second line of output
    -t, --terse          terse output (--strict --compact --quiet --one-line)
    -r, --round          round last element of unit list output to an integer
    -U, --unitsfile      show units data filename and exit
    -V, --version        show version, data filenames (with -t: version only)
    -I, --info           show version, files, and program properties
To learn about the available units look in '/usr/share/units/definitions.units'

Report bugs to adrianm@gnu.org.


