From: https://linuxconfig.org/how-to-easily-encrypt-any-file-or-directory-with-mcrypt-on-linux-system

Linux Tutorials - Learn Linux Configuration

How to easily encrypt any file or directory with Mcrypt on Linux System

Details
    admin
    Uncategorised
    16 January 2015

Contents

  • 
      □ 1. Mcrypt installation
      □ 2. Creating a testing sandbox
      □ 3. Basic file encryption and decryption
          ☆ 3.1. Encryption
          ☆ 3.2. Dencryption
      □ 4. Encryption with compression
      □ 5. Directory encryption with Mcrypt
      □ 6. Changing Mcrypt's encryption algorithm
      □ 7. Configuring mcrypt

In this config we will show you a number of examples how to use mcrypt tool to
easily encrypt files whether the file is large or small in size. We will also
use Mcrypt to encrypt and compress files and directories on the fly which can
be usefully for a number of backup and scripting purposes.

| Mcrypt installation
`--------------------

UBUNTU/DEBIAN
    # apt-get install mcrypt
REDHAT/FEDORA/CENTOS
    # yum install mcrypt

| Creating a testing sandbox
`---------------------------

Let's first create a directory with some files we can work with:

    $ mkdir dir1
    $ cd dir1/
    $ echo "My File to Encrypt" > file1
    $ cat file1
    My File to Encrypt
    $ fallocate -l 500MB file2
    $ md5sum file*
    bccd44aaa84c7c9d04a268f670ae92c5  file1
    4034379ecc54213fc9a51785a9d0e8e2  file2

With the above commands we have created a directory dir1. Within our directory
we have created two files file1 a simple text file and file2 of 500MB in size
and contains some random binary data. Next, we have generated md5sum for both
files so we can compare our files after decryption.

| Basic file encryption and decryption
`-------------------------------------

Encryption
----------

At this stage we can start with a simple file encryption and decryption
examples. The following linux command will encrypt file1 with a passphrase
entered by user during the mcrypt command execution:

    $ mcrypt file1
    Enter the passphrase (maximum of 512 characters)
    Please use a combination of upper and lower case letters and numbers.
    Enter passphrase:
    Enter passphrase:

    File file1 was encrypted.
    $ ls -l
    total 488292
    -rw-rw-r--. 1 lrendek lrendek        19 Jan 15 18:24 file1
    -rw-------. 1 lrendek lrendek       125 Jan 15 18:24 file1.nc
    -rw-r--r--. 1 lrendek lrendek 500000000 Jan 15 18:24 file2

The output of the above encryption Mcrypt command is file1.nc. To encrypt both
files at once we could supply both file names on the command line and enter
encryption passphrase for both files separately. Instead it is easier but less
secure to use the passphrase on the command line. Example:

    $ mcrypt file1 file2 -k abc123
    Warning: It is insecure to specify keywords in the command line
    File file1 was encrypted.
    File file2 was encrypted.

Both files have been encrypted with a passphrase abc123.

Dencryption
-----------

At this stage we can try to use Mcrypt's decompression facilities. Let's
decrypt our file1.nc:

    -rw-------. 1 lrendek lrendek 124 Jan 15 18:24 file1.nc

    mkdir dir2
    $ mv file*.nc dir2/
    $ cd dir2/
    $ ls
    file1.nc  file2.nc
    $ mcrypt -d file1.nc
    Enter passphrase:
    File file1.nc was decrypted.

The same way we can also decrypt both files at once:

    $ mcrypt -k abc123 -d file1.nc file2.nc
    Warning: It is insecure to specify keywords in the command line
    File file1.nc was decrypted.
    File file2.nc was decrypted.

and compare decrypted files with the previous md5sum output:

    $ md5sum file[1,2]
    bccd44aaa84c7c9d04a268f670ae92c5  file1
    4034379ecc54213fc9a51785a9d0e8e2  file2

Encryption with compression
---------------------------

Mcrypt also offers an option to compress files with gzip before the actual
compression takes place. Consider a following example:

    $ mcrypt -k abc123 -z file1
    Warning: It is insecure to specify keywords in the command line
    File file1 was encrypted.
    $ file file1.gz.nc
    file1.gz.nc: mcrypt 2.5 encrypted data, algorithm: rijndael-128, keysize: 32 bytes, mode: cbc,

In the above example the file file1 was compressed with gzip before it was
encrypted with mcrypt. To decrypt gzip compressed file we simply reverse the
procedure. First decrypt your file:

    $ mcrypt -k abc123 -d file1.gz.nc
    Warning: It is insecure to specify keywords in the command line
    File file1.gz.nc was decrypted.

and then decompress the output with gunzip:

    $ gunzip -v file1.gz
    file1.gz:       -10.5% -- replaced with file1

Once again to confirm a validity of the above procedure we use md5sum:

    $ md5sum file1
    bccd44aaa84c7c9d04a268f670ae92c5  file1

Directory encryption with Mcrypt

In order to encrypt directories with mcrypt we we first need to use tar on the
directory. The next command example will encrypt our entire initial directory
dir1:

    $ tar cz dir1/ | mcrypt -k abc123 > dir1.tar.gz.nc
    Warning: It is insecure to specify keywords in the command line
    Stdin was encrypted.
    $ file dir1.tar.gz.nc
    dir1.tar.gz.nc: mcrypt 2.5 encrypted data, algorithm: rijndael-128, keysize: 32 bytes, mode: cbc,

Let's create yet another directory called dir3 which we will use to decrypt the
above directory dir1 from file dir1.tar.gz.nc.

    $ mkdir dir3
    $ mv dir1.tar.gz.nc dir3/
    $ cd dir3/
    $ ls
    dir1.tar.gz.nc

As with the files we first need to decrypt our encrypted archive:

    $ mcrypt -k abc123 -d dir1.tar.gz.nc
    Warning: It is insecure to specify keywords in the command line
    File dir1.tar.gz.nc was decrypted.

Once the archive is decrypted we can decompress it with tar command:

    $ tar xzf dir1.tar.gz

and compare md5sum

    $ md5sum dir1/file[1,2]
    bccd44aaa84c7c9d04a268f670ae92c5  dir1/file1
    4034379ecc54213fc9a51785a9d0e8e2  dir1/file2

Changing Mcrypt's encryption algorithm
--------------------------------------

use the following linux command to list all encryption algorithms available to
your disposal:

    $ mcrypt --list-hash
    Supported Hash Algorithms:
    crc32
    md5
    sha1
    haval256
    ripemd160
    tiger
    gost
    crc32b
    haval224
    haval192
    haval160
    haval128
    tiger128
    tiger160
    md4
    sha256
    adler32
    sha224
    sha512
    sha384
    whirlpool
    ripemd128
    ripemd256
    ripemd320
    snefru128
    snefru256
    md2

To change an encryption algorithm is a quite easy task with mcrypt's -h option.
Simply choose one of the above listed algorithms and use -h to specify it on
the command line. For example the below algorithm will encrypt our file1 with
the whirlpool encryption algorithm:

    $ mcrypt -k abc123 -h whirlpool file1
    Warning: It is insecure to specify keywords in the command line
    File file1 was encrypted.

| Configuring mcrypt
`-------------------

It is also possible to create a configuration file so mcrypt's options can be
committed on the command line. This is a great feature especially for scripting
etc. For example we can create a config file with a default passphrase abc123 :

    $ echo "key abc123" > ~/.mcryptrc
    $ mcrypt file1
    Warning: It is insecure to specify keywords in the command line
    File file1 was encrypted.
    $ mcrypt -k abc123 -d file1.nc
    Warning: It is insecure to specify keywords in the command line
    File file1.nc was decrypted.
