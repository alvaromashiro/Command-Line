MCRYPT(1)                            General Commands Manual                            MCRYPT(1)

NAME
       crypt, mcrypt, mdecrypt - encrypt or decrypt files

SYNOPSIS
       mcrypt  [  -dLFubhvrzp  ]  [-a algorithm]  [-c config_file]  [-m mode]  [-s keysize]  [-o keymode]
       [-k key1 key2 ...]  [-f keyfile] [ filename ...  ]

       mdecrypt [ -LFusbhvzp ] [-a algorithm] [-c config_file]  [-m mode]  [-s keysize]  [-o key‐
       mode] [-k key1 key2 ...]  [-f keyfile] [ filename ...  ]

DESCRIPTION
       Mcrypt  is  a  simple  crypting  program,  a  replacement for the old unix crypt(1).  When
       encrypting or decrypting a file, a new file is created with the  extension  .nc  and  mode
       0600.  The new file keeps the modification date of the original.  The original file may be
       deleted by specifying the -u parameter.  If no files are specified, the standard input  is
       encrypted to the standard output.

       Mcrypt uses all the symmetric algorithms included in libmcrypt.

       crypt  is  a small shell wrapper around mcrypt to emulate the old unix crypt command.  For
       more information run crypt --help !

HINTS
       By default, mcrypt , when one of these algorithms is specified, prompts something like:
        Enter passphrase: ...
        You should then enter a passphrase long enough (512 characters is  the  maximum  length).
       Now  in  order  to encrypt the file, The passphrase is transformed using the specified (or
       the default) key generation algorithm, and a random salt. The produced value is then  used
       as the key, which is fed to the algorithm.

       Algorithm Vulnerability: Most algorithms today are designed to resist in specific attacks.
       None of them is proved not to be vulnerable to some kind of attack not as yet known.

       Compression: By compressing your data  before  encryption  you  gain  both  in  efficiency
       (faster  encryption)  and safety of your data (language redundancy is removed). A drawback
       is that most compression programs will add specific headers in the compressed  file,  thus
       making  known  plaintext  attacks easier.  Compression after encryption is useless and may
       result to compressed files with longer size than the original.

       Error Recovery: There is some error recovery in mcrypt.  If bytes are removed or lost from
       the file or stream in ECB, CBC and OFB modes, are impossible to recover, although CFB mode
       will recover. If some bytes are altered then a full block of plaintext is affected in  ECB
       mode,  two  blocks  in  CBC  and  CFB  modes, but only the corresponding byte in OFB mode.
       Mcrypt uses a 32 bit CRC to check for errors in the encrypted files.

       Extra security: For the very paranoid, if mcrypt is executed with superuser priviledges it
       ensures that no important data (keys etc.) are written to disk, as swap etc.  Keep in mind
       that mcrypt was not designed to be a setuid program, so you shouldn't make it one.

       Do not rely on the fact that  an  algorithm  has  a  large  key  size,  try  to  use  long
       passphrases and try to make them unpredictable.

       All the block algorithms above support these modes of encryption:

       ECB:  The  Electronic  CodeBook  mode. It is the simplest mode to use with a block cipher.
       Encrypts each block independently.

       CBC: The Cipher Block Chaining mode. It is better than ECB since the plaintext  is  XOR'ed
       with  the  previous  ciphertext.  A  random block is placed as the first block so the same
       block or messages always encrypt to something different. (This is the default mode)

       CFB: The Cipher-Feedback Mode (in 8bit). This is a self-synchronizing stream cipher imple‐
       mented from a block cipher.

       OFB:  The  Output-Feedback Mode (in 8bit). This is a synchronous stream cipher implemented
       from a block cipher. It is intended for use in noisy lines, because  corrupted  ciphertext
       blocks  do  not  corrupt  the  plaintext blocks that follow. Insecure when used to encrypt
       large amounts of data, so I recommend against using it.

       nOFB: The Output-Feedback Mode (in nbit). n Is the size of the  block  of  the  algorithm.
       This  is  a  synchronous stream cipher implemented from a block cipher. It is intended for
       use in noisy lines, because corrupted ciphertext  blocks  do  not  corrupt  the  plaintext
       blocks that follow.

       Encrypted files can be restored to their original form using mcrypt -d or mdecrypt

       mdecrypt  takes  a  list of files on its command line and creates a new file for each file
       whose name ends with .nc by removing the ".nc" or by adding ".dc" to the end of  the  file
       name if .nc is not in the encrypted file's name.

OPTIONS
       -F --force
              Force  output  on  standard  output  or input from stdin if that is a terminal.  By
              default mcrypt will not output encrypted data to terminal, nor read encrypted  data
              from it.

       -z --gzip
              Use  gzip  (if  it  exists in your system) to compress files before encryption.  If
              specified at decryption time it will decompress these files.

       -p --bzip2
              Use bzip2 (if it exists in your system) to compress files  before  encryption.   If
              specified at decryption time it will decompress these files.

        --openpgp-z INT
              This option will enable compression in OpenPGP (RFC2440) encrypted files.

       -d --decrypt
              Decrypt.

        --help
              Display a help screen and quit.

       -v --version
              Version. Display the version number and quit.

       -L --license
              Display the mcrypt's license and quit.

       -o --keymode MODE
              MODE  may be one of the keymodes listed by the --list-keymodes parameter.  It actu‐
              ally is the convertion to the key before it is fed to the algorithm.  It is  recom‐
              mended to leave it as is, if you do not know what it is.  However if you still want
              to use this option, you might want to use the 'hex' mode which allows you to  spec‐
              ify the key in hex (and no convertion will by applied).

       -h --hash HASH_ALGORITHM
              HASH_ALGORITHM  may  be  one of the algorithms listed by the --list-hash parameter.
              This is the digest that will be appended to the file to be encrypted, in  order  to
              detect file corruption. The default is the CRC32 checksum.

       -s --keysize SIZE
              SIZE  is  the  algorithm's  key  size in bytes (not the size of the passphrase). It
              defaults to the maximum key supported by the algorithm. The maximum  key  sizes  of
              the  algorithms  may  be  obtained by the --list parameter. It is safe not to touch
              this.

       -g --openpgp
              This option will make mcrypt to use the OpenPGP (RFC2440) file format for encrypted
              files. This will make files encrypted by mcrypt accessible from any OpenPGP compli‐
              ant application.

       -b --bare
              No important information like the algorithm, mode, the bit mode and  the  crc32  of
              the  original  file  are  written  in the encrypted file.  The security lies on the
              algorithm not on obscurity so this is NOT the default. This flag must also be spec‐
              ified  when  decrypting  a  bare  encrypted  file.  When the bare flag is specified
              decryption and encryption are faster. This may be  usefull  when  using  mcrypt  to
              encrypt a link or something like that.

       --flush
              Flushes the output (ciphertext or plaintext) immediately. Usefull if mcrypt is used
              with pipes.

       --time Prints some timing information (encryption speed etc.)

       --nodelete
              When this option is specified mcrypt does not  delete  the  output  file,  even  if
              decryption failed. This is usefull if you want to decrypt a corrupted file.

       -q --quiet
              Suppress some not critical warnings.

       -u --unlink
              Unlink  (delete)  the input file if the whole process of encryption/decryption suc‐
              ceeds. This is not the default in order to use an external program to remove sensi‐
              tive data.

        --list
              Lists all the algorithms currently supported.

        --list-keymodes
              Lists all the key modes currently supported.

        --list-hash
              Lists all the hash algorithms currently supported.

       -r --random
              Use  /dev/(s)random  instead of /dev/urandom. This may need some key input or mouse
              move to proceed.  If your system does not support /dev/random  or  /dev/urandom,  a
              random gatherer will be used.

       -k --key KEY1 KEY2 ...
              Enter  the  keyword(s) via the command line. The KEY(s) is/are then used as keyword
              instead of prompting for them. Keep in mind that someone may see  the  command  you
              are executing and so your keyword(s).

       -c --config FILE
              Use  the specified configuration file. The default is .mcryptrc in your home direc‐
              tory. The format of the configuration file is the same as the parameters. An  exam‐
              ple file is:
               algorithm safer+
               mode cbc
               key a_very_secret_one

       -f --keyfile FILE
              Enter  the  keyword(s) via a file. One keyword is read per line.  The first keyword
              read is used for the first file, the second for the second file etc.  If  the  key‐
              words  are  less  than the files then the last keyword is used for the remaining. A
              limitation is that you cannot use the NULL (\0) and the Newline (\n)  character  in
              the key.  A solution to this problem is to specify the keyword in hex mode.

       -m --mode MODE
              Mode  of  encryption and decryption. These modes are currently supported: ECB, CFB,
              OFB, nOFB, CBC and STREAM. CBC is the default. Unless the bare  flag  is  specified
              there  is  no  need  to  specify these modes for decryption.  For stream algorithms
              (like WAKE) mode should be STREAM.

       -a --algorithm ALGORITHM
              The algorithm used to encrypt and decrypt. Unless the bare flag is specified  there
              is no need to specify these for decryption.

              The algorithms currently supported are shown with the --list parameter.

EXAMPLES
       For  mcrypt to be compatible with the solaris des(1), the following parameters are needed:
       "mcrypt -a des --keymode pkdes --bare --noiv filename".

       For mcrypt to be compatible with the unix crypt(1), the following parameters  are  needed:
       "mcrypt -a enigma --keymode scrypt --bare filename".

       To  encrypt  a  file  using a stream algorithm (eg. Arcfour), the following parameters are
       needed: "mcrypt -a arcfour --mode stream filename".

ENVIRONMENT
       Mcrypt uses the following environment variables:

       MCRYPT_KEY: to specify the key

       MCRYPT_ALGO: to specify the algorithm

       MCRYPT_MODE: to specify the algorithm's mode

       MCRYPT_KEY_MODE: to specify the key mode

       You can use these instead of using the command line (which is  insecure),  but  note  that
       only one key should be used in MCRYPT_KEY.

SEE ALSO
       des_crypt(3)

DIAGNOSTICS
       Exit status is normally 0; if an error occurs, exit status is something other than 0.

       Usage:  mcrypt  [-dLFubhvrzp]  [-f  keyfile] [-k key1 key2 ...] [-m mode] [-o keymode] [-a
       algorithm] [-c config_file] [filename ...]

AUTHORS
       Version   2.6.0   Copyright    (C)    1998,1999,2000,2001,2002    Nikos    Mavroyanopoulos
       (nmav@gnutls.org).

       Thanks  to  all  the  people  who reported problems and suggested various improvements for
       mcrypt; who are too numerous to cite here.

local                                      03 May 2003                                  MCRYPT(1)
