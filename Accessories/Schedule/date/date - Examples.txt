#####[date - Examples]#####

The following should be self-explainatory:
-----------------------------------------

date -d 'tommorrow 2:30 pm'
date -d 'yesterday 2:30 pm'
date -d 'next week 2:30 pm'
date -d 'next month 2:30 pm'
date -d 'next year 2:30 pm'
date -d '3 days 2:30 pm'

And no, you don't need to specify the time if you don't want to:
---------------------------------------------------------------
date -d '3 hours'
date -d '3 minutes'
date -d '3 seconds'

Getting a little more complex...
--------------------------------
Example of current time: Wed Sep 11 12:12:39 EDT 2019

Then after using:
     date -d 'next week 3 days 3 hours'

...Output is: Sat Sep 21 15:12:34 EDT 2019
