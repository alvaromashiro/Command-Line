usage: calcurse [--daemon|-F|-G|-g|-i<file>|-Q|--status|-x[<format>]]
                [-c<file>] [-D<path>] [-h] [-q] [--read-only] [-v]
                [--filter-*] [--format-*]

Operations in non-interactive mode:
  -F, --filter  Read items, filter them, and write them back
  -G, --grep    Grep items from the data files
  -Q, --query   Print items in a given query range

Note that -F, -G and -Q can be combined with filter options --filter-*
and formatting options --format-*. Consult the man page for details.

Miscellaneous:
  -c, --calendar <file>   Specify the calendar data file to use
  --daemon                Run notification daemon in the background
  -D, --directory <path>  Specify the data directory to use
  -g, --gc                Run the garbage collector and exit
  -h, --help              Show this help text and exit
  -i, --import <file>     Import iCal data from a file
  -q, --quiet             Do not show system dialogs
  --read-only             Do not save configuration or data files
  --status                Display the status of running instances
  -v, --version           Show version information and exit
  -x, --export [<format>] Export items, valid formats: ical, pcal

For more information, type '?' from within calcurse, or read the manpage.
Submit feature requests and suggestions to <misc@calcurse.org>.
Submit bug reports to <bugs@calcurse.org>.
