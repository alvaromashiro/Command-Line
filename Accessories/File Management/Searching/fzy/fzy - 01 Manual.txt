FZY(1)                               General Commands Manual                               FZY(1)

NAME
       fzy - A fuzzy text selector menu for the terminal.

SYNOPSIS
       fzy [OPTION]...

DESCRIPTION
       fzy  is  a fuzzy text selector/file finder for the terminal using a search similar to that
       of TextMate or CmdT.

       fzy reads a list of newline-separated items from stdin to be displayed as a  menu  in  the
       terminal.  Upon pressing ENTER, the currently selected item is printed to stdout.

       Entering  text narrows the items using fuzzy matching. Results are sorted using heuristics
       for the best match.

OPTIONS
       -l, --lines=LINES
              How many lines of items to display. If unspecified, defaults to 10 lines.

       -p, --prompt=PROMPT
              Input prompt (default: '> ')

       -s, --show-scores
              Show the scores for each item.

       -t, --tty=TTY
              Use TTY instead of the default tty device (/dev/tty).

       -q, --query=QUERY
              Use QUERY as the initial search query.

       -e, --show-matches=QUERY
              Non-interactive mode. Print the matches in sorted order for QUERY to stdout.

       -h, --help
              Usage help.

       -v, --version
              Usage help.

KEYS
       ENTER  Print the selected item to stdout and exit

       Ctrl+c, Esc
              Exit with status 1, without making a selection.

       Up Arrow, Ctrl+p
              Select the previous item

       Down Arrow, Ctrl+n
              Select the next item

       Tab    Replace the current search string with the selected item

       Backspace, Ctrl+h
              Delete the character before the cursor

       Ctrl+w Delete the word before the cursor

       Ctrl+u Delete the entire line

USAGE EXAMPLES
       ls | fzy
              Present a menu of items in the current directory

       ls | fzy -l 25
              Same as above, but show 25 lines of items

       vi $(find -type f | fzy)
              List files under the current directory and open the one selected in vi.

       cd $(find -type d | fzy)
              Present all directories under current path, and change to the one selected.

       ps aux | fzy | awk '{ print $2 }' | xargs kill
              List running processes, kill the selected process

       git checkout $(git branch | cut -c 3- | fzy)
              Same as above, but switching git branches.

AUTHOR
       John Hawthorn <john.hawthorn@gmail.com>

fzy 1.0                                     2018-09-23                                     FZY(1)
