gitfm(1)                             General Commands Manual                             gitfm(1)

NAME
       gitfm - GNU Interactive Tools File Manager

SYNTAX
       gitfm [options] [path1] [path2]

DESCRIPTION
       gitfm  is a file system browser with some shell facilities which was designed to make your
       work easier and more efficient.

       The GNUIT package also contains gitps, an interactive process viewer/killer and gitview, a
       hex/ascii file viewer.

       More  extensive documentation on gitfm and the other gnuit tools is available in info for‐
       mat, try 'info gnuit'.

OPTIONS
       A summary of options is included below.  For a complete description, see the Info files.

       -h     Show summary of options.

       -v     Show version of program.

       -c     Use ANSI colours.

       -b     Don't use ANSI colours.

       -l     Don't use last screen character.

       -p     Output the final path on exit

NOTE
       The main configuration file of the GNUIT package is gnuitrc.TERM where TERM is  the  value
       of  the  environment variable 'TERM' e.g for Linux console your environment contains some‐
       thing like 'TERM=linux' so the configuration file name is  gnuitrc.linux.   You  can  (and
       should)  have  a configuration file for each terminal type you use.  If you customise git,
       the version of the config file in your home directory should have a leading ".", eg  .gnu‐
       itrc.linux.

FILE-SYSTEM BROWSER
       gitfm is made of two panels. The left one and the right one. Each one contains a file sys‐
       tem directory. You can browse the directory tree with the usual cursor keys  and  pressing
       ENTER  when you want to enter or leave a directory.  You may change the panels by pressing
       TAB.  See also BUILT-IN COMMANDS.

INPUT LINE
       Under the two panels there is a shell like input line which you can  use  to  type  normal
       shell  commands.  The  input line supports unlimited characters and keeps a history of the
       typed commands. gitfm uses the GNU history library for that.  See also BUILT-IN COMMANDS.

WARNINGS AND ERRORS REPORTING
       Under the input line there is a status bar. You can see there the status of the  currently
       executed command, the warnings and errors and you will be prompted if a decision has to be
       taken.

BUGS
       Please send bug reports to:
       gnuit-dev@gnu.org

SEE ALSO
       termcap(5) terminfo(5) sh(1) ps(1) mount(8)  umount(8)  gitps(1)  gitview(1)  gitaction(1)
       gitmount(1) gitkeys(1) gitrgrep(1) gitunpack(1)

AUTHORS
       Tudor Hulubei <tudor@cs.unh.edu>
       Andrei Pitis <pink@pub.ro>
       Ian Beckwith <ianb@erislabs.net> (Current maintainer)

                                                                                         gitfm(1)
