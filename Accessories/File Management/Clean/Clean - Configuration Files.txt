###################################
# Clean - Configuration Files.txt #
###################################

Most of your application's settings are stored inside of your Home folder's
hidden directory '~/.config.' If you are in X (GUI), you can toggle your
file manager's "show hidden files" setting by using 'CTRL+h.' If you are
in TTY/console, and are using a file manager such as 'mc' (Midnight Commander),
you will probably see hidden files by default.

Another directory in which you may find configuration files is inside the
Home folder's hidden directory '~./local/share/.' However, this directory
is usually meant for databases and logs. I'm only mentioning it as a
possibility because sometimes, though by poor design, those database files
(.db) may or may not contain settings, or at least me used to influence 
them in some way.

Sometimes an application will create its own hidden directory or settings
file inside of the Home folder itself. This is more common for terminal-
focused applications and settings since, though rare these days, not all
Linux/UNIX systems create nice "Documents," "Pictures," "Downloads," etc.
directories.

But where do these "personalized" settings actually originate from? Usually,
they come from the '/etc/' or '/usr/share/' directories. They are copied 
from there to your Home folder's hidden files or directories when you open 
an application for the first time and then all edits of that application's 
settings are kept in the Home folder. This means, though do so at your 
own risk, that you should be able to delete everything associated with an 
application that's giving you problems in the hidden Home folder directories, 
relaunch the  application, and it will copy everything from the defaults 
in '/etc/' or '/usr/share/' as if it was the first time you've ever ran it.
