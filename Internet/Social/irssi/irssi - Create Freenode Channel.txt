#####[ irssi - Create a Freenode Channel ]#####

The huge benefit of creating a channel on Freenode
is that you can essentially chat with a lot of
friends in a fairly safe place (SSL is port 6697)
just using an IRC client, to which I can't think
of a single operating system, including phones,
that don't have one and if not, there are web-based
clients available too.

The one huge downside is that unless you have a
registered NICK with Freenode, your IP address
is exposed to everyone. That doesn't mean it
is 100% cloaked when registered, but it will work
well enough against most eaves-dropping. I wish Freenode
had an easy way to use Tor and not freakout when you
try to use a "Tor box" as a go between your computer
and router, but they get spammed a whole bunch and 
don't charge anyone to use their service, so I sort 
of understand why.

1. Connect to freenode:
   -------------------
    
       /connect freenode
    
2. Create a topic channel:
   ----------------------
       
       /join #mychannel
    
         [or]
           
   Create an off-topic channel:
   ---------------------------
       
       /join ##mychannel
       
3. Register the channel with ChanServ bot:
   --------------------------------------
       
       /msg ChanServ register ##mychannel
    
    This prevents others from taking over the channel
