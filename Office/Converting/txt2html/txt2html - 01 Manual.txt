TXT2HTML(1p)                   User Contributed Perl Documentation                   TXT2HTML(1p)

NAME
       txt2html - convert plain text file to HTML.

VERSION
       This describes version 2.51 of txt2html.

SYNOPSIS
       txt2html --help | --manpage

       txt2html [ --append_file filename ] [ --append_head filename ]
           [ --body_deco string ] [ --bold_delimiter string ]
           [ --bullets string ] [ --bullets_ordered string ] [ --caps_tag tag ]
           { --custom_heading_regexp regexp } [ --debug ] [ --demoronize ]
           [ --default_link_dict filename ] [ --dict_debug n ]
           [ --doctype doctype ] [ --eight_bit_clean ] [ --escape_HTML_chars ]
           [ --explicit_headings ] [ --extract ] [ --hrule_min n ]
           [ --indent_width n ] [ --indent_par_break ]
           { --infile filename | --instring string }
           [ --italic_delimiter string ] { --links_dictionaries filename }
           [ --link_only ] [ --lower_case_tags ] [ --mailmode ]
           [ --make_anchors ] [ --make_tables ] [ --min_caps_length n ]
           [ --outfile filename ] [ --par_indent n ]
           [ --preformat_trigger_lines n ] [ --endpreformat_trigger_lines n ]
           [ --preformat_start_marker regexp ] [ --preformat_end_marker regexp ]
           [ --preformat_whitespace_min n ] [ --prepend_file filename ]
           [ --preserve_indent ] [ --short_line_length n ]
           [ --style_url stylesheet_url ] [ --tab_width n ]
           [ --table_type type=0/1 ] [ --title title ] [ --titlefirst ]
           [ --underline_delimiter string ] [ --underline_length_tolerance n ]
           [ --underline_offset_tolerance n ] [ --unhyphenation ]
           [ --use_mosaic_header ] [ --use_preformat_marker ] [ --xhtml ] [file ...]

DESCRIPTION
       txt2html converts plain text files to HTML.

       It supports headings, tables, lists, simple character markup, and hyperlinking, and is
       highly customizable. It recognizes some of the apparent structure of the source document
       (mostly whitespace and typographic layout), and attempts to mark that structure explicitly
       using HTML. The purpose for this tool is to provide an easier way of converting existing
       text documents to HTML format.

       One can use txt2html as a filter, outputting the result to STDOUT, or to a given file.

       One can define options in a config file as well as on the command-line.

OPTIONS
       Option names can be abbreviated to the shortest unique name for that option.  Options can
       start with "--" or "-". Boolean options can be negated by preceding them with "no";
       options with hash or array values can be added to by giving the option again for each
       value.

       See Getopt::Long for more information.

       If the Getopt::ArgvFile module is installed, then groups of options can be read from a
       file or files designated by the @ character preceding the name.  For example:

           txt2html @poem_options --outfile poem_glory.html  poem_glory.txt

       See "Options Files" for more information.

       Help options:

       --help
           Display short help and exit.

       --manpage
           Display full documentation and exit.  This requires perldoc to be installed.

       General options:

       --append_file filename | --append filename | --append_body filename
           If you want something appended by default, put the filename here.  The appended text
           will not be processed at all, so make sure it's plain text or decent HTML.  i.e. do
           not have things like:
               Mary Andersen <kitty@example.com> but instead, have:
               Mary Andersen &lt;kitty@example.com&gt;

           (default: nothing)

       --append_head filename | -ah filename
           If you want something appended to the head by default, put the filename here.  The
           appended text will not be processed at all, so make sure it's plain text or decent
           HTML.  i.e. do not have things like:
               Mary Andersen <kitty@example.com> but instead, have:
               Mary Andersen &lt;kitty@example.com&gt;

           (default: nothing)

       --body_deco string
           Body decoration string: a string to be added to the BODY tag so that one can set
           attributes to the BODY (such as class, style, bgcolor etc) For example, "class='with‐
           image'".

       --bold_delimiter string
           This defines what character (or string) is taken to be the delimiter of text which is
           to be interpreted as bold (that is, to be given a STRONG tag).  If this is empty, then
           no bolding of text will be done.  (default: #)

       --bullets string
           This defines what single characters are taken to be "bullet" characters for unordered
           lists.  Note that because this is used as a character class, if you use '-' it must
           come first.  (default:-=o*\267)

       --bullets_ordered string
           This defines what single characters are taken to be "bullet" placeholder characters
           for ordered lists.  Ordered lists are normally marked by a number or letter followed
           by '.' or ')' or ']' or ':'.  If an ordered bullet is used, then it simply indicates
           that this is an ordered list, without giving explicit numbers.

           Note that because this is used as a character class, if you use '-' it must come
           first.  (default:nothing)

       --caps_tag tag | --capstag tag | -ct tag
           Tag to put around all-caps lines (default: STRONG) If an empty tag is given, then no
           tag will be put around all-caps lines.

       --custom_heading_regexp regexp | --heading regexp | -H regexp
           Add a regexp for headings.  Header levels are assigned by regexp in order seen When a
           line matches a custom header regexp, it is tagged as a header.  If it's the first time
           that particular regexp has matched, the next available header level is associated with
           it and applied to the line.  Any later matches of that regexp will use the same header
           level.  Therefore, if you want to match numbered header lines, you could use something
           like this:

               -H '^ *\d+\. \w+' -H '^ *\d+\.\d+\. \w+' -H '^ *\d+\.\d+\.\d+\. \w+'

           Then lines like

                           " 1. Examples "
                           " 1.1. Things"
                       and " 4.2.5. Cold Fusion"

           Would be marked as H1, H2, and H3 (assuming they were found in that order, and that no
           other header styles were encountered).  If you prefer that the first one specified
           always be H1, the second always be H2, the third H3, etc, then use the
           -EH/--explicit-headings option.

           This is a multi-valued option.

           (default: none)

       --debug
           Enable copious script debugging output (don't bother, this is for the developer)

       --default_link_dict filename
           The name of the default "user" link dictionary.  (default:
           "$ENV{'HOME'}/.txt2html.dict")

       --demoronize
           Convert Microsoft-generated character codes that are non-ISO codes into something more
           reasonable.  (default:true)

       --dict_debug n | -db n
           Debug mode for link dictionaries Bitwise-Or what you want to see:
                     1: The parsing of the dictionary
                     2: The code that will make the links
                     4: When each rule matches something
                     8: When each tag is created

           (default: 0)

       --doctype doctype | --dt doctype
           This gets put in the DOCTYPE field at the top of the document, unless it's empty.

           Default : '-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd'

           If --xhtml is true, the contents of this is ignored, unless it's empty, in which case
           no DOCTYPE declaration is output.

       --eight_bit_clean | -8
           If false, convert Latin-1 characters to HTML entities.  If true, this conversion is
           disabled.  (default: false)

       --escape_HTML_chars | --escapechars | -ec
           turn & < > into &amp; &gt; &lt; (default: true)

       --explicit_headings | -EH
           Don't try to find any headings except the ones specified in the --custom_heading_reg‐
           exp option.  Also, the custom headings will not be assigned levels in the order they
           are encountered in the document, but in the order they are specified on the command
           line.  (default: false)

       --extract
           Extract Mode; don't put HTML headers or footers on the result, just the plain HTML
           (thus making the result suitable for inserting into another document (or as part of
           the output of a CGI script).  (default: false)

       --hrule_min n | --hrule n | -r n
           Min number of ---s for an HRule.  (default: 4)

       --indent_width n | --indent n | -iw n
           Indents this many spaces for each level of a list.  (default: 2)

       --indent_par_break | -ipb
           Treat paragraphs marked solely by indents as breaks with indents.  That is, instead of
           taking a three-space indent as a new paragraph, put in a <BR> and three non-breaking
           spaces instead.  (see also --preserve_indent) (default: false)

       --infile filename
           The name of the input file.  This is a cumulative list argument.  If you want to
           process more than one file, just add another --infile file to the list of arguments.
           Or else just add the filename without the option, after all the options.  Note that
           the special file name of '-' means standard input.

           (default:-)

       --instring string
           An input string.  One can either have input files or input strings, not both.  If you
           want to process more than one string, just add another --instring string to the list
           of arguments.

       --italic_delimiter string
           This defines what character (or string) is taken to be the delimiter of text which is
           to be interpreted as italic (that is, to be given a EM tag).  If this is empty, no
           italicising of text will be done.  (default: *)

       --links_dictionaries filename | --link filename | -l filename
           File to use as a link-dictionary.  There can be more than one of these.  These are in
           addition to the System Link Dictionary and the User Link Dictionary.

       --link_only | --linkonly | -LO
           Do no escaping or marking up at all, except for processing the links dictionary file
           and applying it.  This is useful if you want to use the linking feature on an HTML
           document.  If the HTML is a complete document (includes HTML,HEAD,BODY tags, etc) then
           you'll probably want to use the --extract option also.  (default: false)

       --lower_case_tags
           Force all the tags to be in lower-case.

       --mailmode | -m
           Deal with mail headers & quoted text.  The mail header paragraph is given the class
           'mail_header', and mail-quoted text is given the class 'quote_mail'.  (default: false)

       --make_anchors | --anchors
           Should we try to make anchors in headings?  (default: true)

       --make_links
           Should we try to build links?  If this is false, then the links dictionaries are not
           consulted and only structural text-to-HTML conversion is done.  (default: true)

       --make_tables | --tables
           Should we try to build tables?  If true, spots tables and marks them up appropriately.
           See "Input File Format" for information on how tables should be formatted.

           This overrides the detection of lists; if something looks like a table, it is taken as
           a table, and list-checking is not done for that paragraph.

           (default: false)

       --min_caps_length n | --caps n | -c n
           min sequential CAPS for an all-caps line (default: 3)

       --outfile filename
           The name of the output file.  If it is "-" then the output goes to Standard Output.
           (default: - )

       --par_indent n
           Minumum number of spaces indented in first lines of paragraphs.
             Only used when there's no blank line preceding the new paragraph.  (default: 2)

       --preformat_trigger_lines n | --prebegin n | -pb n
           How many lines of preformatted-looking text are needed to switch to <PRE>
                     <= 0 : Preformat entire document
                        1 : one line triggers
                     >= 2 : two lines trigger

           (default: 2)

       --endpreformat_trigger_lines n | --preend n | -pe n
           How many lines of unpreformatted-looking text are needed to switch from <PRE>
                      <= 0 : Never preformat within document
                         1 : one line triggers
                      >= 2 : two lines trigger (default: 2)

           NOTE for --prebegin and --preend: A zero takes precedence.  If one is zero, the other
           is ignored.  If both are zero, entire document is preformatted.

       --preformat_start_marker regexp
           What flags the start of a preformatted section if --use_preformat_marker is true.

           (default: "^(:?(:?&lt;)|<)PRE(:?(:?&gt;)|>)\$")

       --preformat_end_marker regexp
           What flags the end of a preformatted section if --use_preformat_marker is true.

           (default: "^(:?(:?&lt;)|<)/PRE(:?(:?&gt;)|>)\$")

       --preformat_whitespace_min n | --prewhite n | -p n
           Minimum number of consecutive whitespace characters to trigger normal preformatting.
           NOTE: Tabs are expanded to spaces before this check is made.  That means if tab_width
           is 8 and this is 5, then one tab may be expanded to 8 spaces, which is enough to trig‐
           ger preformatting.  (default: 5)

       --prepend_file filename | --prepend_body filename | --pp filename
           If you want something prepended to the processed body text, put the filename here.
           The prepended text will not be processed at all, so make sure it's plain text or
           decent HTML.

           (default: nothing)

       --preserve_indent | -pi
           Preserve the first-line indentation of paragraphs marked with indents by replacing the
           spaces of the first line with non-breaking spaces.  (default: false)

       --short_line_length n | --shortline n | -s n
           Lines this short (or shorter) must be intentionally broken and are kept that short.
           (default: 40)

       --style_url stylesheet_url
           This gives the URL of a stylesheet; a LINK tag will be added to the output.

       --tab_width n | --tabwidth n | -tw n
           How many spaces equal a tab?  (default: 8)

       --table_type type=0/1
               --table_type ALIGN=1 --table_type BORDER=0

           This determines which types of tables will be recognised when "make_tables" is true.
           The possible types are ALIGN, PGSQL, BORDER and DELIM.  (default: all types are true)

       --title title | -t title
           You can specify a title.  Otherwise it will use a blank one.  (default: nothing)

       --titlefirst | -tf
           Use the first non-blank line as the title.

       --underline_delimiter string
           This defines what character (or string) is taken to be the delimiter of text which is
           to be interpreted as underlined (that is, to be given a <U> tag).  If this is empty,
           then no underlining of text will be done.  This is NOT the same as the following
           "underline" options, which are about underlining of "header" sections.  (default: _)

       --underline_length_tolerance n | --ulength n | -ul n
           How much longer or shorter can header underlines be and still be header underlines?
           (default: 1)

       --underline_offset_tolerance n | --uoffset n | -uo n
           How far offset can header underlines be and still be header underlines?  (default: 1)

       --unhyphenation | --unhypnenate | -u
           Enables unhyphenation of text.  (default: true)

       --use_mosaic_header | --mosaic | -mh
           Use this option if you want to force the heading styles to match what Mosaic outputs.
           (Underlined with "***"s is H1, with "==="s is H2, with "+++" is H3, with "---" is H4,
           with "~~~" is H5 and with "..." is H6) This was the behavior of txt2html up to version
           1.10.  (default: false)

       --use_preformat_marker | --preformat_marker | -pm
           Turn on preformatting when encountering "<PRE>" on a line by itself, and turn it off
           when there's a line containing only "</PRE>".  When such preformatted text is
           detected, the PRE tag will be given the class 'quote_explicit'.  (default: off)

       --xhtml
           Try to make the output conform to the XHTML standard, including closing all open tags
           and marking empty tags correctly.  This turns on --lower_case_tags and overrides the
           --doctype option.  Note that if you add a header or a footer file, it is up to you to
           make it conform; the header/footer isn't touched by this.  Likewise, if you make link-
           dictionary entries that break XHTML, then this won't fix them, except to the degree of
           putting all tags into lower-case.

           (default: true)

FILE FORMATS
       Options Files

       Options can be given in files as well as on the command-line by flagging an option file
       with @filename in the command-line.  Also, the files ~/.txt2htmlrc and ./.txt2htmlrc are
       checked for options.

       The format is as follows: Lines starting with # are comments.  Lines enclosed in PoD mark‐
       ers are also comments.  Blank lines are ignored.  The options themselves should be given
       the way they would be on the command line, that is, the option name (including the --)
       followed by its value (if any).

       For example:

           # set link dictionaries
           --default_link_dict /home/kat/.TextToHTML.dict

           # set options for poetry
           --titlefirst
           --short_line_length 60

       See Getopt::ArgvFile for more information.

       Link Dictionary

       A link dictionary file contains patterns to match, and what to convert them to.  It is
       called a "link" dictionary because it was intended to be something which defined what a
       href link was, but it can be used for more than that.  However, if you wish to define your
       own links, it is strongly advised to read up on regular expressions (regexes) because this
       relies heavily on them.

       The file consists of comments (which are lines starting with #) and blank lines, and link
       entries.  Each entry consists of a regular expression, a -> separator (with optional
       flags), and a link "result".

       In the simplest case, with no flags, the regular expression defines the pattern to look
       for, and the result says what part of the regular expression is the actual link, and the
       link which is generated has the href as the link, and the whole matched pattern as the
       visible part of the link.  The first character of the regular expression is taken to be
       the separator for the regex, so one could either use the traditional / separator, or some‐
       thing else such as | (which can be helpful with URLs which are full of / characters).

       So, for example, an ftp URL might be defined as:

           |ftp:[\w/\.:+\-]+|      -> $&

       This takes the whole pattern as the href, and the resultant link has the same thing in the
       href as in the contents of the anchor.

       But sometimes the href isn't the whole pattern.

           /&lt;URL:\s*(\S+?)\s*&gt;/ --> $1

       With the above regex, a () grouping marks the first subexpression, which is represented as
       $1 (rather than $& the whole expression).  This entry matches a URL which was marked
       explicity as a URL with the pattern <URL:foo>  (note the &lt; is shown as the entity, not
       the actual character.  This is because by the time the links dictionary is checked, all
       such things have already been converted to their HTML entity forms) This would give us a
       link in the form <A HREF="foo">&lt;URL:foo&gt;</A>

       The h flag

       However, if we want more control over the way the link is constructed, we can construct it
       ourself.  If one gives the h flag, then the "result" part of the entry is taken not to
       contain the href part of the link, but the whole link.

       For example, the entry:

           /&lt;URL:\s*(\S+?)\s*&gt;/ -h-> <A HREF="$1">$1</A>

       will take <URL:foo> and give us <A HREF="foo">foo</A>

       However, this is a very powerful mechanism, because it can be used to construct custom
       tags which aren't links at all.  For example, to flag *italicised words* the following
       entry will surround the words with EM tags.

           /\B\*([a-z][a-z -]*[a-z])\*\B/ -hi-> <EM>$1</EM>

       The i flag

       This turns on ignore case in the pattern matching.

       The e flag

       This turns on execute in the pattern substitution.  This really only makes sense if h is
       turned on too.  In that case, the "result" part of the entry is taken as perl code to be
       executed, and the result of that code is what replaces the pattern.

       The o flag

       This marks the entry as a once-only link.  This will convert the first instance of a
       matching pattern, and ignore any others further on.

       For example, the following pattern will take the first mention of HTML::TextToHTML and
       convert it to a link to the module's home page.

           "HTML::TextToHTML"  -io-> http://www.example.com/tools/text_to_html/

       Input File Format

       For the most part, this module tries to use intuitive conventions for determining the
       structure of the text input.  Unordered lists are marked by bullets; ordered lists are
       marked by numbers or letters; in either case, an increase in indentation marks a sub-list
       contained in the outer list.

       Headers (apart from custom headers) are distinguished by "underlines" underneath them;
       headers in all-capitals are distinguished from those in mixed case.  All headers, both
       normal and custom headers, are expected to start at the first line in a "paragraph".

       Tables require a more rigid convention.  A table must be marked as a separate paragraph,
       that is, it must be surrounded by blank lines.  Tables come in different types.  For a ta‐
       ble to be parsed, its --table_type option must be on, and the --make_tables option must be
       true.

       ALIGN Table Type

       Columns must be separated by two or more spaces (this prevents accidental incorrect recog‐
       nition of a paragraph where interword spaces happen to line up).  If there are two or more
       rows in a paragraph and all rows share the same set of (two or more) columns, the para‐
       graph is assumed to be a table.  For example

           -e  File exists.
           -z  File has zero size.
           -s  File has nonzero size (returns size).

       becomes

           <table>
           <tr><td>-e</td><td>File exists.</td></tr>
           <tr><td>-z</td><td>File has zero size.</td></tr>
           <tr><td>-s</td><td>File has nonzero size (returns size).</td></tr>
           </table>

       This guesses for each column whether it is intended to be left, centre or right aligned.

       BORDER Table Type

       This table type has nice borders around it, and will be rendered with a border, like so:

           +---------+---------+
           | Column1 | Column2 |
           +---------+---------+
           | val1    | val2    |
           | val3    | val3    |
           +---------+---------+

       The above becomes

           <table border="1">
           <thead><tr><th>Column1</th><th>Column2</th></tr></thead>
           <tbody>
           <tr><td>val1</td><td>val2</td></tr>
           <tr><td>val3</td><td>val3</td></tr>
           </tbody>
           </table>

       It can also have an optional caption at the start.

                My Caption
           +---------+---------+
           | Column1 | Column2 |
           +---------+---------+
           | val1    | val2    |
           | val3    | val3    |
           +---------+---------+

       PGSQL Table Type

       This format of table is what one gets from the output of a Postgresql query.

            Column1 | Column2
           ---------+---------
            val1    | val2
            val3    | val3
           (2 rows)

       This can also have an optional caption at the start.  This table is also rendered with a
       border and table-headers like the BORDER type.

       DELIM Table Type

       This table type is delimited by non-alphanumeric characters, and has to have at least two
       rows and two columns before it's recognised as a table.

       This one is delimited by the '| character:

           | val1  | val2  |
           | val3  | val3  |

       But one can use almost any suitable character such as : # $ % + and so on.  This is clever
       enough to figure out what you are using as the delimiter if you have your data set up like
       a table.  Note that the line has to both begin and end with the delimiter, as well as
       using it to separate values.

       This can also have an optional caption at the start.

EXAMPLES
       Convert one file to HTML

           txt2html --infile thing.txt --outfile thing.html

       This will create a HTML file called "thing.html" from the plain text file "thing.txt".

BUGS
       Tell me about them.

PREREQUISITES
           Pod::Usage
           HTML::TextToHTML
           Getopt::Long
           Getopt::ArgvFile
           File::Basename
           YAML::Syck
           perldoc

SCRIPT CATEGORIES
       Web

ENVIRONMENT
       HOME
           txt2html looks in the HOME directory for config files.

FILES
       These files are only read if the Getopt::ArgvFile module is available on the system.

       "~/.txt2htmlrc"
           User configuration file.

       ".txt2htmlrc"
           Configuration file in the current working directory; overrides options in
           "~/.txt2htmlrc" and is overridden by command-line options.

SEE ALSO
       perl(1) htmltoc(1) HTML::TextToHTML Getopt::Long Getopt::ArgvFile

AUTHOR
           Kathryn Andersen (RUBYKAT)
           perlkat AT katspace dot com
           http//www.katspace.com/

       based on txt2html by Seth Golub

COPYRIGHT
       Original txt2html script copyright (c) 1994-2000 Seth Golub seth AT aigeek.com

       Copyright (c) 2002-2005 Kathryn Andersen

       This program is free software; you can redistribute it and/or modify it under the same
       terms as Perl itself.

perl v5.8.8                                 2008-05-04                               TXT2HTML(1p)
