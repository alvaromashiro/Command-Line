
Usage:
 script [options] [file]

Make a typescript of a terminal session.

Options:
 -a, --append            append the output
 -c, --command <command> run command rather than interactive shell
 -e, --return            return exit code of the child process
 -f, --flush             run flush after each write
     --force             use output file even when it is a link
 -q, --quiet             be quiet
 -t, --timing[=<file>]   output timing data to stderr (or to FILE)
 -V, --version           output version information and exit
 -h, --help              display this help and exit


For more details see script(1).
