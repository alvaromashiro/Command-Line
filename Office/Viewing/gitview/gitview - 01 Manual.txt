GITVIEW(1)                           General Commands Manual                           GITVIEW(1)

NAME
       gitview - An ASCII/HEX file viewer

SYNOPSIS
       gitview [-hvcblp] file

DESCRIPTION
       This manual page documents briefly the gitview command.

       gitview  is  an  ASCII/HEX  file  viewer.  Use the `arrows', `PageUp', `PageDown', `Home',
       `End', `^N', `^P', `^V', `ESC v', `Space' and `Backspace' to move in  the  file,  `^L'  to
       refresh the screen and `F10', `q' or `^X ^C' to leave.

       More  extensive  documentation  on  gitview and the other gnuit tools is available in info
       format, try 'info gnuit'.

       You can change these keys, just read the GITVIEW-Setup, GITVIEW-Color,  GITVIEW-Monochrome
       and GITVIEW-Keys sections in the configuration files gnuitrc.TERM.

       gitview is part of git(1), the GNU Interactive Tools.

OPTIONS
       A summary of options is included below.  For a complete description, see the Info files.

       -h     Show summary of options.

       -v     Show version of program.

       -c     Use ANSI colours.

       -b     Don't use ANSI colours.

       -l     Don't use last screen character.

SEE ALSO
       gitfm(1)

       gitview  and  gitfm  are documented fully by GNU Interactive Tools, available via the Info
       system.

BUGS
       Please send bug reports to:
       gnuit-dev@gnu.org

AUTHORS
       Tudor Hulubei <tudor@cs.unh.edu>
       Andrei Pitis <pink@pub.ro>
       Ian Beckwith <ianb@erislabs.net> (Current maintainer)

                                           Sep 30, 2007                                GITVIEW(1)
