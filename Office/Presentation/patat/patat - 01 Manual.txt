patat manual(1)                                                                   patat manual(1)

NAME
       patat - Presentations Atop The ANSI Terminal

SYNOPSIS
       patat [*options*] file

DESCRIPTION
   Controls
       · Next slide: space, enter, l, →

       · Previous slide: backspace, h, ←

       · Go forward 10 slides: j, ↓

       · Go backward 10 slides: k, ↑

       · First slide: 0

       · Last slide: G

       · Reload file: r

       · Quit: q

       The  r key is very useful since it allows you to preview your slides while you are writing
       them.  You can also use this to fix artifacts when the terminal is resized.

   Input format
       The input format can be anything that Pandoc supports.  Plain markdown is usually the most
       simple solution:

              ---
              title: This is my presentation
              author: Jane Doe
              ...

              # This is a slide

              Slide contents.  Yay.

              ---

              # Important title

              Things I like:

              - Markdown
              - Haskell
              - Pandoc

       Horizontal rulers (---) are used to split slides.

       However,  if  you prefer not use these since they are a bit intrusive in the markdown, you
       can also start every slide with an h1 header.  In that case, the file should not contain a
       single horizontal ruler.

       This means the following document is equivalent:

              ---
              title: This is my presentation
              author: Jane Doe
              ...

              # This is a slide

              Slide contents.  Yay.

              # Important title

              Things I like:

              - Markdown
              - Haskell
              - Pandoc

   Configuration
       patat is fairly configurable.  The configuration is done using YAML.  There are two places
       where you can put your configuration:

       1. In the presentation file itself, using the Pandoc metadata header.

       2. In $HOME/.patat.yaml

       For example, we set an option key to val by using the following file:

              ---
              title: Presentation with options
              author: John Doe
              patat:
                  key: val
              ...

              Hello world.

       Or we can use a normal presentation and have the following $HOME/.patat.yaml:

              key: val

   Line wrapping
       Line wrapping can be enabled by  setting  wrap: true  in  the  configuration.   This  will
       re-wrap all lines to fit the terminal width better.

   Auto advancing
       By  setting  autoAdvanceDelay  to a number of seconds, patat will automatically advance to
       the next slide.

              ---
              title: Auto-advance, yes please
              author: John Doe
              patat:
                  autoAdvanceDelay: 2
              ...

              Hello World!

              ---

              This slide will be shown two seconds after the presentation starts.

       Note that changes to autoAdvanceDelay are not picked up automatically if you  are  running
       patat --watch.  This requires restarting patat.

   Fragmented slides
       By  default, slides are always displayed "all at once".  If you want to display them frag‐
       ment by fragment, there are two ways to do that.  The  most  common  case  is  that  lists
       should be displayed incrementally.

       This can be configured by settings incrementalLists to true in the metadata block:

              ---
              title: Presentation with incremental lists
              author: John Doe
              patat:
                  incrementalLists: true
              ...

              - This list
              - is displayed
              - item by item

       Setting  incrementalLists works on all lists in the presentation.  To flip the setting for
       a specific list, wrap it in a block quote.  This will make the list incremental if  incre‐
       mentalLists  is  not  set, and it will display the list all at once if incrementalLists is
       set to true.

       This example contains a sublist which is also displayed incrementally, and then a  sublist
       which is displayed all at once (by merit of the block quote).

              ---
              title: Presentation with incremental lists
              author: John Doe
              patat:
                  incrementalLists: true
              ...

              - This list
              - is displayed

                  * item
                  * by item

              - Or sometimes

                  > * all at
                  > * once

       Another way to break up slides is to use a pagraph only containing three dots separated by
       spaces.  For example, this slide has two pauses:

              Legen

              . . .

              wait for it

              . . .

              Dary!

   Theming
       Colors and other properties can also be changed using this configuration.  For example, we
       can have:

              ---
              author: 'Jasper Van der Jeugt'
              title: 'This is a test'
              patat:
                  wrap: true
                  theme:
                      emph: [vividBlue, onVividBlack, bold]
                      imageTarget: [onDullWhite, vividRed]
              ...

              # This is a presentation

              This is _emph_ text.

              ![Hello](foo.png)

       The properties that can be given a list of styles are:

       blockQuote,  borders,  bulletList,  codeBlock, code, definitionList, definitionTerm, emph,
       header, imageTarget, imageText, linkTarget, linkText, math, orderedList,  quoted,  strike‐
       out, strong, tableHeader, tableSeparator

       The accepted styles are:

       bold,  dullBlack, dullBlue, dullCyan, dullGreen, dullMagenta, dullRed, dullWhite, dullYel‐
       low, onDullBlack, onDullBlue, onDullCyan, onDullGreen, onDullMagenta,  onDullRed,  onDull‐
       White, onDullYellow, onVividBlack, onVividBlue, onVividCyan, onVividGreen, onVividMagenta,
       onVividRed, onVividWhite,  onVividYellow,  underline,  vividBlack,  vividBlue,  vividCyan,
       vividGreen, vividMagenta, vividRed, vividWhite, vividYellow

   Syntax Highlighting
       As part of theming, syntax highlighting is also configurable.  This can be configured like
       this:

              ---
              patat:
                theme:
                  syntaxHighlighting:
                    decVal: [bold, onDullRed]
              ...

              ...

       decVal refers to "decimal values".  This is known as a "token type".  For a full  list  of
       token types, see this list -- the names are derived from there in an obvious way.

OPTIONS
       -w, --watch
              If you provide the --watch flag, patat will watch the presentation file for changes
              and reload automatically.  This is very useful when you are writing  the  presenta‐
              tion.

       -f, --force
              Run the presentation even if the terminal claims it does not support ANSI features.

       -d, --dump
              Just dump all the slides to stdout.  This is useful for debugging.

       --version
              Display version information.

SEE ALSO
       pandoc(1)

AUTHORS
       Jasper Van der Jeugt.

patat v0.4.7.1                           January 23, 2017                         patat manual(1)
