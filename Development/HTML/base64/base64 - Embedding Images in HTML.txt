#####################################
# base64 - Embedding Images in HTML #
#####################################

Embedding example:
-----------------

    <img alt="Embedded Image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIA..." />
    
But why? You could have a copy of an image to point an HTML file to; however,
by using base64, you can have a single, highly portable HTML file with
images.

Encoding an image to base64:
---------------------------

    base64 "/path/to/image"
    
    *Use -w flag if you need word-wrapping in your terminal.
    
Decoding base64 to an image:
---------------------------

    base64 -d "iVBORw0KGgoAAAANSUhEUgAAADIA..."

Example base64 encoded image (See example.png):
----------------------------------------------

iVBORw0KGgoAAAANSUhEUgAAAkQAAAF2CAYAAACPuodhAAAgAElEQVR42u2dWZoqOa9Fg/sxRRgk
DJL78CdUlSKVW7LlaPBaL1WchGgc4U7e3rosy/JaAAAAACbm6v3h9frfOOlyufzns8f7e+p4rb/3
riv791bU9UfvxzuO/X7VfUTLq/o6or+LlgfMxe3nvXj+vAf2c/R989ox73vee+idP1s/3t+//3x+
iHrX2s5Gj5eltR3Yq35H7zva7kXby6rjZ693q+fa2r9Er6OqfLL18/9oegEAAGB2VhGi1+3vEZUc
wb1//7ykRsje8d4zs/dM6h78fWskyZuZRmdm9+BIWn3fPY8pX+/zu8BWET4zNfUigOGZtfO8q2YE
6v7Pwlmvu/d9zJbDqr47n22kxmtf7uLzxTmOrQ/e9WTrV7S9U9cTbR9tA+O1P+/fq+v3np9q3+z1
taIih/azF4nL9hfRSErv/bZe76gI/hKsL9n2Xa1AZccfveMBIkQAAAAA7wHXIkTVvWvZaoSXjSCo
87eu8UZHoNHrzUZasiP46uNErz86k6h6T74lgiLfmyeaqUwkQL1P3kz9kayn3sw9266p37//7t1v
tj2t0lhWaUSOVp+rNKG9528t195ISPR6su1zq5apSjvUerz334kQAQAAwPRcpQZAaU7uv89woyPU
1jVcOxN8mrVUTxOj1mhbrzeqwbGLyjehiejWntx/j1i8j9e7Zq7uV71fqny8iMtZIipK6xXV3nwL
4XK4iM/B98m2D4uJ2HmaSW932UXN8E0DoyJVq8+OZirbnrrti2gPlCZJtbdRjaTXf/RGBr3+KNuu
tUZe1PV777N9X7z+5v2FKs1Mb3+31efwLr1bboXCno8IEQAAAExPs4ZoCUZYqv01VCDkmYz4ZGdc
Wb+T1ohLa0Rm1Plaryv7PL/dfwhNUQ3KF2j0e5WdgXuRl4fTfnmRhodo70bd56j2NdvORvuD6Hla
+5tsvzXKF6rqvre+nt776K3naIgAAAAAHNJO1ZfkroeoL4Q3k8quIdrruAnfEPXZrrHfnZlf1Deh
deZrIwyrNWjnfNVaFbvmvVqzFxoF5XP17cymKcreZ6tPUdR3xr6v6n3Ovr/e9X+uT0R6Fhs5svcX
9EnzfJHU/dvr8TROS7L93lrj0v0+BvubqIN51Peqylfv1ak9i0a6suOHqM9SNKLTG+FCQwQAAABg
B0iv5NAqGvFQuykUvbvB1Mi3N1db64g9OzJPz8iLc6D1ljPEIijZCNPseD49KjIcfc9Hay6y7WFv
rrRse9p7fbb8W/uT1ud21hxqre14r3Yz27+P1kT1+n61vj9EiAAAAGB6rq25RryRWu+a6HvG/DBT
mzJHztvvM0mVC6l3JutdZ9RnQ96P4yuUzS0V1Uh4yXpm9ddpJautOTtVGjZV7z3fIdV+ebnAvOeQ
1Uh4ucO83GB30V54Gke1my3anlrNk0rWpXbTKZ8g1d9En7/Xnqvcj56P1LOx/Va+WMqnSLW/VmOr
fKWiufQuO2uiPscT589GPpVGjAgRAAAAMEEdpSFKX4hYk/TWFKvU8b0aota1zLKZ98l9fUb5qJw1
gpKuP5NH4Kq1c727aaRjdjFRnyOl6awqv6gGpLcd7m0XezMUjH6PWvvl1t1a6vqrc4H2/j07rlDl
QYQIAAAApmczDZGaSURHnGoNVo64g7nX1EyvVYMkfXyCEYRWX5Sspij7d+gjrfU6uVar6v30dpcp
XyE1o4z6bnkapJXGoTHy87zEtGXp3GkikqOuxyKPr3I5ivYwWh4qF2c4onHP9Sc3Z7djNjeb6v+i
mrCq3dr28T2TDu2XoK+eikT1jldU/06ECAAAAJiQvjZKYhLNEhz1HYjmAoquYff6GfTmyJleO7OT
hqh3l9/WkZRwfZssgqc0NHJ3a9JXLdoeZa+ntT2JZgCI5kbrbcd6j79Ve5Nth1vbKZmBoEg7NVpD
FL3e6nHD6PORywwAAADgh6u3JmrXXHtzk6g1VzXzCjuwJmeGckTq+Ox45SM1TsLHIxopaNWSHFVr
MioyFJ7BHzxStPKpmSQXWhQvV5jS+HzaI6FxeHj19eez/fvz2XY93mfVnni+beo9vwdzJ3q+NyoS
cheaH6Wx6f2czVUWvX/Vn3mRuItTXq2507xInLu7MXi9nq9RtL1uHR+M8j1UETA0RAAAAADvAZen
Iapeu/OOl809E/W5iF5Ht/M1/jm7citeW36exbcJv6K2cjtYfc1G3qPHi2qYqn//7e9FOONCcteT
jeT0+udln3evRtbzDXw2aveyK1OqPKPlRoQIAAAApudalYus2cfBGRmqkXR090Y0knS55HJKrXwu
zMx9lLZjVp+gXife6Oez0Kop+voI0OD62vvZa8+89k1d70qzk3zPrUamNzJU3b5t9vkSbJ8bc2tG
NVfe9SgfLPX8vHa0VROs+tEwzvUrzd8jqnF2NHLedRIhAgAAgOkZ5kOU9S/oXeNuzcmC9ueYkaCt
eZ78PeiNAH1rRPFJ/f41MuBpLLIaoqO3o6Ovr2rlQ2VcyPazVeUzWkPUOj5QtGreiBABAADA9Gym
IbJrhHbN9GH+rmY20TV2TxwSXTOu1gTN7h/T6uw7+vPZS7vXp+hb3rtVexH02Rnl43Xr9OFR549G
wqK+Q6uZtGmflU+c55sTdaxW9xfWyjmaFJXbLvv8V+UlRDzSR0howlQkSvogmevNaoyyudvse6We
Z+94I+1Qb85PhAgAAACmZ5iGKDpDfwr1/Sw+GN/OXtqgKN+mNZldU7S3huhoubxGlUdUg9Lrq9Ta
3p9FK6qej8rNqfrbVf1u9LVqfX9UfYhqpaK+Rdnd5W+IEAEAAMD0DNcQyZGr8EtRmiFvZly1Rlyt
rfh2zdBRNULfriFazXwm1RSlfWVGtR+OZqdVExM9f1aj0v2eJXNDtkYmwruUkr5T2feh9f1RuUHV
++D1hytNkH2+zvnV++L6WiU1b4uJBEU1bPb5u5Gi+9/vY/R9J5cZAAAAwHtAtZWGqDVXT+vMAo3R
WI6uCcry7X41VU7Vs+dCO3t9Pdt7XpVzcu/rr8oN11oeMnIo2sHR789eWjE7/CFCBAAAANOzmYbo
g8lFotZ4e9ecs74Xs/sEqRnGWTRBvbscN59JDn6vqjRF30615rDa16i1PTpLZEj50vT6Co1+Pqt6
F+wvw9yXVHl4GjKvHVSaIqWBa26fhGP68/K3Jivab10cfyQ0RAAAAADvAdJbQxT1LYjmGtnaNyga
wSC3Ua48v5293wcVeRkdiURbBGfirBrRo/lA7d0fZqXLl6Rfkbp/73tEiAAAAGB6LsuyvH4baUZz
00T/rtYEWzU6t+SaKJqhXDl+6+ejzTD3fg9768XpfYuel03KZSvN0LeUd+/xRte7qC/UVpGZaG63
bIaIqKbIO17UB0iNF6TjtZMLz/oYes+LCBEAAAAQIar2IbqIkdyokfJZfTb2jgidjacTkWz9/VFn
0OH6VhwpQFMEMK69PXp/F9XcRCNJrblLL8mIUhVEiAAAAGB6whqi3txlUU3PcEfMjdeod484nDW3
WNJf4myaoex7t7WGJ33+V18OsaOU87e3B2d/70c/n6NpiLIO0739a6vPT1V/HtUoKY1zq68SESIA
AAAgQpTVEHl+ANHIUNWaI/w+sj4r2RnEqOMfbabcXcE7IxnyOlSIrvh64BjtCu1yWzlmIx3RSNC3
ENUGoSECAAAAGERaQ6S0Gl5uGeuDMNvId9SMYxYfoNb7/Zb3aG+foKiGSEWKiBB9Z/tSNVPv1eyc
RruW3M3V246FIy9BTVS0v1aaoqxvYWv5Rn9PhAgAAACm56Mh8nJ7LM7INbvW16sxmn2mdlZ6n98s
mqHozLms4idn0OHzJzVFrdcDx2xviOwn6/Xg3GzRSNS3lIcniY6OU4gQAQAAwPRcXiY0E13D9EZy
KpdK1jdgVt+Ps2iERs0IZ9cMyZnQQXNsfQhGimbLGThLe3MhUnSodsDrf4+WozCrSfLuLzo+seMZ
IkQAAAAwPa4P0V4+ALPOzI7KVhEXtAttM0O3YicjQ+mGI3o8NEVTtj9oiQ7SThxUQ6S0PlEtUPX9
ESECAACA6bl6+/5XM1Ezs/NyFmXXINWa4bf5Eh01t9heM4be8ph9JjpKQ1T2+fK3T1G0HYExDGtP
KNox7WW2v/15MA/jA2jrY9bXyfP5ifbXF6URCrbr3vgFHyIAAACA1gmm0hCpNbpWLdEsuc2OqhE6
Sjme3WfIama2jmh4GqCjRYrCIQnVYBExOnT79C3t87dGnns1N1s7bKvrCA908CECAAAAiCE1RGoN
0tMIqJnce+R4d0aS6u9nG4F/m09Q1czsrPejIiFbaWAOryHyNEW3Nk3Rt9ObwylLdXtzFmT7c5B+
p9pPzNX0OZEd+zkaGaoqR9WOLGI8cg/2y2+IEAEAAMD0fLLdV0U8Ztn1gzZo23I76n31Ri5G5RQ7
W+RIXX91OZ6tXox2hP/2dudb25/qcon25727w6t8DKNaIjte8f5OhAgAAACm5+rlDlsev4+s7Mjx
LnKPnd1PBN+gsTOQs0ccqyIjqp6cRiM06vPrdw3S2duZaPsy6q6+vf2par+PcrejtX13oQFSGiF7
vOxus3TkydMgNu6OJ0IEAAAA0xPWEPX6Dp0tknE0zrqGPUuOsqPtgjq7ZijsYxQsh7PWi9Hv/17a
pbO247NpZEdlkFDjiGguM+93asXBOz4RIgAAAJieqxr5efv4Pd+hs63loxEaO8OY5f5XuX5m1/yM
iiw1+hYdtV3aWzM3u0Yx+7l6hST7Xo6uZ4uNpAjNkS135Vh9e/2tMbq9/vYZan3u0eMTIQIAAIDp
kRoiL6LyLRGMo/Ata9OzaIbUDE5WvOdYR+avjxR5IY5k+Rylfuz13p81l9de7fcsGqLseKAqwmgj
QVntcnZ8Ys9HhAgAAACm55peY7z/PiM+umZoqzXmWZ28Z9MMqcgDmp9tfImymqK9tI5ZJ+CtOKrv
Du11Y8SmOPdZNjfa3XmOT6EZ8nyC7HnUe+FFlLwcgfb3RIgAAABgei6vn6HTt/kKHUUjNEsOHMrn
7xnbquIN9u2ZPVKUTcc+OlJ0Fu3Q1te1VTuN9uck7aUTGfL+br/nRYa871uIEAEAAMD0XD1fITuz
QiP09+fLZDMQNEMxiOQM/nzJRYq2zoWmfM5a61dV/RmlIRrdHs+SOaFX87OVb5F97q0RuVVESORW
td+Paog8H0UiRAAAADA9Kw1Rta/AVpGKrTnKmnTviHyr8p51DX+UBqj6eL0+SKfRFCXv5+jtU2u9
ymqI8P2Boe1kUOvjRQS9fhANEQAAAECS62rm9PP5EZxZ7eX3sZVm6HnwyNjnejeKFG1dfmfLjdda
L/i8saboPVMc3G49g/4prZ+H1efJ29eztiN7Hy/ab6ncYt731UrWXfgaeblZ38cnQgQAAADTs8pl
djQN0dZr12eZsWztI5J9DrPP/LJanMPkBhvV0Bwl11kyxHJ0LZGdaR8FIj9Q2p6K99tGetT3vd1o
RIgAAABgeq5erjKrIdra7+HGGvafI2V5f0WaovD5Gsuz11fj6KDx2Tny1elLVE11+5Wtn/iGHbx9
H+wz1Hu+7N/L2lFn95jtd6KaJM+niAgRAAAATI/MZba1puiovh1HIVo+vfeJZqhvhpeuiAfTDLXO
+KoiK9+a6+xoWh/qMZy6vXUiPdHxCRoiAAAAAMPKh0hpikZHhlgD/5vw/XaOuI+qwapeM69aAz+7
5qaKo+RYOrov0aj2T9XH1txqcJCIyGDfob3aDe96Pd8ildPOixTZ3Gj2xSdCBAAAANOz8iHamtH+
HN/GaA3RVhqlb5uxeZGSo+Yi22uXXvb6juZLtHf5jW4/qNdwqvbXiQyt6mvw70SIAAAAYHour58p
w14+L9U+Gkef4fT6OUXLa9Tx9y7f0bl2qq7rWzVCpy2/g2iIjtL+kFvsSyIkB/MlstyEH57VBL1f
RO/4nqbI0xjZ43kapDdEiAAAAGB6Tq8hOnvusez1b6UBsOdh5vj3DG1VsTbWDHnnO1uEyG2oNo4U
yYYTDRGAfM9UBNKLWCqqdlU+iRABAAAA/JerXYPbau2813fjLL5CVb4f0fKq4qgzxdHamuj7jmao
FnvdR81tdtby3rr9gH3av+zve8/v/v2nHqmcmtF+3ttNdhcaoqxWjggRAAAATM9uGqJvz5VVvVaP
P9AxZ2oq4rGX79BZI0XDy6tIQ3S2cqb9gDO8n9FIkVoxUrvJVu0O2e4BAAAA/se1OjdKlrP7Cnkj
zVGap2/P1db7/m39PqMhqmX47rtiDdHZQEM0V3u49/ncemPq29Nojux76WqIRL9r32vli0SECAAA
AKbn8Bqi2XyGWsuLtf9tZ2R7RzrO7juULeejaojOUu5oiOAQ9Tq5gqKy2HuZH1QkyDsPESIAAACY
nuveF3BWTUxUFb9ZeX3LDGKwz0bvGjuaoW0Y5Us0qw+Raj/QEEFpPTMv1qee/Hx+PP773tnI0UdT
ZC/wLj6b93ox1+f6LpLtHgAAAOCfwMIhNURHXcve67rREB1rBhWNcBApGlPeaIhoP+CA9bVRI1S1
wvJy3vPo8YgQAQAAwPRc955xHt13SPkbbK15qiqvrAp/64jA8Fw7aIhOwbflMvO0Elvx7RpE2Bmj
EVKRV7tbzH5f5VrNHk/9nggRAAAATM/hNER7RSyOmlttq5xoaAiCM/yDaIi8881S3mfTEI2qx9Hf
oSGCQ9bvwZkdstdBhAgAAACmR/oQjdYqqJwlW41Io5+PsuZ/Lz7eXvPDam3MaK0NGqJtKS+vy9+R
otEaot72xMvhFG0vv1VDlM1uDmP6VVvusp44miNbPz0fIRUpzdZbIkQAAAAwPbtpiOzIPkqVZmbU
+bYuHzREG8+E0BAdoryH5TZrbUgbNURbaYC+TUNEbraD1M9BPkJbaYqsbxERIgAAAJie62jflugI
XuUGu4sRZO/no605R8uj93jMoNoiAWiIvqO899YQZenVAFVpiLZuT6LtIRqijetn0vcnqylSz1Hl
TvN8jFb1nV1mAAAAAP9MDF5HvLBe7Y/HWSIi1WvkaIdqOKqGyJ7328p5WHkO1hBVaXdaj7NVOzKq
XTmqPxwcpJ0QmiO1C9P+nQgRAAAATM/uPkTZkX5WQ3T2GUP1GnmRMe+0HFVDFI1cUd5/R4aqNVyj
669q36rbx7CGp3jGj2boO7H1KOvA7kWC1OfFeW+IEAEAAAAT3uWgGqJZGbVGj4aodkazW2Qjef5v
Kd+jaYai5V1V71qPs5WGadT9VZ8HJmuvzXulIolEiAAAAGB6rrP6mxyV0Wv0aIhqIgFH1RCdvd6e
xXeoW0O0Uf3dKhdi6/2hGTo2o9uZrGYom7ts5ZMkvk+ECAAAAJjwLmiIDgUaonPNnHaLdEQr+Eki
RmfzHYqW71E1RKO0Q1XHtxkMaLe+s5/bKgKo/IjeECECAACA6blSBMcCDdG5wJfoJOVZ7DsULd/R
GpvR51czeNcpuOj6qts/yJHVDEW//47w3Z1Ika2v1qfo/X37d7feBjVHRIgAAACACe6ChugQjPL5
YA1+mxnUZpGOosjPXtqizcsrGBkaVX6jNTzfqiHqPe5R23fa3d/Lw0Z+LslMFVWaIyJEAAAAMD3d
ESJ8jGpHzNW5h0aPqJlBxerB7L5Fm9/fIM1QNjK0d/0d9fvR7dO35KZU93e0+xndrmT7uU8E9/H7
962GqHd8QoQIAAAAiBAtaIgOFSFRVPuX7K0xip7/qBGkvTREozVHrZGj3vONut+tNVjkMDt3u3T0
dv0s9+3dl6chUuXgOU7biKfyHfKOQ4QIAAAApufqzfDQAO1DtXOnu1ar1nIHR2RuwfN7/hRH0UCd
VTP0rZ/ti1+Vq6y1XWz1/ZJOvr3tSW/70Xl/0fI5m2/a2TRR1fVi9d6KSFA0N9mqPnc6znv9BxEi
AAAAmB40RAehas0/e1zF6AhRL2iKzuF4PZum6qj+Q1W/r74vTzN49Po+i49Sa7lUZbH/tK/OCoH9
d+/zql6bvxMhAgAAgOk5nIZoNl+j6Jpz9u6jfg8qN1G1Zie7xl6tWRgNmp7v/NxKtcZGtQfh+hWc
kW+labyI6z+KZvDbfZN6xwNKQ5TVHNnvR9+DlXbQiShZiBABAADA9KAh2pmt/YfU8Uav3bfe79l8
SaxWhUjLOTVQrRGivfyDejUtW2sZj1rPR+dw+1ZsBK1aQ6TO62mGlJboDREiAAAAmJ7rViNtz3dg
9lxoYY1P8fEvznMavQaeXXM/Sy42FWEgUjPn7rqshkjVD28m3KrBy/4+2+5nj1fd/mUjGtnymy0i
5PXPXjku0XrnZL23ucq883v9gdp1xi4zAAAAADuAWg6qITp79vPo/UUZnQNpL+1Q9nqqr2urGVW6
YhKp2TXS0xuhzr6/Vb5c3nlG+35VtWej6/1RfdnOioqwZcurakXA0xDZv9t/J0IEAAAA03P1fAFa
NT/qeL25UL6NrTVE1d/vvZ5v9/UgUnOsyNBo3yEbSXBnvMkZdquvWPR6sr5kozQ36n5VBKHK5+wy
SSSotR60vreqXKN+eMofL3oeCxEiAAAAmB40RDvfX3Sm2XvcvbRD2fsdfR1HmZERaTqWhmjUrtbq
935rn5696+0obdW3ti+bt2ciUtOqCVrtNou2k0G/ocW5DiJEAAAAMD2X289QSuUaadUU9WqQtso5
tDVRTcBolf5WPj+9Goivm1kRudn283vGeN8mMrRVfT9KezW6vmbbD3yDxrRL6vvR5+aNJ9TvZTZ7
8+DxIQIAAADIRoiWoIZIRYpaR/xqZIiGaC7/oVlnZLKiDtpNhYZo+XOGu1X9pz7UlCfl+N3PR+Us
c+s32e4BAAAAYnw0RL3+F61rgtm137PnOhvtuxPVBG2Vu+zsucg2ey+I5Oz6GQD62yVFdqXJ0yB7
HafyKVK52IgQAQAAwPRcXj9Do1an1F4N0Wy7Ao6Sw2xv/yHW+mMzM7fiFuXq+nZN0FE1RAAzUTV+
iDqVL+bf1fHeECECAACA6bm8zNCtN1KU9R3w1gS/da0fDVFs5F59/rNrltD8oCECqG5Hesn6FGY1
RCrXqR0vqHbfjTj/HJAIEQAAAEzPyoeoNZtw9O+zM0pDdLYcZs+NnG23Pu9WM7zyhqDT5yh7vF6N
kHe+1nIiUgSQb197Mw9EI/hebjN1XLe+k+0eAAAA4Heudg3NG+ktS83fR+c2+/YRtcIr96rv917P
5/6Kjp/VZJ2daE6hb9k91lrPvYgRGiKYidG+QXfns213PYfpu9OO20j++/PFfF+2A8FcZm+IEAEA
AMD0uD5E0TW9bC6y3l0/Z901dBT/odbv974HvedpLcfe851tJtjcEBRrerY6X3Smix8RwPj+TWWl
V/15tH93d5E5/UP0+ESIAAAAYHquSmvhrkEKn4HP8cSaovId8jROZ4kUZTUuvRqbrTRE3nO2kZje
+2suv8l2NWY1M7Nksc+WD8A30ashkv270w6H++f77+289BFyfAxVJEhdDxEiAAAAmJ6VD1FrBMRz
QvY0RK0z+rNpiI6mHdr6d1Xfryqv2WaGbsUf7AsUPW/1cXvLBw0RgN/fRiP23vdHjQdW9TipQSLb
PQAAAMAP19bcI96aoR0JernKHo4myNMUeWKXo/uJRDUv9+rji91f3u+rNU/ue5P0ZRqVa+1b2VtT
tPX19ZYPwBkZ5a+lND2uRkf1I9a3SPSDahezOkC2PIgQAQAAABPJxcllVqXp6d0ldPbcaFFNzChf
nlZNUJQqzVN1ucDfM0rZMHRqilpnqFntU9V5AUD370oTpPr/ag1R9X0RIQIAAIDpuUZHgp6WQ675
OZohLzLkJUd7BNcIj6IpSucuazxPrw9PdMReNYJXWjQiQmPZyzeo6vrU9wEgESkRmmGpLRUO1Xen
3a/SEJW1Oz/HJ0IEAAAATBjfucxUVtiqXT/Vxzsqo7VD6jxEWiAzQ5QNxU6aoux1Vp0PYCZUxP7W
uCs4mxu1ejygcp6R7R4AAADAcLWRIJtbREaKHJ+i5pFl0hdptF9J64h0lO+QxSt3gAhH0xCp6/zU
s4NqB8vbk87cU/DdqOcdrqdvTU9n/+2NF964/bdz/mw9j+Y2I9s9AAAAgDfxemuIlE9ANFdZNmIS
jXCcZVfSVtohgJEzTtlwNGqKqiIXW2mXAKBdQ6T68+dGfkOrdoEIEQAAAMDvXJWvgJtrLLjG16oZ
sj/IrjHaNcTRI1ByccE30K1F6NyN1nqds2iIspqRUZGCqMYT9n0/ejVoVuNjjyJzUipNUqfPkLo/
pZG2uVOJEAEAAAATwuUnl5naHaay3GZnFt5Moje3yVa5UcjJBTPORN2GZGdNEQDsR1bju1ckUWmK
iBABAADA9HxymSnfARt58dbglM+A/WzP5+Uu83KjrNYQze9Ha4ha/ZUAzkTWB2hrTdFZ2Nrfqfd4
WY3QLehLRztY8/6UR05Mf67el1VusuDzlprhQfXB0xS9IUIEAAAATPyWHw2RGunbkaTnSxT1GVK+
BqNyqVWD7xDMPFMtb5DQFH33e9OoQYWxz6Mq99hRNb9RiBABAADA9FzVyG8RuUE8nyDX10iMLFfJ
T4wmSGmIts5xpHbJERmCbwRNUeOMvCr31MY56Lx2TWmErEbknmwnj+4v1fs8R59fft/R7NrxgPd8
70HNsfd8laZn6+dPhAgAAACY6C1GQ6T26as14KxzM07OAN8XAelumNASpRi1e6ta44GG6OT1O9j/
Vz1fe7zW40ePQ4QIAAAApmelIfJ2d725i5mDivxIDZHIRaZ8C/C52CciwIwe/v0ebO23c5b60esD
5PnAqNyQ3vdV++m15/801227h1vLR+WutPdztPZvbw1Y2DdQaMi8CIu3+zy6W/0lNMu9kSIFESIA
AABgQvcyoiE14noFfXeyPgbKdyjqS4QmCQfsV1kAACAASURBVM7ALJFMNEXHep+qfWeikaFen5qs
ZhWN0pjyiUZmqt4HNEQAAAAAG3Nd+QA4M7z3DC0aqfFmLjaX2UWM4GwuM8834fn8PZcKxGbwrTNw
NER9M7Zv961CU5SrL83lEvRl622v742+NL0apJVPzSMWaaiOxFa3l9WaMuUzFC1/5Ruk8HKbRft7
e/3e+aPPQ2mdiBABAAAAvAdKLyc05M1YlU+RHXk+gsdr9S1izRjOQDTn3eLUk28BTdHg8hW5Jlvf
q2yuykVEIqLHb81tGe1/4Jjvq/c9Ne7o/T4RIgAAAJieqxpZL0JD5I24vDVEtabtrXkSGaqdoW+V
WweN0X8JR0C/VAs3u4aot/4oHx7pExf0KcpqiLzj35LnuzufF1FvVrnTnL/3tldH+37v+6N+P/p9
jvoMehEe14dKrGzZ8cv7v0SIAAAAYHo+GiK1RuseYNCatPd91oDhjHgaIjuT9v7+7RGR5gaMyOOf
M1+vva3ONVWt0WnVAOFL9B3vr6z3Yne8+rv3fSJEAAAAMD1XteasNEO34jVjj+jMo3fN9Vtn4FVa
DDRBfXh+LNG/fxu9fivfVj+76/P99+8v5r2K5rbK5ipbRHvfevzo75Xv0V65xEadv+p9GpULLns/
ni9Rc24zIda0vydCBAAAANPjaojkD5NrstksyADfRFQjNKuWyJ25Zhs0Ipd/l2tjbqhs+z3a9whi
z/fs/bF6X9V4JepvhIYIAAAA4IdraxZ5b3eBt4apct14a9rla+zi+2edSW+1Jp7VBMB/yWqIPjO2
ycrJ1sdv1RSN1vh5GiMvp57SFCmfoKwv0S2YG63192GfpYNoiHojKdHnGc01V11+0d9nNUPZyJEX
QSJCBAAAANPjaojSam5nxHoJ5q5BQwTfDBqi2ohKuIFDU9TVTmff72xus7NphKrKy3uvqyJFUQ3R
s7Mfr77OXl+hbMSICBEAAACA4WrXlNUI69440lyNhN/fNzO+0blUvnXGXHV/W6/BzwYaoj7SOb5O
Xp9H+9q0+vQsXq7JpIaoVyPUqymqyhWnNLRe+d6M1qf3eu0D9fpnVT7R++19f1c4PkFeREdGim65
CBcRIgAAAGDCtSzL698jwOjIS850Bq09VkU+APZ4/yyz5zKriqjIdgst0a/vY1WuMOUofZb7y95v
1f1vFTnP9svR5119HXuNH4gQAQAAwPRcs2tsamR7c3wpsmuWrWuycqYg1miPOpOrXhMf5SuUvb7Z
UH5e7LrMMbt2rbn+OxqeqObIixTY4/f6BDVrhIIaJXX+9w0+HkssUpTsb0ZrZns1Qfbv92TuOe/+
vN1lcvd6sc+gLR8iRAAAAMAEa/nREGVHatlIgTvia1yj7I1cAGzJ7ZXb9cR7WjNTdhs+NEXHfG4i
cjFKWxLVHMnd2D/v3f3xdz3eK2LZqiFSWp/WCPco7bL9nffvFiJEAAAAMD3X6MhXzcRa14DVmuzi
HN9bs12tTQqN1GjtQfXxt16T79UwMeNt26UCfahcaKd5fwbnZuw9fm/7k/Y1CkaSovenfJFWkQdr
3HRffo0MKR8m6zv0MMeL+iLZ76v79/rHRWikvPJWudCi17Mq3+ABVe4zL9LknY8IEQAAADCRWhwN
0da7XqJZeJW6/eGMBM++awcN1N/lshwswoJmCGash739RauGaFT7mO0Psz5Nqv/6HFdEclo1QdEI
dlVuNHU9rc8nPfBxIklEiAAAAGB6rtGRrTcSjK4Ze2ukq5Hlz/EeYk3Zrvl6kSG1BnpL+hL1+v60
+gRFcwP1aoDOohm6BbUFrTOe1vNnPxMZgj3p1RzdOjU+nu/P8/m35tSth8n2Kq1pspEJq+FRvkXv
6/35g9UcebnI3v++0tgmNTbe9SufJdn/JftPV/MjcpmpSJfMdn//O1JEhAgAAACm5/JyFuGqNDfe
Gl/vjNqdodshbVT+viy7RDqyEQl8l5wZ1MkgMgTfWA/3clqvau+yEeasL5Hs75xdY17kpdpXKFqe
1eWd3h0mxitKW4QPEQAAAIDDVY10leZGOXyqSI9d873YEZ5ZU/Zy1XyO/zAjbbEb4WbWdKtzhZV9
DpZ3VnM0OrfZVpGWKk1P1We1i4P4EByRVo2g1YC2Rnb29k2Tvnj33yNJ6rOnGVqd7+d/Xvf/RpY8
3yKl2VI+RCoXnOzvO1cu7PUozU9UY+QdT+0+J0IEAAAA0/PxIWpdc6ta88uiIj2r8ynV+5L73W4z
uKAvBr5FsfenlWdwDT77OwA4fnub1RSFI8yNGqLeXaxZzVB1/5Idf7SOO9TxiRABAADA9Fxvr781
Kt6anrfmulLXm7/bNUGVqyWrkcmuYdvz2izFWR+Lat8fW06qPKvX3FsjL0ffDbfV+ZKbHAGOFSEZ
nOtRtQe9vnC9mtCoT4/UEHmRm5vx6XPKY3E0rlZj2+rDpDSnrZpU73lZDdBFOJJ7mh80RAAAAADF
fHyIPA1K1FfCXbMT2Xi944R9iER24FEzpaPM2LznhRPyvqAhAjg/K41pMJdYtD32dqOFNUTBfqnV
V2jlo5Tsz6PjhKwjdXqgIzRIaIgAAAAAfrhG1zyVet4d4f2MMO1Iu9UHwssFY30b3p/V+dw1VXP8
pdGnqDp3mDeyd30uBucuy97vbKAhghkiKFUan9HtT/Z8UYfp6G6yReVKM0ZlnobI6/9sfxfO9WZz
uZnrWfWHl+XP8vQiSUrD42m41O+jGiEvckQuMwAAAID3AMlqiLyRrjzQJefzo9ZC3ciH8BnqjUhs
7UsUHdlmZ2JoivblbBoie728JzBjfX0GtbLSib7Rl+h5qW3fvd3Trf2h6v+i/X1vP+n9fRHlr75P
hAgAAACm5xrVCCl1fHqkfRO5WpzIkFrTjI5gsxGllU+S+Ltas876WHgaJuVTcU/6SPT6KqmZ17c7
aEedxI9+vXecziERQWj1KarO/djankXbR5FaS7bn0tfI0eCoyJDS2H4+JzVW9vfZ9nxUf7w6T9LH
iGz3AAAAAN7A6hXc2L9yoG70JXgFnaTVjOCD42hdPfNR19+8RhvdnSD8KdzyS+bSISLQx1n9h9AQ
AfW236Ff5jpz+s+wRjaoQcpqeV5q91uyXajeXRzdVabGLwoiRAAAADA912Z1fNBRU61Jep+tr9Bi
B3hiDTbrQ6F8h6SmyTm/9LlwRuKrz6ZcvJG3La9sZCjr41E90zo77gzx4NdNRAhGkI0UVGuQou1T
q++R9fG5OxpO2z4/1Of77xEStz03GqElGHmykeFs/5DVtGb7C1u+0QiQ1WxF30ciRAAAADA9l2VZ
Xr+OrMQIbNRMwr3QpIZIao+qCvA5Nkt81HHTO55a04ZayGEGR3wfef/2KXcZ+U+2z17kw/ZHtl8N
+weZCwpreRfx/c5xRZVGSB2PCBEAAABMz9WOmLyRa3QGkvWtuSXXPC3eGqddw7xcchoiVyO0/Pfv
Vfdv14CVr4U8vudDlHx+vT4fs88ImZnDId7Hg9bLXh+arX2Qwu2laH+zudGsD5LVHEV94WT/+z7f
s60/9Y5ftSLRm7tMaZKIEAEAAMD0uLnMoiMteYKgb473/exMo/V47hqps5stuobbG1HwnkfvGrXa
rdD7PGadkVuIEAHv4/eS1Xi6uUKdBjka6cn2k1mtUVqLZBH3FbRD/KdfSo5X1PMi2z0AAADAD1fP
Z0jNPC7B3DC3JabRic5cvDXLz0A7uaaq1kjfvj8PZ4Sb1eDINWa71n///Xqlxkl8fgrfjKpcZ7MR
1d4BjIxYnCaX3sa5zqpypbVqPL321+Y2ez7bNJ1qN5j1+ftEXER/1qrB/VzH4uw6uy9/RmxWTtT3
v/++Oo5Z6VG5zYgQAQAAwPSscpmp3CBSWyR+Z0esrbsBViNAe76N1larriMaWVE+TFW5bWBJPTcL
Wg3gffy+cm2NxKv+cqtck727+KL93qp/b/QRUpqf1nEJu8wAAAAAHK5eROhyyfkYeD4JVkxh1xhV
7pNsbpTs9XprpKs1YpFDrNWnJ/t5pcESM4msb0SvD8dsnDV3GewTYRhVT87qgzXad6j382LK0Wos
7VVnc1m+H5TVEHka216fOJvrMqoJsrlFs5rgrEZIaX7c44r3SUWSiBABAADA9Hxymb2KHCW9kZ6X
G8WLtLTOgLwcKlFNjafJ8XyAnsER6qgZovK1QCO0zczfgmaDyM5f70fVezLq/bPH5X0eg9od1dpf
ll1fMGeZ14+ufneJ+Q+plauwX2JQC40PEQAAAMAPVztikmuezkziIUZw789vX6Jo7rJb0Cdntea5
OOp9J3fa53hLLPeXPZ5cczXfb9Xw2M+uhqhR05TVEKnn9e0aIzRE55yBRyMkrbnAoruSeiPz1e+f
u+tpVIS704co+/3e3GfR9k7u9n38/h7cnf74MijXpDqepxHyNLWqvY/2X+FIkcgwkc19RoQIAAAA
psf1IfJmDoszgvNmZCtHyuff/65G+qvzO7/zHJxV5Cvq9OrOAJ37yUZW4BycVUNUpRGpem+rNSu9
z2XU7z2ejRGsqvdutNZpFrJO4dlcla27q6u+Z/vdqvPIv0edqYOaJDREAAAAAA4rHyJvZBpd45Zr
gOYwXiRlcXyKPF+GaBb3i3NemYXYjty9XGKOhugZzP12T67BRteIW30tRq/Bn5XT+r4UaURcLZ1T
D9UMeZRmJZtbTjkH90YI3L9vdH8qAtR7fb2an+zxDtdeOb5C3v24f+/MLen59rXmNrMaJtu/qf5O
XY98/tHdZE7FVVo9cpkBAAAAvAdGr1dssfvuzCS8kdZq5OZdwKVvd0E2F8zD0TKFZ7SOlsnL8pud
UUZHzuGIwC2nZcC3KBch8mbaZ7vu6PVntTHq+NHjVWl3eu+v9ferGXPx/VW9d/hqJdvXzl2CrT5E
1dqgrFN4tt8IX48oz+BwpTknKxEiAAAAmJ5rdp/+Tai0V7tOTOhFaS2sj8DbF8jzYfDW5q1vkdX4
LL9fnl7zVblbzAg66lPh+SRlfSNk7rNiDdFsvkPZiOJRuDVqfKK/j35uPV60dFvvb/jvR9/foPdj
dD3eWhNUpSGy7YC3a9rV8AgfIqWhVZqdy6U2t1nr7rYqH8NF/H2lIfIiRUJsR4QIAAAApkdqiFrX
8nrXVN1dGiJXWHjGWeRzonyFZO614BoyHCvSYplVO6Te/yr29g+qKp/Rx5/lfT5KfVL9zfPSppmJ
+vDJDv65bYQv+7sVj1j/nM2BFv07ESIAAACYnmtUM7TSmDQ6VEsfAsfHwTpQ21wqrmbIGblHd3d5
mhnPLyWae81qoGx5X4p9iJ6XMbmCttIQVWlYqn1tzpa7LHv9rZop5WuT9emJ+hK1Ph+psWos373K
v7U+HS0yNFoj1KpJCvc3qn9wrkP5AD2cXKBh3zvnBbW/V5Eq5WMU7T+i77UafyzB47DLDAAAAMAb
KC3L8vprZhKNBKm1uaoZu40UeZocNQNVkbHoDMtzAFUjZXW9Z3E+Hk21FsWLZLRex1mez97alayG
qVqztLf/UPV9VecsQzPURnRlRfVn2X5jFTHp9BFyBwjFPkZe+a3Oe7l0/d69H3FcIkQAAAAwPVc1
0vUiGeFIkaMJimpOXF+dn7/fHd+jh1Gre9evfCTcNeWg74PUIC3Lr5+fSafsqKanOrfP0TVDrVoy
770/i++Quv57Y/lXa7CymqDs8Uf7/GTLN3r86vI/be69jXOZZR2Vs5pU5ROXzTqfLS97Pov9e28u
NFeD5PTPtuLI5yMaerVyRYQIAAAAwAYW3j5Eqyzvzkw4qsEZ5VOUjUysRtSPWAQh6txbpSG6XMbm
kvFG3Edla98V7zxoh2rue7R2plUD1JpLbfT59ir32ejVlGYzPPSWe7b979UYbaUhugT9+bK/I5cZ
AAAAQJKr8v+w3OU//D4Sa93NFdXwuD4/du3S0TQ9nPuRuciKNETR8vF8J9Qarv299XFqHfFXa4hG
a3aiGqH7STUXltGamGy5Z9uBURqi1lxqvbtCwzkNg89L1cuzv7/VmsdoLjHlo6e0iFE/rayPnNIA
qfZe+RTZ/kP6BnZGjC7F/UZ0N7nXDhEhAgAAgOlxc5n1jtxaI0KtM4fmGUeRD1F1OUVH4qvnNnjN
dzR7aR56fWjOUo4qYnGU+1fvQVaL0/q77PFHPZ+j15+zktUARZ3WvZWX0c+hN4K2V/m7A5RgLtXo
SpTSFBEhAgAAgOm5tvqTeGvhnzXXS98ar/LVWe5/X89T5H75aGjEboDez9IXwpvhOff3GdE+286X
Lf+tIwJ7aR6eRbm0jjLjavVhOprmJHu9KqdUdFdpVTbz7P31+kR9m2YoGwHJ/t4+d9UfeO/RYvo/
VwvZ2N95ucrscTzN0PvEo3ybVhEX8TxaM16sdsEJH6Po+d4QIQIAAIDpWfkQPZP793tzkUQjT57D
tfd35ejpziCKdsONnimNmnHtxd7ah1GajqNcd5a977NVI1T9PO1xRjmyf9vz+zZuRRFGtZIQ7riL
dt319ivZ/tUbN0R9g7LjjaxGiQgRAAAATM/VG7mqkZc7chO5SKK+PnbN1s2lYkbqUV8gNZL3fCjc
GZ7Q/GTLI+prpEbwnu+QWnOO5tbpnmnsnCtMnV/l8DvqjDWbm61Kc1K1q0ZqusTxn0W7fEZHWqpz
831bZGir3GS99cy2C17/lfWxUxpYz4fOi+RYDVK2/Gx/19tfRHOhqu+v7veR+/77uokQAQAAwPRc
lmV5/TUSfjbu51+SM+pwbrKiGa4a+S/JEe/Wa7dV1xVdux51X0fVDh01l9lRtU7f5uN0lOdIeY6l
tX/q3eXn/r7REr7ar27UcbLjB+85vZL1JxrRJ0IEAAAA03O1I9XVmpsXcRAamegaXtbXx1tjfD5z
ucSUr8QjeH1Va9QyMiY0QdmZg/c9b83a87HopSp3U2u5qxldVOu2Fd717KVpivoCEc/4u7wuRHx+
bV96NUFe+6pyl9kX2T6frP+V0sBKzZHjQ2SPt/JZcjSj7wqZ9bHrzdBwExG57PFdzVHQ0AsNEQAA
AIAdYC2OhkiNyLIzoqxPQ+/xe2ds0SzHZVnek7vH9vKZqD7v1hqd1vPNrpFp1bygddn3fZ+9nKt2
8V3Erke18qH6G7X7aeXQ/Dz3+xJ1jlb9fNWu3/dxiBABAADA9FztDCWrjXB9fh5/j6zVmqn32Rtp
R3efrTRE3veF5qg1t5k7Y7z/XZ52TdibSciRcKPvxKgZ7Faak94I37fkOmudOUVn0DDm/ZuFKh8h
t5x/jvcQGlcv8pD1IVpd/88P3ueP5rJchA9Rq69Qb38wun+wmiOVi07lTvN2j5PtHgAAAOBfE/FX
ZKTXmrNslMand43Xm5ms7q8zIsOMMVYOKjJTffxRx60+z1GfyzLp+3qU9w/a+ove3VFZzVHzfXiR
js4cYr3XU3W+rIZIHccbj2THLUSIAAAAYHquyl9F+QaoNUdvjVRm/X20jcQ9XwJvxK1yh9n7VLm/
7Mg56oMU9TnKrvFm1+S32sVmZ8reDKx6Blc1M4/m2qrK7TUqcrHVjHdW8GXqi4xkP1+KtH4qN5mq
N9H21OsPPU2p1QxZn6JeTat6Htnzeaw0QcnnEXWuzu5iI0IEAAAA0xPWEKVH+kFHytHZnMvXPoU/
xKjz7HWfvcdpjVxUs3dutL2uj5xZ+4B26JjPI7t7OapR7Y2otvYjWe1rb/9gV3CWe1F/U9Tu92qQ
iBABAADA9Kx8iFRW3yotgRf5uYvdV6tcMEbDY9dYvb97uVxkLjUnN4w9fnZNd3V9wd/3aoi833u+
F6MjRdls0ltphoZff7GzbpUzL9S2cxCLQEQ1juHPIiehqheeZsXLcOBdr9IMef2J5zvk9Ue2va7y
lfucb/m9nKO+e1GNso3kVLfr9nkSIQIAAIDpubx+hkbRXGPemmo0x4i3VuiN/LwR/2ckba/PWdNU
a8bZXGtqTTabm0yOyDfe/dW7pl11nd/mhzNKI5UFLcs+z5dyP9Zzas01ll05uYms91FN6tbtr9s/
2PfcOEBX5xjzQqzV90mECAAAAKbn6kV6PE3Px9en0WlSRZoWRyPk+f5ENTxqzdhev9J6XBo1PZ6v
xMPJsaJ8KdQModeHKLrbTK1hV0Uyjurr0xuZuQ3SEBGR2BY0WWLG35l7MRp5kZrKpO+dqqderiyv
/4nmJvM0pt73rUZ2dMR+1X9f+t6HbC7Uqtx3RIgAAAAA3gO8l1kMVE6OWa1Qb64z5TCtRoo20lSl
IaqaKa0eyEEcpKtngGe5foCKmbOFSN2Y8vba52j7rXaPRTVGUc2R0qDafmBrn6Fs+97b/8vzNeZQ
bT0PESIAAACYnqvKBSJxfBe8kfFKA3Nf/vy+5xukND1RjU9VZMhqaJQjalRjdAs6frf6DskZQDKy
06uBAjgjz2CEYXZ62yurAbqLXJz27xcTEbiLiEY2t6bUHDn908PZRRX10VscH76sT533vDwNk9Vi
eT56US3d6npMAXdHnMT7RIQIAAAApueTy+zlqMcXMYJejcCSa35RB9DoGrHdDSd9gISGKBoZUpGS
2UFDBDAvozWZvbnJopGHqE+R+n5VDrBs+ypXCITvj71e6Zv0SA5ILm3Xn13h8s5DhAgAAACm57ra
jWVGXJegc7TV5NiRsTdTyEaGFvPZzUXz8w+35XcfJbsmHF37Vz5CvbnS1Pmqc5f1fg6PyIkMwQGo
9i2BYPma9ro1MhTNVfbJ9eVEguzvoueLaoY+/YPz/c9n4duWzWWpHLDt+WwkR+22uwm/oZWG6e5o
sJwVHLVLWZVXdiXKriQRIQIAAIDp+WiIVORm9cNLbg0vO3KL5oZRGiJvd4A3UvQiGtVr4FW7rZjh
AsBeVPnNtLaPUR872RE+2/qbZ/K8o/3Zsjk2V9+v2s0VXGHKvkd21598rs44Bg0RAAAAgMP1Fszt
pXZjXYS6X420PwhfIs8XwWqKnm+NzSJ8EMz5rAZoWZzjX3I+Qu7uuEYNkb1eL7fNUTVFAEMiFryf
Q8vTa989TUY2t2O0ffTObyM34XZMRChU++9pYtzcmo6vj4roqOejfud99jRV2f5XRmyeud18F8e/
77W0rSyplSoiRAAAADA9n1xmUmMTHHHJNd2gJsl18HRCS2oE3TpSHUU2az0zYAA4Gr0aoqwPXKvm
NBrpUrnIVprVe7LDFbvBdnuOt1fX71s1QWpcEB1n4EMEAAAAUMS11cHz/Xer+lY+BuncL4/fv/+w
PkKOhsau4S7O+bI+D9k18afjf+EdbwmuOSvfo+pcZkSoTj6jP/nzG615433ui+xEy1/5wFkNkadx
jWpOP39fYj5FFk9DdBERFi+35eL41mXfv2z/5PV/9noezmfv/qzmNrtydHE0ZF6u1Kif1GpX2v3v
8iNCBAAAANMjNUTezCCrOVJrxN7I1tMQXS61M9ysg7T6ffVMmBksAOxNNpdX9HhZDVHUB676eqva
863a8bS/j+lnF+Eovfp+74BkkP9RFCJEAAAAMD1XlXtsNZL9+cLD0Qypz25uFKuZeQ9Q7Rrs0qfO
dyNR7/OJz3bErHwzWjVHl2CkKjvz2Dr3Gew8oz/Y89ta0zbb+z3q+qxm52EiCJ4PkWpvXA2S0/55
GiPXp+33gEc4UqJ83aL9i+3fWtv3qEbV0/R6EZ9VBGiJrVCoDBfhzz/HDedqc1JU9O4+JEIEAAAA
0+NqiFp9GZ5BLVF090F2zbd1hiQdU53yadUcAQAcHXf3V7K/6D2/8qnL7hpU/77qtxojbir7vNsx
P2u0NCtWST+d/jkaYQlGftyIlXNdWR9D7z6yECECAACA6XE1RCq3mfVhiI7U5Jrq8neus2hulbdP
j5e1PuobtPyMcB9O5OouNES3Ik1Rq69QdveczIa85GZkaIqKZ+wn09xkr7/3fnrrT6+vy9nfJ/tZ
RYI8TZGKND0vbT5Fn38I5gLzfNps/9O6mzjaH70/q93UKjdltj1f9e/G1y+78uL9XkaKllcqIuU6
USc1Q9n6TYQIAAAApuejIfIiOi+RzV6N+BbHR0jOPDzsyNv+Xqwpt5KOLH1ZRCIaIQL4xoiJ24Ce
rB5EtSFRDaXSEKmVBxlZUcdvjEBXtWtuPyeOK3OHCS2Nex2jCHfQvz+frE9RdY6yKESIAAAAYHqu
as3NyxWjcod4uUmafQpeZua2mJmHkxuml1unRqg6F9roz70aj+zfiUiMLe+tNUJb+1xVafSUhsP+
/SyR4qgmx4uIrXJRWl8gx6ctHRlalj/L25Mofa476INkf9esqQzm7rTlGdWkeZob93rVbrvG/tbm
Dg1rfOzzWWK70Vb+SEkN0SLebyJEAAAAAAKpIXIjJWYkHJ6hRtXkWQZph+TIHGBCWncjVkUM1fla
d0e6DeVgv5rNnls2MiD6hez5VPsajcTZ43u737LPx/MNCpdn1fvdmoPMqwciB6jUQiW1Q+q+0gOV
zuNG318iRAAAADA912gWejvwXvnwiDX4z2d7BfffR3JqDXC1Zls08q/WKIzWTCh6c6Ft7TvzLZGS
rbJYexqQUe9bNjeTlxvK843JRqKiPi/S98Y5nooYHU1DpJ6nFwlSucpa2wtPg+ruVnv83r94z8/V
KCXby5XPjuNTZD/biIt6v8P9Q7A/8d5jdT9exMR9jsHXOZtbreqzN37IOm8TIQIAAIDp+WiIVK4y
L7DzFCpwmV446bfgRXy8mWbrWr/STEX/juYIhkSG7PucnAn11q/W3XHZepiNiPR+/+sjmsmZs7d7
qvf8UV+jp/DF2yr32Kj3sfz5RvvFYq2uq8ET2iPle1jtr6TeeyJEAAAAMD3X7BqvmnmufAeSvgje
bgG7BnuJ5nKJal2sJsPx3UhHjjp9gKojTVnfoa01RtUz/qoZV9XxW32fPjOwYC6fXo3Rp/qZ3T7V
vkDVGiyVxVxpPFTuQ1U+vZqi3ghWlMwQXAAABqtJREFUc26wYPvv+Q55kX7v/FEH6rtzPcr3Jlq+
K01S8Hk+RMTEO5/nyxd9fqo+h9+vYGQmq5Fad8C5yM9ojZGCCBEAAABMz2VZ/muJ2Zo7JJzbzEGl
SpGO2q0zx0bfDTXzU74Zl5Nri77lPjYrr0atg8x5tPxecbL+Ja3+PdkIxlHKv0pj0vo8d9OYOPW2
N3dZNpfZJbjrb/V+NrY7rdqpbA7P6HXI/q3TVyd6f9HjuxHn4HHs97L32zzAST4/IkQAAAAwPVc1
olMj/5sz8nv/Xa1ZernIPMdRe77bEstNtDi+EmpTnPKJiPq2eGvgXu6bUT4nWQ1Lte+RmjH3+jqN
iixEj+9dv5fb77MW7zjJWh8RV/uz/D3z93yEVO6uJVgfPtVVaAxG5y7zvr84uQ6zmhx7nrDmxClv
5eSdrZ+y/nm+Qk5ustZI0RL8+yp3lYgU3Yvra5rkLmrXlyn4e08T2BwpC2qI3Ot5tml2Liq3W9SJ
WuUyE5oupTkiQgQAAADTs/IhivpEqIFzeu0yqJ5Xv7cz1UdwF8Io36DWNWe1hq1G5r1r3dH7Gp3j
6NuIrpVncyNly1NparpnoMUz9CoN1t7apq00RFXtWXPWevP3retX1t8mGjHJtr/ZdrlVaxPV8kbb
pSofoFZtVbUmK/o7IkQAAAAwPdfqff93MVNRI2svV9JnpiHWEL3cOEqj4/lsKF8kNQNUWZizEYB7
cqbiab6iDtu9GqremW21xik7g2/N9fW8tK1hK1+VKk2bp6nJPp+tc+X15ibrzU3Y+1k9z16fMOWD
lva1+fn7OzeYm1vM8W2rro/R3bu2vbQaPjcSYnzoWnNxuREJJ9TmHS97/qgvVKvGptr3R0a6RH+r
7td9n5znS4QIAAAApuejIcrmklEjPDny90bqagYzaE0z68PRuoYcnhkl16DDuX8afZeqZwRpzctg
TVTVc6u+juj9RiMJq+M9L6n62XredE6oRp+YqvOnn5OzW2gvzZKKpGTr/9HI+t9kNa3R9y7bTrce
X0ZSkpGesghOY3/b7IPUePxoeRMhAgAAgOn55DJ7ePv6hSZG+Z54Mxc5kvVyoQV9CNy/O3yu9+d+
HyL3TmvEJKo5uDRqUO6Oz5JTfN0O4GpGrjQvYQ1KkUbFex5e5C+bCyw6U4/+Xb2/SnOncv81awqc
3EaeT0/Ul2hUZCRa/6IaJunIfF9y73dQgxd9fk8vp5hp7z2ft1YNX+/zzGoCWzWuStOa9b2pilSl
NTrm+u6Nx4vev4q49EbIou1/erdu8H6JEAEAAMD0fDRE2ZGc0ty42BH1I/Z9zxE2nPssGdGJzlR6
NUTZNV7v+rO503rXZFtH/HvTO1PLzojkc3V2lXm7S1rvq/V7sl7ZiMrr711v0RmhpyFM38dGGiLP
idc+1yrC7VPwetIas+L7bM0RJ8v/IepZkQYn2/5X+xe1Rk5Gn7eqPcreXytEiAAAAGB6rspRVPp8
OAPx8Br4/fXnSN7mNlM+Qd7I/pLMxZX18ViV16Uvt5nSaNl/b/XlsN/P5uJSGgT1PmV9YbI+KlEf
D/tCRyNFt+jMyol0uOX1+LV40zNulXvILb/g+/HRCC05TYKrWXqadsGpn4vQ6Hj377Uv0qfMiVSr
B6Ryp1X7DHnP5yF8p7I+YtHvt/pMRXO/qQirt3IR1Qi1fm6OmIv+xI2sRdu3JXf8bK6ycPvUGIkf
5ZOEhggAAADgX/OE0KKc0qisRnDRtevgDLs3l1p2LVT5+lT5eGRz7Kj7kQ88eT+tOWZaqfYNGfW7
8PMR/jTVzzH7exmhaNVGJa/fiyhEI2Dp+hHMcbiVJihLdjdW1X1Ua7JG+VhVtQPRHF+tEZLWdqXX
fyfb/la1y72+T63lGb0uIkQAAAAwPVc1ElO5vtwRc2PuGZWLS2qWzEwoqolw70f4MN3FjEbO5O65
mfetNQu5OZ7SGEV3V0R9XTzNT9TXxctlpzRMqryiPjxeeXqaKhWJi2puWjUL0d1fSqOh3vd0bkE1
cwtGAqIRndZccZ/bMxqcbC6y1kiOKodnMBJ/e+V8v9R12fdFabC8+4n6VKnyyWqyshqT0ZHyap+g
aOS2tX1RvnLZ/qbqfmV9F7nj3hAhAgAAgOkJ5zLL+g71ahyyI+zWSIkaqXv0OhBX5WrK3nfv2nC1
P0925jKKUVqZ7MxQRXhsZCbqp6Jm/l5FHv3+RZ9D+H6Kc94dJbedfH5OhGj0edR5VYQoer3p7yfr
c5VfXJXW5TKo3+jNjVbd36jn0JqLrVV7+//Af1T01EF8IwAAAABJRU5ErkJggg==
