#################################
# ncurses - HelloWorld In Color #
#################################

In your main.c file:
-------------------

    #include <ncurses.h>
    
    int main()
    {
        initscr(); //Creates stdscr
        raw();
        startcolor();
        initpair(1, COLOR_RED, COLOR_BLUE); //Red text and blue highlight
                                            //Note the color pair number
        attron(COLOR_PAIR(1)); //Attribute On for color pair 1
        printw("Hello World"); //Prints "Hello World" in top left corner
        attroff(COLOR_PAIR(1)); //Attribute Off for color pair 1
        
        getch(); //Waits for you to press a key
        endwin(); //Release initscr from memory and close ncurses library
        return 0; //Quit program
    }
    
Replace 'raw()' with 'cbreak()' if you need to be able to use keyboard
shortcuts to quit the program like 'CTRL+Q' and so forth.

Compile using:
-------------

    gcc -o HelloWorldColor "/path/to/main.c" -lncurses
    
Run:
---

    ./HelloWorldColor
