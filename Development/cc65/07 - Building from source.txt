###############################
# cc65 - Building from source #
###############################

All you have to do is make sure you have the basic build from source
code tools, cd into the folder, and run "make." It will then build a bunch 
of libraries and some binaries inside. You can then just run those binaries
as:

   /path/to/cc65/bin/[pickone] [options] file
