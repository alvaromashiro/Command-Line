What is an array?

 An array is a list of data item inmemory that all have the same name
 and whose elements are differentiated by a subscript.

 In simple terms, an array can be anything that is a group of related
 numbers such as prices, phone numbers, catalog order numbers and the
 list goes on and on.

-----------------------

Why do we need them?

 From the beginning of the computer, programmers like to "cheat".  This
 means that they try to make the computer do as much as possible, which
 makes the code for a program much shorter.

 Arrays can be huge, and what person wants to input each individual
 variable.  This is why we need to "cheat" and use arrays to our
 advantage.

-----------------------

Sample code:

this is a sample for the variable (avar)

CLS

PRINT "DEMO FOR ARRAYS"

  FOR avar=1 to 100

    INPUT "What is the product number?"; prodnum(avar):PRINT

    INPUT "What is the product desciption(name)?"; prodscr$(avar):PRINT

    INPUT "are there any more products:(Y/N)"; ans$

    ans$ = UCASE$(LEFT$(ans$,1))

    IF (ans$ <> "Y") THEN

     EXIT FOR

    END IF

  NEXT avar

PRINT "hey, you made it"

PRINT prodscr$(1); tab(20); prodnum(1)

PRINT prodscr$(2); tab(20); prodnum(2)

END

-----------------------

What is the ARRAY ELEMENT in the above code?

  avar is the array variable. This is simply a name that is given to the
  array.  The important information is held in the array number or element.
  1 to 100 signifies how many prodnum and prodscr there are. Towards the end
  of the program, we used this number to take out the 1st and 2nd inputs and
  print them on the screen.

-----------------------

What is a PARALLEL ARRAY?

  A parallel array is an array that has two or more shared informations.  The
  program uses a parallel array between prodnum and prodscr$.  For example
  prodnum(1) = 409 and prodscr$(1)= Huffy Bike.  These two correspond to each
  other.  In your product database or ordering form, the Huffy bike is ordered
  as number 409.  Do you see how they are parallel.  As a final statement, the
  parallel arrays must have the same number of elements(1 to x) and must have
  related value(depending on the users need."

-----------------------
What is a DYNAMIC ARRAY?

  A dynamic array is used for constantly changing data.  This is handled in
  a different format.  Dynamic arrays are another section.  Do not ask me
  how to use them, because I don't do anything with graphics.

-----------------------

submitted by ANTHRAX
  contact him at http://ddi.digital.net/~triplej/calcpage.htm
  or e-mail me at triplej@digital.ne
