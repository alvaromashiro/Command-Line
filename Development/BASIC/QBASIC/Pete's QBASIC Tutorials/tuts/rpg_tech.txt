QB-RPG Techniques,
version 1.0,
by Tal Bereznitskey (tal@fshsoft.cjb.net)
01-09-2000

copyright 2000 to Tal Bereznitskey. i don't know why, but i had to say it...
you can upload it anywhere, just don't change it. Use it, don't Abuse it.
---------------------------------------------------------------------------

1. Introduction
2. Different Map Structure
3. Buy/Sell
4. Special Effects
5. Must Read Files

===============
1. Introduction
===============
In 1995, there was a tutorial about tile-based games that made me think a
lot. it was writen by Greg Taylor, who introduced me to new techniques i
wasn't aware of, and also tons of other ideas. Jason McIntosh, made another
text file like Greg with more ideas, it was also pretty good, and he made me
think about pascal's linked-lists stuff. now i know some techniques of my
own, maybe they are already old and all of you are aware of them, but i
decided to write them down in this textfile-ideasfile so you can check out
some new stuff for your RPGs. the main subject is the map structure, but
there are some other interesting stuff. you can email me thoughts at
tal@fshsoft.cjb.net anytime, even in the middle of the night...

some info:
NPC = Non Player Characters
TILE = sprite
ENGLISH = mine is very bad, so excuse me for that

thanks for reading.


==========================
2. Different Map Structure (keep an open mind, always!)
==========================
What most people do is make a map(100,100) of integers. so if map(x,y) stores
the number 2, the tile we should draw is the second tile. pretty simple.
what about layers, you ask ? no problem they say! map(100,100,3) so we will
have three layers... ain't that simple. but hey, not all (x,y) cordinates
needs the extra layers. and what if one of them needs a fourth layer ?

that's why we should [or: must] use something else. it's a bit more
complicated, but worth it. imagine if you could tell each (x,y) cordinates
how much layers it should have, so that one could have three layers while
the other has only one. it's possible of course.

here is the idea:(if we were using pascal, it would be much much easier with
linked-lists, but here's the answer for QB users).

instead of declaring : DIM map(100,100) as integer
we will declare [war...not this time, maybe later on] : DIM(100,100) as string

"what do you mean strings ?". hey, don't get jumpy on me! here's the basic
idea for it: a string can be converted to a number easily by using ASC(s$)
so it is like using integers, but... the string for each (x,y) cordinate is
not the same length, one can be 3 chrs long and the other 10 chrs long. if
we look at one (x,y) cordinate, we can know how long it is by using LEN(map(x,y))
the length is the number of layers in the (x,y) cordinate. so now we can make
a loop to go thru all the layer in this cordinate. here's the rough coder to
display a (x,y) cordinate of the map:

--CODE--------------------------------------------------------------------
length = LEN(map(x,y))                'get the length of the file
FOR t=1 TO length                     'go thru all the layers
 tile = ASC(MID$(map(x,y), t, 1))     'get a number for the specific layer
 DrawSprite (tile)                    'draw the correct tile
NEXT t                                'make loop

Function DrawSprite(...)
'each one uses his own DrawSprite routine, so i guess you have something
similar to this. just use it as always.
End Function
--------------------------------------------------------------------------

of course there are some bad stuff... the speed goes down. but believe me,
i use this technique and it only reduse the FPS by about five frames. it
ain't that much, and with todays ASM routines for graphics it shouldn't be
a problem at all.

there will need to be more hard work on the other procedures:
IsCordinateMovable(x,y) -> again, we need to go thru the layers of that cord
                           and check that all layers are movable.

now for the messy part. you need to draw the hero(or any other NPC). as i
learned [not along while ago] from a tutorial by FrozenEmu is that "The Hero
is not special !"... we should treat him like anything else in our map, like
a tree or a grass tile.

always make sure you make a camera to the map, so the center of it will be
shown all the time. but, make it so that when your player moves, the camera
locks on him. if he is now on tile(5,4)... center the camera on (5,4) also.
it doesn't matter if you have a tile*tile engine or pixel*pixel, it will
always work with a bit of tunning.

okay... let's put our hero into the map! yes, into the map... what i do is
make him sprite number 255. and when the map is supposed to draw the 255th
sprite, it doesn't and instead draws from the hero%(0) sprite we have.

what you need to code are two routine:

InsertObject(num, x, y, layer)
 'this routine will insert num(an integer) as a chr into the map(x,y)-string.
  the position we will insert is the layer. num will be 255 if we need to
  draw our hero. you can draw NPC with 254 for example. side note: i still
  have no idea how to do NPCs AI inside the game though.

RemoveObject(num, x, y)
 'this routine will search for num inside the map(x,y)-string and will remove
 it.

that's about it. here's the summary:

UPs:
 � Better customizable maps. each cordinate has it's own layers number
 � Saving memory, so we can load bigger maps (even in XMS, optimising
   is still important)
 � Animation (removeobject, insert another object)
 � MultiPlayer games, you can do a RPG for two people, each one gets 3
   minutes on each turn. the new structure is good so when it's your turn,
   the other one is just another NPC for you. think about it
 � now that i think about it, it opens a lot of new doors. what about an RPG
   where you can move from body to body, just by moving next to someone and
   casting a spell. this is of course if the NPCs and the hero have the same
   structure

DOWNs:
 � Slower game... if someone could get around it, please do
 � A bit harder to program

everything really works, tested. if you need source code for anything writen
above, email me and ask.


===========
3. Buy/Sell
===========
RPGs have worlds, worlds have towns, towns have shops... i just want to set
a differnt look on buying and selling stuff.
did you notice that when the one who buys or sells says something is worth
400, you can never get it for less ? why is that ? i don't mean coupons, but
i mean there should be a negotiating system. he says 400, you say 200, he
says 350, you say 250, he say "get lost"... and you can not buy this item
anymore.

these kind of things can make the game more interesting. i never got myself
into the part of buying or selling(i'm stuck in the walkable-demo-state), so
i couldn't do it myself. but you should.

same thing for fights, you can maybe bride the enemies to leave you alone
if you are in a bad state. or maybe they can try and bride you before they
are killed.


==================
4. Special Effects
==================
There are tons(about four or five tons) of QB-RPGs... so to make your game
shine above them, you might wanna think about adding special effects to
your game. here are two ideas, others can be found in Greg's and Jason's
work:

� Tile Animation
     fire, water and many other things can become dynamic(not a qb command)
     inside the game, and break their static state. this can be done by
     altering the palette, but i think this is too hard if you're not an
     artists(i'm not). i suggest adding info to maps at the end, that says:
     on codinate (x,y) these tiles will run: 23, 37, 39 -- then inside the
     game you will rotate the tiles chosen when the (x,y) position is shown.
     (you'll have an array of map places that should be animated, and if one
     or more is on your screen, you will switch the tile).
� Hero Animation
     make it so that if your hero didn't move for a minute, he would do
     something like sit down and rant. try to make your hero alive.
� Day/Night
     many use this, most of them just darkens the palette, i suggest to load
     another tile set(you are using sets, right?), so each set has a night
     and day mode. so in the night, you can make street-light shine.


==================
5. Must Read Files
==================
Here's the list of files you should get:

 � Greg Taylor - tile based games v1.2 (1995)
 � Jason McIntosh - tile graphics techniques v1.0 (1996)
 � Tsugumo - so you want to be a pixel artist ?
 � FrozenEmu - an article(faq/tutorials) about RPG and scripting


=================
X. Secret Section
=================
oh no, the secret [section] is out in the open. oh well, i'll tell you all
my secret contact information.

Name -> Tal Bereznitskey
Age -> 18
Email -> tal@fshsoft.cjb.net
FSH-Soft Site [software for EA Sports games] -> http://fshsoft.euro-ball.com/
