'Simple encryption by Nick Emblow 2002
'I would say something like, "Use this code but give me credit"
'but im pretty sure someone else could figure this out too :-)

INPUT "Enter string to encrypt:", write$

'Encryption routine - takes the ascii values multiplys them by 3/2 and returns
'the new ascii character for that value. i.e. encryption.
FOR i = 1 TO LEN(write$)
    crypt$ = crypt$ + CHR$(ASC(MID$(write$, i, 1)) * 3 / 2)
NEXT i

PRINT "Crypted:";
PRINT crypt$

'Same shit except its backwards - instead of multiplying by 3/2 you
'divide by 3 * 2.

FOR i = 1 TO LEN(crypt$)
    ucrypt$ = ucrypt$ + CHR$(ASC(MID$(crypt$, i, 1)) / 3 * 2)
NEXT i

PRINT "Unencrypted:";
PRINT ucrypt$

