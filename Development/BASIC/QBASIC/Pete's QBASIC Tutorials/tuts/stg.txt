Self-taught and glad By Dab

 The arrays tutorial is partially helpful to those who want to mess with data and other things like that, I just wanted to cover some more, hopefully this will be helpful information.
 
 Note: None of this code has been tested.

  This should work for Qbasic, QuickBasic 4.5 or QuickBasic 7.1 PDS, I'll just refer to all three as the "interpreter", ok?

 You know how to DIM a statement in the interpreter, Right?

 Ok, in the arrays tutorial it refers to a parallel array, I just call it an array with dimentions.

 I'm not planning to get into how these work in memory, cause I have no idea how they work in memory, neither do I need, or want, to know how.

 Say you dimention something to store within 50 something dimentions.

IDEA 1: 
 (Number of chrs allowed in a string)(32767 - 1(for compensation)) * 50(or the number your dimentions the array has) = 1638300 actual charactors, if you decided to use an entire 32767 charactors of each string, you would be able to store ALOT of things.

 I'm planning on utilizing this in a little while in my compiler/scripter, but havn't gotten around to it just yet.

 You can parse, or seperate things by a single charactor, and do this quickly, strings are fairly quick I have noticed, for storing things.

 I don't use this method, yet, as i've told you, so i havn't done alot on it.

IDEA 2:

 DIM Array$(50)
 ' Make an array with fifty dimentions.
 
 I Use arrays to store Commands, paremeters to the    commands, variables(Numbers, strings, commands, subs),
sub storing locations and whatever else.

 Scope, is a main factor in my program, do you know what scope is?
 
 Interpreters have a few scope for variables.
CONST, COMMON & SHARED are all scope keywords, the one that I am interested in is SHARED.

 DIM SHARED Array$(50)

 The SHARED keyword allowes your subs and functions to access this Array called Array$, simple huh?

-Try this:

 DIM SHARED Array$(2)
 Array$(0)="Array Subscript(or dimention) one"
 Array$(1)="Array Subscript(or dimention) two"
 Array$(2)="Array Subscript(or dimention) three"
 
 SUB StateStuff
  ? Array$(0)
  ? Array$(1)
  ? Array$(2)
 END SUB

-End code.

 There is another way to use this SHARED, with out using SHARED.

 You know that you can put variables that go to the sub, for it to act on, right?

-Like this:

 Sub Write(A$)
  ' Prints A$ to the screen
  ? A$
 End Sub

-End code.

 That is what I call an input variable, from the subs point of view anyway.

 Let's say you wanted to add the letter "a" to any string for some reason or another, ok?

 You could just make function like this:

 ? Add.an.a$("I need ");
 ? "cd-burner"
 
 Function Add.an.a$(Strng$)
  Add.an.a$ = Strng$+"a"
 End Function

  Or:

 A$ = "I need "
 Add.an.a (A$)
 ? A$;
 ? "cd-burner"
 
 Sub Add.an.a(Strng$)
  Strng$ = Strng$ + "a"
 End Function
 
 End code.

 That's what I call an output variable sub.

 Now you could make a decision based on another variable, like this:

 Sub Decide(Strng$,After$)
  Select case UCASE$(MID$(After$,1,1))
  case "A","E","I","O","U": 
   Strng$ = Strng$ + "an"+After$
  case else: Strng$ = Strng$ + "a"+After$
  End select 
 End sub

 End code.

 Remember dimentioned arrays?
 Those can be inputed to an array too, like so:

 Sub Demo(A$())
 End sub

 End code.

 Now with that, you can choose which order the user should put the variables, say dimention 0 is the beginning sentence, and 1 is the deciding factor.

  Sub Decide(A$())
   Select case UCASE$(MID$(A$(1),1,1))
   case "A","E","I","O","U": 
    A$(0) = A$(0) + "an" + A$(1)
   case else: A$(0) = A$(0) + "a" + A$(1)
   End select 
  End sub

The same sub inputed and outputed to one array, simple enough huh?

 Another thing, this might be helpful.

 Qb1.1 won't work with this code, sorry.
   
 I've heard the word dynamic, which means changing, or changable.
 So think of this, static is the opposite, not changing.

 Static is the way basic works with variables in the first place, and also with dimentioned arrays.

 The '$DYNAMIC meta-keyword is a great command, it's the only way to use the REDIM statement too.

 Check this out:
 '$DYNAMIC
 Dim A$(0)
 A$(0) = "Something important" 'Remember this when i  'mention it next.
  
 Sub AddSomething(V$())
  REDIM V$(3)
  V$(0) = "Statement one"
  V$(1) = "Statement two"
  V$(2) = "Statement three" 
  V$(3) = "Statement four"
 End sub

 End code.

 That's not exactly adding, or appending as i call it, cause if your trying to use that while somethings stored, cause it deleted "Something important", even if you don't write anything in dimention 0, it will just be nothing, cause you REDIMMED the array, deleting everything written made prior to that.

 For those of you who have some knowledge of vbWin, this is similar to the PRESERVE keyword.

 '$DYNAMIC
 Sub (A1$(),SA,A2$(),SOA)
  ' A1$ Or Array 1
  ' SA Or Start At
  ' A2$ Or Array 2
  ' SOA Or Size Of Array (2)
  DIM Temporary$(SA) 'Make a temporary array
  ' to store the contents of Array 1 up to Array 2s
  ' Start At point.
  '
   
  DO ' Begin the preserving loop
   Temporary$(X) = A1$(X)' Add Dimention X to Temporary
   X=X+1 ' Add one to X 
  LOOP UNTIL X=SA 'Stop at Array 2s Start At point
  X=0 ' Reset X
  REDIM A1$(SA+SOA)' Redimention Array 1 to store the
  'part of array 2 that you want.
  
  DO ' Begin the secondary preserving loop
   Temporary$(X) = A1$(X)' Add Temporary Dimention X to
   ' Array 1 again
   X=X+1 ' Add one to X 
  LOOP UNTIL X=SA 'Stop at Array 2s Start At point again
  
  X=SOA ' Set X to the starting point.
  
  DO ' Begin the appending loop
   A1$(X) = A2$(X)' Add Temporary Dimention X to
   ' Array 1 again
   X=X+1 ' Add one to X 
  LOOP UNTIL X=SOA 
                  'Stop at Array 2s Start At point again 
 End sub
 
 End code. 

 
 Hope i got my point across.

 