                     Vic's QBasic Programming Tutorial

                           Basic Tutorial XVII

   Qbasic is C++,JAVA,PASCAL,FORTRAN,PERL,and 5 million other languges...

                            What did he say?

-----------------------------------------------------------------------------

Here is a bit of my thoughts, skip this if you find it too boring...

Its been a while, Tutor #17 is more or less my complaint that people who
program in other languages and hate QB are crazy...

There are a few facts that make sense to why you would not Use Qbasic when
it comes to MS DOS...

1.) many languges compile thier code so it will run much faster and more
    orginized...
2.) Some languages use more advanced ways to get the same results making it
    Easier to handle more Advanced things...
3.) Many other languages support PORTING better... So instead of writing the
    same code over and over in order to put it on a different operating
    system you can just compile it on a different compiler...
4.) Some languages are harder to use making the person programming in it to
    feel better about themselves when they use it...

That in my opinion is the only reasons to use other languages...
but as the qbasic community advances more and more each year these seem to
be dissapearing...

1.) The use of ASM in Qbasic organizes the code and greatly speeds up the
    commands...
2.) Same thing goes for #2
3.) Porting is still a problem, but with EMULATION going full scale and
    covering many operating systems, porting will only be a matter of
    finding an emulator to run the program!  I use Qb in LINUX with a
    DOS emulator all the time!!!
4.) Programming in other langs makes for a great headache sometimes, using
    qb can sometimes be relaxing...


Some of the bigger problems that people run into when using QB is that
in order to do great unimaginable things you sometimes have to dabble a bit
in ASM...  The problem with that is learning ASM in order to use the
commands...
Well, thanks to the Internet, you don't have to learn ASM...
If you havn't heard, then this tutorial is way out of line for you to even
bother listening too...

There Are many many programming libraries out there...
And you know what?  There is a way to convert any programming library from
any other programming language into something that QB can use...

if you have no Idea what I talking about read my older tutors!!

so, basically QB has no known limitations!!!
Memory is no longer a problem, and thats what C++ users have been spending
time bitching about...

Another thing that C++ users has been complaining about is that it can't
keep with the times...

Emulation has been the THING to do with DOS and programming...
For a while no one even bothered to try it,
but Now, there are 2 NES emulators that I know that were made in QB!!!
I myself am working on a GB emulator (very tedious)...

Emulation is possible...
What isn't?  when it comes to DOS that is...


----------------------------------------------------------------------------

If you havn't tried a different programming lang I know what your thinking...

If it is so much the same, how can you convert QB into other langs?

Here are some examples...

--------------
C++

Here is a BASIC qBasic program...

'--- start
01 print "dude"
'--- done

Here is what it would look like in Turbo C++ from Borland...

'--- start
#include <stdio.h>

int main(void)
{
printf ("dude");
return 0;
}
'--- done

same effect, 3 times as long(crappy QB example, but it would be very very
long and confusing...)

---------
Pascal


QB example...

'---start
SCREEN 13
FOR y = 1 TO 200: FOR x = 1 TO 320
PSET (x, y), y
NEXT: NEXT
'finish

TURBO PASCAL...

'------start
uses crt;

PROCEDURE Screen13;
BEGIN ASM MOV AH,0; MOV AL,$13; INT $10; END; END;

PROCEDURE Pset(pix2set_x, pix2set_y : Word; pix2set_c : Byte);
BEGIN Mem[$A000 : pix2set_y * 320 + pix2set_x] := pix2set_c; END;

var
x : integer;
y : integer;
begin
screen13;
for y:=0 to 200 do begin
for x:=0 to 320 do begin
pset (x,y,y);
end;
end;
readln;
end.
'----finish



look at that, you have to tell the computer ahead of time before you can
use the arrays!!!

in these langs, you can use libraries to cut down on code...
I just wanted to show you both examples withought libraries...

-----------------
Disecting the Languages...

Look at some of the commands in both C++ and PASCAL...
expesially the FOR loop...

For y:=0 to 200 do begin
end;

that is a basic pascal For loop...
and here is what it looks like in qb

for y = 0 to 200
next

not that hard if you really think about it...


------------------------------


Wow, I seem to be going nowhere with this don't I?

um,,,
Ok, here we go...
...
wait, lost it...

I guess I am just saying, 
1.) don't be afraid to stick with QB
2.) try other langs before you diss them...
3.)
when you draw a picture that takes 5 minutes and it looks good, it makes
you feel good,,,
when you draw the same picture but takes 50 minutes it makes you feel
good, maybe even better than good,,,
But it can also make you feel you wasted time,,,
and, while your drawing for 50 minutes you have time to think of stuff to
add...

Do you get #3?
(i don't, but it sounds good and was told to me from an old QB mentor, who
now only programs in Windows ASM and C++, and I might add that that was the
only thing I learned from him...  But I don't get it, so maybe what I am
writing is just a big waste of time... ahem......)

where was I?
oh yeah... nowhere...

Qbasic is a friend of Apathy...
If I had to program my own games in C or ASM then I wouldn't even bother...
I sometimes leave a QB project undone, but If I programmed in ASM all the
time I wouldn't of even started them......

Thank you for listening and letting me waste your time...

----------------------------------------------------------------------------
****************************************************************************
----------------------------------------------------------------------------

   File  Edit  View  Search  Run  Debug  Calls  Options                   Help
+--------------------------------- Untitled -------------------------------�+-+
�Thats it for this tutorial, If I didn't get into enough detail in the         
�explanations then just look at the source code and try to figure it out
�on your own.  All else fails E-Mail Me...  I want to make these tutorials     _
�as easy to understand as posible!!!                                           _
�                                                                              _
�My current E-Mail address is RADIOHANDS@AOL.com                               _
�                                                                              _
�If you are using this tutorial on your page, please leave the tutorial        _
�exactly as it is... please don't change anything, unless its spelling         _
�errors... Theres alot of them! I don't like using the backspace key...        _
�                                                                              _
�The original website that these were on is                                    _
�                                                                              _
�http://members.aol.com/radiohands/index.html                                  _
�                                                                              _
�Thank you                                                                     _
�Vic Luce                                                                      _
�Finished                                                                      _
�May 23                                                                       _
�2000                                                                          _
�                                                                              
� ___________________________________________________________________________�
 <Shift+F1=Help> <F6=Window> <F2=Subs> <F5=Run> <F8=Step>     �     N 00001:008


...
If you want to be notified when a new tutorial is out..
Send An E-mail to RADIOHANDS@AOL.com
with the subject saying VQBLIST and then your E-mail address(check website)



