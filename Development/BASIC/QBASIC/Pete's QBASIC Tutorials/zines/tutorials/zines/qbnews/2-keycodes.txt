     The QBNews                                                     Page 38
     Volume  1, Number  2                                 February  2, 1990



     ----------------------------------------------------------------------
                                S w a p   S h o p
     ----------------------------------------------------------------------

     Extended Key Codes Made Easy
     
     Detecting  various key codes in QuickBasic couldn't be easier,  given
     the  SELECT  CASE construct.  In my QB programs I usually  INCLUDE  a
     file called, FKEYDEFS.BAS, which consists of the following lines:
     
     =====================================================================
         ' Special Key Assignments
     
         UpKey$ = CHR$(0) + CHR$(72)
         DownKey$ = CHR$(0) + CHR$(80)
         LeftKey$ = CHR$(0) + CHR$(75)
         RightKey$ = CHR$(0) + CHR$(77)
         PageUp$ = CHR$(0) + CHR$(73)
         PageDown$ = CHR$(0) + CHR$(81)
         HomeKey$ = CHR$(0) + CHR$(71)
         EndKey$ = CHR$(0) + CHR$(79)
         InsKey$ = CHR$(0) + CHR$(82)
         DelKey$ = CHR$(0) + CHR$(83)
         EnterKey$ = CHR$(13)
         TabKey$ = CHR$(9)
     
         sTabKey$ = CHR$(0) + CHR$(15)
     
         cHomekey$ = CHR$(0) + CHR$(119)
         cEndKey$ = CHR$(0) + CHR$(117)
         cPrtSc$ = CHR$(0) + CHR$(114)
         cLeftKey$ = CHR$(0) + CHR$(115)
         cRightKey$ = CHR$(0) + CHR$(116)
         cPageDown$ = CHR$(0) + CHR$(118)
         cPageUp$ = CHR$(0) + CHR$(132)
     
         ' Function Keys
     
         F1Key$ = CHR$(0) + CHR$(59)
         F2Key$ = CHR$(0) + CHR$(60)
         F3Key$ = CHR$(0) + CHR$(61)
         F4Key$ = CHR$(0) + CHR$(62)
         F5Key$ = CHR$(0) + CHR$(63)
         F6Key$ = CHR$(0) + CHR$(64)
         F7Key$ = CHR$(0) + CHR$(65)
         F8Key$ = CHR$(0) + CHR$(66)
         F9Key$ = CHR$(0) + CHR$(67)
         F10Key$ = CHR$(0) + CHR$(68)
         F11Key$ = CHR$(0) + CHR$(133)
         F12Key$ = CHR$(0) + CHR$(134)
     
         ' Shifted Function Keys
     
         sF1Key$ = CHR$(0) + CHR$(84)
         sF2Key$ = CHR$(0) + CHR$(85)
     
     The QBNews                                                     Page 39
     Volume  1, Number  2                                 February  2, 1990

         sF3Key$ = CHR$(0) + CHR$(86)
         sF4Key$ = CHR$(0) + CHR$(87)
         sF5Key$ = CHR$(0) + CHR$(88)
         sF6Key$ = CHR$(0) + CHR$(89)
         sF7Key$ = CHR$(0) + CHR$(90)
         sF8Key$ = CHR$(0) + CHR$(91)
         sF9Key$ = CHR$(0) + CHR$(92)
         sF10Key$ = CHR$(0) + CHR$(93)
         sF11Key$ = CHR$(0) + CHR$(135)
         sF12Key$ = CHR$(0) + CHR$(136)
     
         ' Control Function Keys
     
         cF1Key$ = CHR$(0) + CHR$(94)
         cF2Key$ = CHR$(0) + CHR$(95)
         cF3Key$ = CHR$(0) + CHR$(96)
         cF4Key$ = CHR$(0) + CHR$(97)
         cF5Key$ = CHR$(0) + CHR$(98)
         cF6Key$ = CHR$(0) + CHR$(99)
         cF7Key$ = CHR$(0) + CHR$(100)
         cF8Key$ = CHR$(0) + CHR$(101)
         cF9Key$ = CHR$(0) + CHR$(102)
         cF10Key$ = CHR$(0) + CHR$(103)
         cF11Key$ = CHR$(0) + CHR$(137)
         cF12Key$ = CHR$(0) + CHR$(138)
     
         ' Alt Function Keys
     
         aF1Key$ = CHR$(0) + CHR$(104)
         aF2Key$ = CHR$(0) + CHR$(105)
         aF3Key$ = CHR$(0) + CHR$(106)
         aF4Key$ = CHR$(0) + CHR$(107)
         aF5Key$ = CHR$(0) + CHR$(108)
         aF6Key$ = CHR$(0) + CHR$(109)
         aF7Key$ = CHR$(0) + CHR$(110)
         aF8Key$ = CHR$(0) + CHR$(111)
         aF9Key$ = CHR$(0) + CHR$(112)
         aF10Key$ = CHR$(0) + CHR$(113)
         aF11Key$ = CHR$(0) + CHR$(139)
         aF12Key$ = CHR$(0) + CHR$(140)
     
         ' Alt other keys
     
         alt1$ = CHR$(0) + CHR$(120)
         alt2$ = CHR$(0) + CHR$(121)
         alt3$ = CHR$(0) + CHR$(122)
         alt4$ = CHR$(0) + CHR$(123)
         alt5$ = CHR$(0) + CHR$(124)
         alt6$ = CHR$(0) + CHR$(125)
         alt7$ = CHR$(0) + CHR$(126)
         alt8$ = CHR$(0) + CHR$(127)
         alt9$ = CHR$(0) + CHR$(128)
         alt0$ = CHR$(0) + CHR$(129)
         altHyphen$ = CHR$(0) + CHR$(130)
         altEqual$ = CHR$(0) + CHR$(131)
     
     The QBNews                                                     Page 40
     Volume  1, Number  2                                 February  2, 1990

     
         altQ$ = CHR$(0) + CHR$(16)
         altW$ = CHR$(0) + CHR$(17)
         altE$ = CHR$(0) + CHR$(18)
         altR$ = CHR$(0) + CHR$(19)
         altT$ = CHR$(0) + CHR$(20)
         altY$ = CHR$(0) + CHR$(21)
         altU$ = CHR$(0) + CHR$(22)
         altI$ = CHR$(0) + CHR$(23)
         altO$ = CHR$(0) + CHR$(24)
         altP$ = CHR$(0) + CHR$(25)
     
         altA$ = CHR$(0) + CHR$(30)
         altS$ = CHR$(0) + CHR$(31)
         altD$ = CHR$(0) + CHR$(32)
         altF$ = CHR$(0) + CHR$(33)
         altG$ = CHR$(0) + CHR$(34)
         altH$ = CHR$(0) + CHR$(35)
         altJ$ = CHR$(0) + CHR$(36)
         altK$ = CHR$(0) + CHR$(37)
         altL$ = CHR$(0) + CHR$(38)
     
         altZ$ = CHR$(0) + CHR$(44)
         altX$ = CHR$(0) + CHR$(45)
         altC$ = CHR$(0) + CHR$(46)
         altV$ = CHR$(0) + CHR$(47)
         altB$ = CHR$(0) + CHR$(48)
         altN$ = CHR$(0) + CHR$(49)
         altM$ = CHR$(0) + CHR$(50)
     
     All  of  the  variables  that start with  'a'  (such  as  'aF10Key$')
     represent the code for the ALT-Key stroke.  Variables with 's'  (such
     as  'sF12Key$'  are  for the shifted-Key stroke,  and  'c'  (such  as
     'cF1Key$') for the Control-Key stroke.
     
     Now,  in  your program where you want to detect and act  on  specific
     keys, use the following:
     
        DO: x$ = INKEY$: LOOP WHILE x$=""
     
        SELECT CASE x$
           CASE F10Key$
              ' Do something when F10 is pressed
           CASE sF10Key$
              ' Do something when Shift-F10 is pressed
           CASE aF10Key$
              ' Do something when Alt-F10 is pressed
           CASE altX$
              ' Do something when Alt-X is pressed
           CASE UpKey$
              ' Do something when the Up arrow key is pressed
           CASE PageDown$
              ' Do something when the Page Down key is pressed
           CASE "P"
              ' Do something when the "P" key is pressed
     
     The QBNews                                                     Page 41
     Volume  1, Number  2                                 February  2, 1990

           CASE ELSE
              ' Do something if none of the previous keys were pressed
        END SELECT
     
     This arrangement makes it particularly easy to act on any key stroke,
     and  has  the  added  benefit that your program  is  also  MUCH  more
     readable.
     
     William R. Hliwa, Jr.
     Clinical Assistant Professor
     Department of Medical Technology
     State University of New York at Buffalo