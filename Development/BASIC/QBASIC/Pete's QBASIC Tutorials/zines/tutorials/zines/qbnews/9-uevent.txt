     The QBNews                                                     Page 32
     Volume  2, Number  4                                 December 22, 1991

     The UEVENT Bug by Ray Crumrine
     
     While experimenting with a third party library product that uses
     SetUevent, I discovered the following bug in the QB (V4.5) UEVENT
     routines.  Microsoft assured me the bug has been fixed in BC7,
     although I have no way to test it.
     
     If you are trying to use ON TIMER and ON UEVENT in the same program,
     it probably won't work. If you type in the following code and run it
     you will find that once you press a key to CALL SetUevent, from that
     time on when the TIMER(5) seconds passes, QB jumps first to the
     section of code specified by ON TIMER(5) GOSUB, then IMMEDIATELY jumps
     to the section of code specified by ON UEVENT GOSUB! 
         
     The only way I could find to stop this was to use UEVENT OFF at the
     end of the user event section of code. If your user event is a one
     shot deal then you can work around this way. Otherwise, if your user
     event needs to stay enabled, you're probably out of luck. One other
     possible work around is for your user event to include code to
     determine if it is being called uselessly, then simply RETURN
     immediately. Besides being a kluge, this still wastes time jumping to
     the uevent code every time the TIMER goes off. A better work around is
     to NOT use ON TIMER and simply keep track of ElapsedTime using your
     own routines. This is fairly simple to do, and I have some code if
     anyone would like to look at it. 
     
     I check into this BBS once in a while, or you can write me at:  
     
       Ray Crumrine
       1800 Hilltop
       Quincy, Il. 62301                              
     
     Note that you must load the QB.QLB quick library to run this code.
     
     DECLARE SUB SetUevent ()              'In QB.QLB
     ON TIMER(5) GOSUB TimerEvent
     ON UEVENT GOSUB UserEvent
     CLS
     PRINT "Press any key to trigger the user event"
     TIMER ON
     UEVENT ON
     DO
       k$ = INKEY$
       IF LEN(k$) THEN
         IF k$ <> CHR$(27) THEN
           CALL SetUevent
         END IF
       END IF
     LOOP UNTIL k$ = CHR$(27)                
     END
     
     TimerEvent:
       PRINT "In the timer event section"
       RETURN
     
     The QBNews                                                     Page 33
     Volume  2, Number  4                                 December 22, 1991

     
     UserEvent:
       PRINT "In the user event section"
       'UEVENT OFF here will break the chain
       RETURN