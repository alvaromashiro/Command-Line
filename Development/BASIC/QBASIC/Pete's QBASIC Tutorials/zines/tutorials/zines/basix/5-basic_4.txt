______________________________________________________________________________
| SECTION 1 PART B SUBPART 1 | Basic Tutorials - Part 4 |
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 BASIC Tutorials - installment 4
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 In the last instalment I showed you the following piece of code. Assume
 that n$ is holding someones name and that age% is holding the value of
 someones age.

         PRINT "Hello, ",n$
         PRINT "You are ",age%," years old"

 Let's say n$="Ben" and age%=32 .. that piece of code would print the
 following onto the screen:

 Hello, Ben
 You are 32 years old

 Lets go back to the code and look at it bit by bit:

         PRINT "Hello, ",n$

 We can understand the PRINT, we know that the print command will display
 the information produced from the expression following it onto the screen.
 We can also recognise the word 'Hello' (if we know English) next comes a
 comma which is actually in the text and then inverted commas " signify the
 end of the readable text.. Next comes a comma and then a variable name, this
 basically means that the next thing to print is the value of n$.

 Think about it like a list. You can make lists by putting commas between
 each item.. for example:

 My friends names are Fred, Bill, John, Sam, Josh and Bert.

 You can put lists in the PRINT command too and that is what the comma before
 the n$ is signifying.

 PRINT "Hello, ",n$

 Basically what this means is print the text in the inverted commas and _then_
 display the value of n$.
 Onto the next line:

          PRINT "You are ",age%," years old"

 This should be simple to understand now. The number of things to display in
 the list is now 3. The words 'you are', the variable age% and the words
 'years old'. It's simple!

 Sorry if it was going incredibly slow there but now there is no excuse not
 to understand! :)

 [Assignment]

 Write a program that clears the screen and then asks for the year that you
 were born in. Then have it work out how old the person is and then displays
 it like so:

 You were born in (year%) and that means that now you are (age%) old.







--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #5 from April 1996.