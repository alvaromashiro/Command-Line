| SECTION 1 PART A SUBPART 1 | GFX Colliding! |
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Creating an Audio-CD Player in BASIC (VBDOS/QB4.5)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Part 1:  Introduction and Getting System Information

Hello everybody!

Before I begin this guide I would like to take this moment and introduce
myself to you. My name is Marco Koegler and I'm a student at the University
of Missouri-Rolla. Programming in BASIC is a little hobby of mine and just
recently I figured out how to do something really neat. Well, from the title
of this thing you probably guessed it, I found a way to access the audio
capabilities of my CD-ROM drive. Since it isn't that hard (once you find the
right documentation) I thought I might share my wisdom (or lack thereof) in 
the fanzine. Since space is limited, I will only explain the bare essentials. 
Don't worry, even if you don't know anything about interrupts, registers and 
other `complicated' stuff, following this guide closely will provide you 
with a working program. For those, who want to go beyond the scope of this 
guide and create other nifty little CD functions I recommend the document 
MSCDEX21.ZIP (Archie or SimTel should locate this).  Since most of these 
functions are very similar to the basic types coverd in the guide, the docs 
should provide you with enough knowledge to do what you want to do.

Another issue we should get out of the way is the style of the program. It is
very repetitive, but only because I don't want people to cross-reference 
several fanzines just in order to find a certain function. I recommend that,
once the guide is finished, you plug the different types of calls into 
SUBs or FUNCTIONs anyway. Nonetheless I will use two functions, because 
otherwise the code would be a lot more messier than it already is. They are
as follows:

FUNCTION hbyte% (word%)    'Returns the higher byte of a 2 byte integer
IF word% >= 0 THEN         'Even takes care of negative integers
   hbyte% = word% \ 256
   ELSE
   hbyte% = (65536 + word%) \ 256
   END IF
END FUNCTION

FUNCTION lbyte% (word%)    'Returns the lower byte of a 2 byte integer
IF word% >= 0 THEN         'Even takes care of negative integers
   lbyte% = word% MOD 256
   ELSE
   lbyte% = (65536 + word%) MOD 256
   END IF
END FUNCTION

Before we begin coding this prog, please make sure that the standard QLB is
loaded (i.e. start VBDOS or QuickBasic4.5 with /L switch). We are going to 
need this in order to get the CALL INTERRUPTX routine and RegTypeX. Now, that
you have started the editor and written the two functions above, we have to
include the definition file and declare some variables:

' $INCLUDE: 'vbdos.bi'                    'For VBDOS
DIM SHARED inregsx AS RegTypeX            'SHARED optional, but if you are
DIM SHARED outregsx AS RegTypeX           'going to use it in SUBs leave it!
' $DYNAMIC                                '<- So we can REDIM arrays

All, right. Now we are ready to make our first attempts to `talk' with MSCDEX.
See, the program MSCDEX.EXE defines some nice interrupt functions which we 
will use. All the functions are located on interrupt &H2F and are referenced
by the value in the AX register. For example, the first function we will use
determines the MSCDEX version number and it is referenced by a value of 
&H150C in the AX register. From the documentation, we gather that after the
interrupt is called, the version number will be stored in the BX register.
Like this:  
            1st byte/High byte = Major Version Number
            2nd byte/Low byte  = Minor Version Number

This then results in the following code snippet:

' Determine MSCDEX Version
inregsx.ax = &H150C                       'Referencing the CD function
inregsx.bx = &H0                          'Set BX to zero; Just in case!
CALL interruptx(&H2F, inregsx, outregsx)  'Call interrupt &H2F

IF outregsx.bx = &H0 THEN                 'We are in trouble if this is true
   PRINT "No Valid MSCDEX Version!"       
   END
ELSE
   major$ = LTRIM$(STR$(hbyte%(outregsx.bx)))   'Extract number and store it
   minor$ = LTRIM$(STR$(lbyte%(outregsx.bx)))   'Extract number and store it
   PRINT "MSCDEX Version "; major$;"."; minor$;" is installed on your system!"
END IF

Well, as you can see if the BX register contains a zero value the program will
terminate. 

NOTE: The register only contains a non-zero value if MSCDEX Version 2.0 or
      higher is installed on your system!

All right! We have just written our first piece of code for our CD-Player 
program. But, since this is already getting too long, I'll have to stop here.
In the next part we'll get the first CD-ROM drive number and check whether it
is supported by MSCDEX. Then, we are going to take a look at our first call,
which will use a command block/buffer. See you guys next month!

Note from Peter: I already have all the parts of this series so there is no
danger of it NOT appearing in next issue, it will.. :)







--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #5 from April 1996.