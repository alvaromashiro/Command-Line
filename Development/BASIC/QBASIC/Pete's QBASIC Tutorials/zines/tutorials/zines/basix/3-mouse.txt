-------------------------------------------------------------------------------
- SECTION TWO PART 2 - (Programming the mouse) --------------------------------
-------------------------------------------------------------------------------

 This is programming the mouse by : peter@trenham.demon.co.uk

 Programming the mouse in QBasic is a very tedious affair although it is 
 possible but I cannot describe how to do it before this fanzine is meant
 to be out! So I am going to explain how to program the mouse in QuickBasic
 4.5 and PDS 7.1.

 To program the mouse we need to use interrupts. If you want a more indepth
 view into these then you will have to read the section on 'Interrupts in
 depth' next issue... patience...
 
 To be able to use interrupts you need QuickBasic or PDS 7.1 (both from
 Microsoft) and I think PowerBasic may also allow you to do this but I cannot
 be sure. On top of that you need a 'QB.BI' or 'QBX.BI' file. You get QB.BI
 with QuickBasic 4.5 and you get 'QBX.BI' with PDS 7.1. Substitute QBX.BI
 in my programs for whatever your one is called. 

 To start your program off you need to do this:

 '$INCLUDE: 'qbx.bi'
 Dim shared inregs as RegType
 Dim shared outregs as RegType

 SUB mousecommand (ax%,bx%,cx%,dx%)
 inregs.ax = ax%
 inregs.bx = bx%
 inregs.cx = cx%
 inregs.dx = dx%
 CALL INTERRUPT (&H33,inregs,outregs)
 END SUB

 Now, we have the definitions set up and we have a subroutine that gives the
 mouse driver commands. Beware, this subroutine does not return anything from
 the mouse... try and figure out how to do it.. if you can't then you'll have
 to keep reading. So now, what are the commands? Well here goes... here is
 a list of some of the mouse commands:

 AX = 00               (Reset Mouse)
 AX = 01               (Show Mouse Pointer)
 AX = 02               (Hide Mouse Pointer)
 AX = 03               (Get Mouse position and button status)
   - returned values:
		in BX  =   0 no buttons down
			   1 left button down
			   2 right button down
			   3 both buttons down
		in CX = x position
		in DX = y position

 Right! Those commands should get anyone started! ;-) So lets add some more
 to the main part of our program.

 mousecommand 1,0,0,0

 That line (when run) would display the mouse pointer onto the screen. Try it
 out and see! Now, to do something useful with the mouse we would need to know
 where on the screen it actually was. We can get the X and Y coords by using
 AX = 03 (see the table above). So lets try this:

 mousecommand 3,bx%,cx%,dx%
 x% = cx%
 y% = dx%
 print x%,y%

 Now if we try that then we see that the x and y co-ords are NOT DISPLAYED!!
 Why? Well as I said earlier, the procedure does not return anything from the
 mouse... so we need a bit of an adjustment to our procedure...

 SUB mousecommand (ax%,bx%,cx%,dx%)
 inregs.ax = ax%
 inregs.bx = bx%
 inregs.cx = cx%
 inregs.dx = dx%
 CALL INTERRUPT (&H33,inregs,outregs)
 ax% = outregs.ax
 bx% = outregs.bx
 cx% = outregs.cx
 dx% = outregs.dx
 END SUB

 Now if we try then we see it works! Et voila, you have a program that reads
 the mouse once and then displays the coords on the screen. To have something
 more useful we could use this code in the main section of the program:

 mousecommand 1,bx%,cx%,dx%
 DO
 mousecommand 3,bx%,cx%,dx%
 x% = cx%
 y% = dx%
 locate 1,1
 print x%,y%
 LOOP WHILE bx% <> 1

 Try it and see how it works! Have fun with the mouse! 

 Mail me if you wish to see anything else mousewise covered...
 Cheers, 8-)







--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #3 from December 1995.