DIM SHARED Menu$(4)

selection% = 1
Menu$(1) = "New Game"
Menu$(2) = "Load Game"
Menu$(3) = "Instructions"
Menu$(4) = "Quit Game"

GOSUB Redraw

DO
 A$ = INKEY$
 SELECT CASE A$
  CASE CHR$(0) + "H"
   selection% = selection% - 1
   IF selection% = 0 THEN selection% = 4
   GOSUB Redraw
  CASE CHR$(0) + "P"
   selection% = selection% + 1
   IF selection% > 4 THEN selection% = 1
   GOSUB Redraw
  CASE " "
   CLS
   PRINT "You selected: "; Menu$(selection%)
   END
 END SELECT
LOOP

Redraw:
CLS
FOR i = 1 TO 4
 PRINT Menu$(i)
NEXT i
LOCATE selection%, 1
COLOR 7, 1
PRINT Menu$(selection%)
COLOR 7, 0
RETURN

