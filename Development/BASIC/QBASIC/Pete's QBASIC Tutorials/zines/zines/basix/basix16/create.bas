OPEN "C:\test.dat" FOR BINARY AS #1 'Open the file
T = TIMER                           'Take the initial time
FOR i = 1 TO 204800                 'Read 204800 bytes with the one-byte method
GET #1, , a%                        'Get the byte
NEXT i                              'Repeat
PRINT TIMER - T                     'Print the elapsed time
CLOSE #1                            'Close the file
END

