' ================== GLORIA'S TITLE SCREEN ======================
'
' Apologies to whomever who showed how to place circles into a
' SUB statement.
'
DECLARE SUB DrawCircle (x!, Y!, colr!)
SCREEN 9

CALL DrawCircle(320, 8, 3)
CALL DrawCircle(260, 8, 2)
CALL DrawCircle(210, 8, 1)
CALL DrawCircle(160, 8, 15)
CALL DrawCircle(110, 8, 14)
CALL DrawCircle(60, 8, 13)
CALL DrawCircle(10, 8, 12)
CALL DrawCircle(10, 88, 11)
CALL DrawCircle(10, 138, 3)
CALL DrawCircle(10, 188, 2)
CALL DrawCircle(10, 238, 1)
CALL DrawCircle(10, 388, 2)
CALL DrawCircle(70, 438, 1)
CALL DrawCircle(130, 438, 15)
CALL DrawCircle(200, 438, 14)
CALL DrawCircle(270, 438, 13)
CALL DrawCircle(340, 438, 12)
CALL DrawCircle(400, 438, 11)
CALL DrawCircle(670, 338, 10)
CALL DrawCircle(670, 238, 1)
CALL DrawCircle(670, 138, 2)
CALL DrawCircle(670, 38, 3)

LINE (80, 20)-(550, 330), 0, BF

' I adapted this routine from a book of samples of
' enlarged letters, via multiples of X and Y values.

 DEFINT X-Y
 COLOR 8
 LOCATE 25, 1
 PRINT "  Hi, World!";

 FOR Y = 0 TO 11
          Y2 = 2 * Y: Y3 = 3 * Y: Y4 = 4 * Y
          FOR x = 0 TO 94
                        Z = POINT(x, 349 - Y)
                        IF Z <> 0 THEN
                        X2 = 2 * x: X3 = 3 * x
                        ' Chubby rounded font
                        CIRCLE (150 + X3, 200 - Y4), 3, 10
                        END IF
          NEXT
 NEXT

LOCATE 17, 25
COLOR 11
PRINT "shareware Gloria McMillan 1999"
SLEEP 1

LOCATE 20, 27
COLOR 2
PRINT "THIS WOULD START A PROGRAM."

 F$ = INPUT$(1)
CLS

SUB DrawCircle (x!, Y!, colr!)
    COLOR colr
    FOR radius = 1 TO 300 STEP 3
        CIRCLE (x, Y), radius
    NEXT radius
END SUB

