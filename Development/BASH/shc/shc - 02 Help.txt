shc Version 4.0.1, Generic Shell Script Compiler
shc GNU GPL Version 3 Md Jahidul Hamid <jahidulhamid@yahoo.com>
shc Usage: shc [-e date] [-m addr] [-i iopt] [-x cmnd] [-l lopt] [-o outfile] [-rvDSUHCABhs] -f script

    -e %s  Expiration date in dd/mm/yyyy format [none]
    -m %s  Message to display upon expiration ["Please contact your provider"]
    -f %s  File name of the script to compile
    -i %s  Inline option for the shell interpreter i.e: -e
    -x %s  eXec command, as a printf format i.e: exec('%s',@ARGV);
    -l %s  Last shell option i.e: --
    -o %s  output filename
    -r     Relax security. Make a redistributable binary
    -v     Verbose compilation
    -S     Switch ON setuid for root callable programs [OFF]
    -D     Switch ON debug exec calls [OFF]
    -U     Make binary untraceable [no]
    -H     Hardening : extra security protection [no]
           untraceable, undumpable, etc. and root is not required
           * currently only works with bourne shell (sh)
           * does not work with positional parameters
    -s     Hardening : use a single process (no child) [no]
           option available only with -H otherwise its ignored
           experimental feature may hang...
           * currently only works with bourne shell (sh)
           * does not work with positional parameters
    -C     Display license and exit
    -A     Display abstract and exit
    -B     Compile for busybox
    -h     Display help and exit

    Environment variables used:
    Name    Default  Usage
    CC      cc       C compiler command
    CFLAGS  <none>   C compiler flags
    LDFLAGS <none>   Linker flags

    Please consult the shc man page.
