################################
# dpkg-reconfigure - Time Zone #
################################

If you are using a Debian/Ubuntu-based system, you can reconfigure your
time zone by running:

    dpkg-reconfigure tzdata
