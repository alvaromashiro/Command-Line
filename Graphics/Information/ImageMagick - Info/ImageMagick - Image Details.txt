###############################
# ImageMagick - Image Details #
###############################

    mogrify -identify -verbose "/path/to/image"
    
If this displays too much information, remove the '-verbose' flag or use
the 'exiftool,' which you can get from 'libimage-exiftool-perl' package,
or at least search your package manager and see what you can find because
the naming conventions seem to be a little different between Linux distros.
However, when in doubt, it should be included in the 'forensics-extra'
package.
