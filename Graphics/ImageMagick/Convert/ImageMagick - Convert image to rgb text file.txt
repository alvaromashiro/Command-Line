###########################################################################
# ImageMagcik - Converting an image to an R, G, B values filled text file #
###########################################################################

convert file.gif' -compress none -colors 256 -depth 8 PPM:- > file.txt
