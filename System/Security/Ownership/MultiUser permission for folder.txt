###################################
# MultiUser permission for folder #
###################################

If I have a system with multiple users but only one of them can access the
other user's home folder, then it may be a good idea to save space by
creating symbolic links. However, the folders in the other user's home
directory will still be owned by that specific user and cannot be edited without
'sudo.' This is really annoying, so here's how you give permission to a list
of users to edit the contents of a folder or files without having to either
login to that specific user or use 'sudo.'

From: https://superuser.com/questions/280994/give-write-permissions-to-multiple-users-on-a-folder-in-ubuntu

Users in Linux can belong to more than one group. In this case you want 
to create a brand new group, let's call it tomandruser:

    sudo groupadd tomandruser

Now that the group exists, add the two users to it:

    sudo usermod -a -G tomandruser tomcat6
    sudo usermod -a -G tomandruser ruser

Now all that's left is to set the permissions on the directory:

    sudo chgrp -R tomandruser /path/to/the/directory
    sudo chmod -R 770 /path/to/the/directory

Now only members of the tomandruser group can read, write, or execute 
anything within the directory. Note the -R argument to the chmod and 
chgrp commands: this tells them to recurse into every sub directory of 
the target directory and modify every file and directory it finds.

You may also want to change 770 to something like 774 if you want others 
to be able to read the files, 775 if you want others to read and execute 
the files, etc. Group assignment changes won't take effect until the users 
log out and back in.

If you also want (you probably do) that new files created inside the 
directory by one of the users are automaticaly writable by others in the 
group, then see here (https://superuser.com/questions/19318/how-can-i-give-write-access-of-a-folder-to-all-users-in-linux/19333#19333).

------------------------------------------------------------------------

A comment below this reply suggested that you use:

    sudo chmod -R g+w /path/to/the/directory
    
in order to "add write permissions without mucking up everything else."
