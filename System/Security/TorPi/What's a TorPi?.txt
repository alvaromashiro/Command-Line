#####[ TorPi - What's a TorPi? ]#####

A TorPi is when you take a Raspberry Pi
and create an access point between your
devices and the router. When setup correctly,
You devices will recognize the Raspberry Pi
as a WiFi router but then because it's running
Tor, all the traffic is encrypted with it.

You must have a Raspberry Pi with built-in WiFi
or know how to setup a dongle and an Ethernet
cable. This is because you cannot use the WiFi
to connect to your normal router and also have
it act as an access point at the same time.

A lot of people don't realize this, but just
because you are running a VPN on a mobile device,
even though the [VPN] indicator says it is active,
that doesn not mean that an application or even
the system level connections have to honor it.
But, with the TorPi, it doesn't matter. However,
you should still be careful, especially if you
are running services that you've registered to
without using Tor or VPN initially and ones that
have your real information associated with it.

iOS users can go a step further by Jailbreaking
and using Cydia to install a program called
"Protect My Privacy," aka PMP to randomize their
Unique ID and Advertisement ID at will, as well as
block an application's access to various things
you will not find in your Settings, if Apple
or the application maker even care at all.
