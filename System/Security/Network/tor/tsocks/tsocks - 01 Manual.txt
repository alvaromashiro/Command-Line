TSOCKS(1)                            General Commands Manual                            TSOCKS(1)

NAME
       tsocks - Shell wrapper to simplify the use of the tsocks(8) library to transparently allow
       an application to use a SOCKS proxy

SYNOPSIS
       tsocks [application [application's arguments]]
       or tsocks [-on|-off]
       or tsocks

DESCRIPTION
       tsocks is a wrapper between the tsocks library and the application what you would like  to
       run socksified.

OPTIONS
       [application [application's arguments]]
              run  the  application  as specified with the environment (LD_PRELOAD) set such that
              tsocks(8) will transparently proxy SOCKS connections in that program

       [-on|-off]
              this option adds or removes tsocks(8) from  the  LD_PRELOAD  environment  variable.
              When  tsocks(8)  is  in  this  variable all executed applications are automatically
              socksified. If you want to use this function, you HAVE to source the  shell  script
              from yours, like this: "source /usr/bin/tsocks" or ". /usr/bin/tsocks"
              Example:
              ". tsocks -on" -- add the tsocks lib to LD_PRELOAD (don't forget the leading dot!)
              ".  tsocks -off" -- remove the tsocks lib from LD_PRELOAD (don't forget the leading
              dot!)

       [-show|-sh]
              show the current value of the LD_PRELOAD variable

       <without any argument>
              create a new shell with LD_PRELOAD including tsocks(8).

SEE ALSO
       tsocks.conf(5) tsocks(8)

AUTHOR
       This script was created by Tamas SZERB <toma@rulez.org> for the debian package of  tsocks.
       It  (along with this manual page) have since been adapted into the main tsocks project and
       modified.

TSOCKS                                                                                  TSOCKS(1)
