whohas(1)                                                                               whohas(1)

NAME
       whohas - find packages in various distributions' repositories

SYNTAX
       whohas [--no-threads] [--shallow] [--strict] [-d Dist1[,Dist2[,Dist3 etc.]]] pkgname

DESCRIPTION
       whohas  is  a command line tool to query package lists from the Arch, Debian, Fedora, Gen‐
       too, Mandriva, openSUSE, Slackware, Source Mage, Ubuntu, FreeBSD, NetBSD,  OpenBSD,  Fink,
       MacPorts and Cygwin distributions.

OPTIONS
       --no-threads
              Don't use multiple threads to query package lists (will be much slower)

       --shallow
              Limit to one call per server. Faster, but loses some information, typically package
              size and release date.

       --strict
              List only those packages that have exactly pkgname as their name.

       -d Dist1[,Dist2[,Dist3 etc.]]
              Queries only for packages for  the  listed  distributions.  Recognised  values  for
              Dist1,  Dist2,  etc.  are "archlinux", "cygwin", "debian", "fedora", "fink", "free‐
              bsd", "gentoo", "mandriva", "macports", "netbsd",  "openbsd",  "opensuse",  "slack‐
              ware", "sourcemage", and "ubuntu".

       pkgname
              Package name to query for

FILES
       whohas uses various files in ~/.whohas to cache package lists for some distributions.

SEE ALSO
       See intro.txt or intro.html notes on using whohas.

AUTHORS
       whohas is written and maintained by Philipp Wesche <phi1ipp@yahoo.com>

       This  man page was written by Jonathan Wiltshire <debian@jwiltshire.org.uk> for the Debian
       project and adapted for a new version by Philipp Wesche <phi1ipp@yahoo.com>

Jonathan Wiltshire                            0.29.1                                    whohas(1)
