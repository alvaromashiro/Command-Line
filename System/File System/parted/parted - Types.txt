##################
# parted - Types #
##################

To get the most out of parted install:
-------------------------------------
dosfstools
mtools
btrfs-tools
c2fsprogs
f2ffs-tools
hfsutils
hfsprogs
jfsutils
util-linux
lvm2
nilfs-utils
ntfsprogs
ntfs-3g
reiser4progrs
xfsprogs
xfsdump
