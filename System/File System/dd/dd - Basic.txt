##############
# dd - Basic #
##############

I use dd to unpack .iso or .img files to a USB stick:

    if="/path/to/file.img" of="/dev/sdb"
    
To know where your USB device (the 'of' part) is, use:

    fdisk -l
    
You may have to run these as sudo.

You can monitor the progress of a 'dd' job by (in another terminal):

    pgrep –l ‘^dd$’
    kill –USR1 [The number the above command gives you]
    

The terminal in which you are running 'dd' will now show the progress.
