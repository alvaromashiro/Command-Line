PHANTASIA(6)                             BSD Games Manual                            PHANTASIA(6)

NAME
     phantasia — an interterminal fantasy game

SYNOPSIS
     phantasia [-abHmpSsx]

DESCRIPTION
     phantasia is a role playing game which allows players to roll up characters of various types
     to fight monsters and other players.  Progression of characters is based upon gaining expe‐
     rience from fighting monsters (and other players).

     Most of the game is menu driven and self-explanatory (more or less).  The screen is cursor
     updated, so be sure to set up the TERM variable in your environment.

     The options provide for a variety of functions to support the game.  They are:

           -a   Get a listing of all character names on file.

           -b   Show scoreboard of top characters per login.

           -H   Print header only.

           -m   Get a monster listing.

           -p   Purge old characters.

           -S   Turn on wizard options, if allowed, if running as “root”.

           -s   Invokes phantasia without header information.

           -x   Examine/change a particular character on file.

     The characters are saved on a common file, in order to make the game interactive between
     players.  The characters are given a password in order to retrieve them later.  Only charac‐
     ters above level zero are saved.  Characters unused for awhile will be purged.  Characters
     are only placed on the scoreboard when they die.

PARTICULARS
   Normal Play
     A number of the player's more important statistics are almost always displayed on the
     screen, with maximums (where applicable) in parentheses.

     The character is placed randomly near the center of a Cartesian system.  Most commands are
     selected with a single letter or digit.  For example, one may move by hitting 'W', 'S', 'N',
     or 'E', (lower case may also be used, at no time is the game case dependent).  One may also
     use 'H', 'J', 'K', 'L', for movement, similar to vi(1).  To move to a specific (x, y) coor‐
     dinate, use the move ('1') command.  The distance a character can move is calculated by 1
     plus 1.5 per level.  Moving in a compass direction will move the player the maximum allowed
     distance in that direction.

     A player may see who else is playing by using the players ('2') option.  One may see the
     coordinates of those who are the same distance or closer to the origin as he/she.  Kings,
     and council of the wise can see and can be seen by everyone.  A palantir removes these
     restrictions.

     One can talk to other players with the talk ('3') option.  In general, this is a line or so
     of text.  To remove a current message, just type ⟨return⟩ when prompted for a message.

     The stats ('4') option shows additional characteristics of a player.

     One may leave the game either with the quit ('5') option.

     One may rest by default.  Resting lets one regain maximum energy level, and also lets one
     find mana (more is found for larger levels and further distances from the origin).

     One may call a monster by hitting '9' or 'C'.

     Use 'X' to examine other players.

     One may quit or execute a sub-shell by hitting interrupt.  Quitting during battle results in
     death for obvious reasons.

     Several other options become available as the player progresses in level and magic, or to
     other stations in the game (valar, council of the wise, king).  These are described else‐
     where.  In general, a control-L will force the redrawing of the screen.

     Other things which may happen are more or less self-explanatory.

   Fighting Monsters
     A player has several options while fighting monsters.  They are as follows:

           melee     Inflicts damage on the monster, based upon strength.  Also decreases the
                     monster's strength some.

           skirmish  Inflicts a little less damage than melee, but decreases the monster's
                     quickness instead.

           evade     Attempt to run away.  Success is based upon both the player's and the mon‐
                     ster's brains and quickness.

           spell     Several options for throwing spells (described elsewhere).

           nick      Hits the monster one plus the player's sword, and gives the player 10% of
                     the monster's experience.  Decreases the monster's experience an amount pro‐
                     portional to the amount granted.  This also increases the monster's quick‐
                     ness.  Paralyzed monsters wake up very fast when nicked.

           luckout   This is essentially a battle of wits with the monster.  Success is based
                     upon the player's and the monster's brains.  The player gets credit for
                     slaying the monster if he/she succeeds.  Otherwise, nothing happens, and the
                     chance to luckout is lost.

   Character Statistics
           strength   determines how much damage a character can inflict.

           quickness  determines how many chances a character gets to make decisions while fight‐
                      ing.

           energy level
                      specifies how much damage a character may endure before dying.

           magic level
                      determines which spells a character may throw, and how effective those
                      spells will be.

           brains     basically, the character's intelligence; used for various fighting options
                      and spells.

           mana       used as a power source for throwing spells.

           experience
                      gained by fighting monsters and other characters.

           level      indicative of how much experience a character has accumulated; progresses
                      geometrically as experience increases.

           poison     sickness which degrades a character's performance (affects energy level and
                      strength).

           sin        accumulated as a character does certain nasty things; used only rarely in
                      normal play of the game.

           age        of player; roughly equivalent to number of turns.  As age increases, many
                      personal statistics degenerate.

   Character Types
     Character statistics are rolled randomly from the above list, according to character type.
     The types are as follows:

           magic user   strong in magic level and brains, weak in other areas.  Must rely on wits
                        and magic to survive.

           fighter      good in strength and energy level, fairly good in other areas.  This adds
                        up to a well-equipped fighter.

           elf          very high quickness and above average magic level are elves selling
                        points.

           dwarf        very high strength and energy level, but with a tendency to be rather
                        slow and not too bright.

           halfling     rather quick and smart, with high energy level, but poor in magic and
                        strength.  Born with some experience.

           experimento  very mediocre in all areas.  However, the experimento may be placed
                        almost anywhere within the playing grid.

     The possible ranges for starting statistics are summarized in the following table.
     Type          Strength   Quick    Mana    Energy   Brains   Magic
     ──────────────────────────────────────────────────────────────────
     Mag. User      10-15     30-35   50-100   30-45    60-85     5-9
     Fighter        40-55     30-35   30-50    45-70    25-45     3-6
     Elf            35-45     32-38   45-90    30-50    40-65     4-7
     Dwarf          50-70     25-30   25-45    60-100   20-40     2-5
     Halfling       20-25      34     25-45    55-90    40-75     1-4
     Experimento      25       27      100       35       25       2

     Not only are the starting characteristics different for the different character types, the
     characteristics progress at different rates for the different types as the character goes up
     in level.  Experimentoes' characteristics progress randomly as one of the other types.  The
     progression as characters increase in level is summarized in the following table.

     Type        Strength   Mana   Energy   Brains   Magic
     ──────────────────────────────────────────────────────
     Mag. User     2.0       75      20      6       2.75
     Fighter       3.0       40      30      3.0     1.5
     Elf           2.5       65      25      4.0     2.0
     Dwarf         5         30      35      2.5     1
     Halfling      2.0       30      30      4.5     1

     The character type also determines how much gold a player may carry, how long until rings
     can overcome the player, and how much poison the player can withstand.

   Spells
     During the course of the game, the player may exercise his/her magic powers.  These cases
     are described below.

           cloak           magic level necessary: 20 (plus level 7)
                           mana used: 35 plus 3 per rest period
                           Used during normal play.  Prevents monsters from finding the charac‐
                           ter, as well as hiding the player from other players.  His/her coordi‐
                           nates show up as '?' in the players option.  Players cannot collect
                           mana, find trading posts, or discover the grail while cloaked.  Call‐
                           ing a monster uncloaks, as well as choosing this option while cloaked.

           teleport        magic level necessary: 40 (plus level 12)
                           mana used: 30 per 75 moved
                           Used during normal play.  Allows the player to move with much more
                           freedom than with the move option, at the price of expending mana.
                           The maximum distance possible to move is based upon level and magic
                           level.

           power blast     magic level necessary: none
                           mana used: 5 times level
                           Used during inter-terminal battle.  Damage is based upon magic level
                           and strength.  Hits much harder than a normal hit.

           all or nothing  magic level necessary: none
                           mana used: 1
                           Used while combating monsters.  Has a 25% chance of working.  If it
                           works it hits the monster just enough to kill it.  If it fails, it
                           doesn't hit the monster, and doubles the monster's quickness and
                           strength.  Paralyzed monsters wake up much quicker as a result of this
                           spell.

           magic bolt      magic level necessary: 5
                           mana used: variable
                           Used while combating monsters.  Hits the monster based upon the amount
                           of mana expended and magic level.  Guaranteed to hit at least 10 per
                           mana.

           force field     magic level necessary: 15
                           mana used: 30
                           Used during monster combat.  Throws up a shield to protect from dam‐
                           age.  The shield is added to actual energy level, and is a fixed num‐
                           ber, based upon maximum energy.  Normally, damage occurs first to the
                           shield, and then to the players actual energy level.

           transform       magic level necessary: 25
                           mana used: 50
                           Used during monster combat.  Transforms the monster randomly into one
                           of the 100 monsters from the monster file.

           increase might  magic level necessary: 35
                           mana used: 75
                           Used during combat with monsters.  Increases strength up to a maximum.

           invisibility    magic level necessary: 45
                           mana used: 90
                           Used while fighting monsters.  Makes it harder for the monster to hit,
                           by temporarily increasing the player's quickness.  This spell may be
                           thrown several times, but a maximum level will be reached.

           transport       magic level necessary: 60
                           mana used: 125
                           Used during monster combat.  Transports the monster away from the
                           player.  Success is based upon player's magic and brains, and the mon‐
                           ster's experience.  If it fails the player is transported instead.
                           60% of the time, the monster will drop any treasure it was carrying.

           paralyze        magic level necessary: 75
                           mana used: 150
                           Used during monster combat.  “Freezes” the monster by putting its
                           quickness slightly negative.  The monster will slowly wake up.  Suc‐
                           cess is based upon player's magic and the monster's experience.  If it
                           fails, nothing happens.

           specify         magic level necessary: none
                           mana used: 1000
                           Used during monster combat only by valar or council of the wise.
                           Allows the player to pick which monster to fight.

   Monsters
     Monsters get bigger as one moves farther from the origin (0,0).  Rings of distance 125 from
     the origin determine the size.  A monster's experience, energy level, and brains are multi‐
     plied by the size.  Strength is increased 50% per size over one, and quickness remains the
     same, regardless of size.

     Also, nastier monsters are found as one progress farther out from the origin.  Monsters also
     may flock.  The percent chance of that happening is designated as flock% in the monster
     listing.  Monsters outside the first ring may carry treasure, as determined by their trea‐
     sure type.  Flocking monsters, and bigger monsters increase the chances of treasure.

     Certain monsters have special abilities; they are as follows:

     Unicorn           can only be subdued if the player is in possession of a virgin.

     Modnar            has random characteristics, including treasure type.

     Mimic             will pick another name from the list of monsters in order to confuse.

     Dark Lord         very nasty person.  Does not like to be hit (especially nicked), and many
                       spells do not work well (or at all) against him.  One can always evade
                       from the Dark Lord.

     Leanan-Sidhe      also a very nasty person.  She will permanently sap strength from someone.

     Saruman           wanders around with Wormtongue, who can steal a palantir.  Also, Saruman
                       may turn a player's gems into gold pieces, or scramble her/his stats.

     Thaumaturgist     can transport a player.

     Balrog            inflicts damage by taking away experience, not energy.

     Vortex            may take some mana.

     Nazgul            may try to steal a ring or neutralize part of one's brains.

     Tiamat            may take half a player's gold and gems and escape.

     Kobold            may get nasty and steal one gold piece and run away.

     Shelob            may bite, inflicting the equivalent of one poison.

     Assorted Faeries  These are killed if attacking someone carrying holy water.  These are
                       Cluricaun, Fir Darrig, Fachan, Ghille Dhu, Bogle, Killmoulis, and Bwca.

     Lamprey           may bite, inflicting 1/2 of a poison.

     Shrieker          will call one of its (much bigger) buddies if picked upon.

     Bonnacon          will become bored with battle, fart, and run off.

     Smeagol           will try to steal a ring from a player, if given the chance.

     Succubus          may inflict damage through a force field.  This subtracts from energy
                       level instead of any shield the player may have thrown up.  This is a very
                       easy way to die.

     Cerberus          loves metal and will steal all the metal treasures from a player if able.

     Ungoliant         can bite and poison.  This inflicts five poisons, and also takes one from
                       the player's quickness.

     Jabberwock        may tire of battle, and leave after calling one of his friends (Jubjub
                       Bird or Bandersnatch).

     Morgoth           actually Modnar, but reserved for council of the wise, valar, and
                       ex-valar.  Fights with Morgoth end when either he or the player dies.  His
                       characteristics are calculated based upon the player's.  The player is
                       given the chance to ally with him.  No magic, except force field works
                       when battling Morgoth.

     Troll             may regenerate its energy and strength while in battle.

     Wraith            may make a player blind.

   Treasures
     The various treasure types are as follows:

     Type zero             none

     Type one              power booster - adds mana.
                           druid - adds experience.
                           holy orb - subtracts 0.25 sin.

     Type two              amulet - protects from cursed treasure.
                           holy water - kills assorted faeries.
                           hermit - reduces sin by 25% and adds some mana.

     Type three            shield - adds to maximum energy level.
                           virgin - used to subdue a unicorn, or to give much experience (and
                           some sin).
                           athelas - subtracts one poison.

     Type four (scrolls)   shield - throws a bigger than normal force field.
                           invisible - temporarily puts the finder's quickness to one million.
                           ten fold strength - multiplies finder's strength by ten.
                           pick monster - allows finder to pick next monster to battle.
                           general knowledge - adds to finder's brains and magic level.

                           All the scrolls except general knowledge automatically call a monster.
                           These preserve any spells that were already in effect, but are only in
                           effect while in battle.

     Type five             dagger - adds to strength.
                           armour - same as a shield, but bigger.
                           tablet - adds brains.

     Type six              priest - rests to maximum; adds mana, brains; and halves sin.
                           Robin Hood - increases shield and adds permanently to strength.
                           axe - like dagger, but bigger.

     Type seven            charm - protects from cursed treasure (used before amulet); used in
                           conjunction with blessing to battle Dark Lord.
                           Merlyn - adds brains, magic, and mana.
                           war hammer - like an axe, but bigger.

     Type eight            healing potion - sets poison to -2, or subtracts two from poison,
                           whichever is better.
                           transporter - allows finder to move anywhere.
                           sword - like a war hammer, but bigger.

     Type nine             golden crown - allows the player to become king, by going to (0,0).
                           blessing - cuts sin to 1/3, adds mana, rests to maximum, kills Dark
                           Lord with a charm, and gives bearer first hit on all monsters.
                           quicksilver - adds to quickness.

     Type ten              elven boots - adds permanently to quickness.

     Type eleven           palantir - allows one to see all the other players; used by council of
                           the wise to seek the grail.

     Type twelve/thirteen  ring - allows one to hit much harder in battle, etc.

     Any treasure type 10-13 monsters may instead carry a type nine treasure.

     A monster may also be carrying gold or gems.  These are used at trading posts to buy things.
     A gem is worth 1000 gold pieces.  Too much gold will slow a player down.  One may carry 1000
     plus 200 per level of gold.  A gem weighs one half a gold piece.  Monsters of treasure type
     7 or higher may carry gems.

     The chance of a cursed treasure is based upon treasure type.  The more valuable treasures
     have a greater chance of being cursed.  A cursed treasure knocks energy level very low, and
     adds 0.25 poison.

   Rings
     Rings are only carried by nazguls and Dark Lords.  They come in four different flavors.  All
     rings rest the player to maximum and cause him/her to hit much harder in battle with mon‐
     sters (assuming one has chosen to use the ring for battle.)

     Two types of rings are cursed and come either from nazguls or Dark Lord.  After a few times
     of using these types, the player falls under the control of the ring, and strange, random
     things will occur.  Eventually, the player dies, and gives his/her name to a monster on the
     file.  Dying before the ring is used up also renames the monster.

     The two remaining types of rings are much more benign.  The one from a nazgul is good for a
     limited number of battle rounds, and will save the player from death if it was being used
     when he/she died.  The one from Dark Lord is the same, except that it never is used up.
     rings disappear after saving someone from death.  In general, cursed rings occur much more
     often than normal ones.  It is usually not a good idea to pick one up.  The only way to get
     rid of a ring is to have a monster steal it.

   King
     A player may become king by finding a crown and going to (0,0).  Players must have a level
     in the range of 10 to 1000 to be able to find a crown.  When a player with one or more
     crowns reaches level 1000, the crowns are converted to gold.

     Once a player is king, he/she may do certain things while in the Lord's Chamber (0,0).
     These are exercised with the decree ('0') option.

     transport      This is done to another player.  It randomly moves the affected player about.
                    A charm protects from transports.

     curse          This is done to another player.  It is analogous to cursed treasure, but
                    worse.  It inflicts two poison, knocks energy level very low, and degrades
                    the maximum energy.  It also removes a cloak.  A blessing protects from
                    king's curses.

     energy void    The king may put a number of these scattered about his/her kingdom as he/she
                    pleases.  If a player hits one, he/she loses mana, energy, and gold.  The
                    energy void disappears after being hit.

     bestow         This is also done to another player.  The king may wish to reward one or more
                    loyal subjects by sharing his/her riches (gold).  Or it is a convenient way
                    to dispose of some unwanted deadweight.

     collect taxes  Everyone pays 7% tax on all gold and gems acquired, regardless of the exis‐
                    tence of a king.  The king collects the accrued taxes with this option.

     The king may also teleport anywhere for free by using the origin as a starting place.

   Council of the Wise, Valar
     A player automatically becomes a member of the council of the wise upon reaching level 3000.
     Members of the council cannot have rings.  Members of the council have a few extra options
     which they can exercise.  These are exercised with the intervene ('8') option.  All
     intervene options cost 1000 mana.  One intervene option is to heal another player.  This is
     just a quick way for that player to be rested to maximum and lose a little poison.  The main
     purpose in life for members of the council is to seek the Holy Grail.  This is done with a
     palantir under the seek grail option.  The distance cited by the seek is accurate within
     10%, in order not to make it too easy to find the grail.  A player must have infinitesimally
     small sin, or else it's all over upon finding the grail.  In order to help members of the
     council on their quest, they may teleport with greater ease.

     Upon finding the grail, the player advances to position of valar.  He/she may then exercise
     more and niftier options under intervention.  These include all of the council members'
     options plus the ability to move other players about, bless them, and throw monsters at
     them.  A valar's blessing has the same effect as the treasure blessing, except that the
     affected player does not get his/her blessing flag set.  All intervention options which
     affect other players age the player who uses them.  Valars are essentially immortal, but are
     actually given five lives.  If these are used up, the player is left to die, and becomes an
     ex-valar.  A valar cannot move, teleport, or call monsters.  (An exception to this is if the
     valar finds a transporter.)  This is to allow him/her to dispose of excess gold.  Any mon‐
     sters which a valar encounters are based upon his/her size.  Only one valar may exist at a
     time.  The current valar is replaced when another player finds the grail.  The valar is then
     bumped back to the council of the wise.

   Wizard
     The wizard is usually the owner of the game, and the one who maintains the associated files.
     The wizard is granted special powers within the game, if it is invoked with the -S option.
     Otherwise, the wizard plays no different from other players.  The wizard abilities are out‐
     lined below.

     change players        When examining a player, (game invoked with -x, or use 'X' from within
                           game), the wizard may also change the player.

     intervention          The wizard may do all the intervention options.  One extra option,
                           vaporize, is added to kill any offensive players.

     super character type  An extra character type is added.  This character starts with the max‐
                           imum possible in all statistics, selected from the other character
                           types.  A super character's statistics also progress at the maximum
                           possible rate, selected from the other character types.

   Special Places
     Certain regions of the playing grid have different names.  In general, this is only to give
     the player some idea of his/her present location.  Some special places do exist.

     Trading Posts  These are located at |x| == |y| == n*n*100 for n = 1, 2, ..., 1000.  Trading
                    posts farther out have more things for sale.  Be careful about cheating the
                    merchants there, as they have short tempers.  Merchants are dishonest about
                    5% of the time.

     Lord's Chamber
                    This is located at (0,0).  Only players with crowns may enter.

     Point of No Return
                    This is located beyond 1.2e+6 in any direction.  The only way to return from
                    here is a transporter or to have a valar relocate the player.

     Dead Marshes   This is a band located fairly distant from the origin.  The first fourteen
                    monsters (water monsters) can normally only be found here.

     Valhala        This place is where the valar resides.  It is associated with no particular
                    coordinate on the playing grid.

   Miscellaneous
     Once a player reaches level 5, the game will start to time out waiting for input.  This is
     to try to keep the game a bit faster paced.

     A guru will never be disgusted with your sins if they are less than one.

     A medic wants half of a player's gold to be happy.  Offering more than one has, or a nega‐
     tive amount will anger the medic, who will make the player worse (add one poison).

     The Holy Grail does little for those who are not ready to behold it.  Whenever anyone finds
     it, it moves.  It is always located within 1e+6 in any compass direction of the origin.

     There is a maximum amount of mana and charms a player may posses, based upon level.
     Quicksilver is always limited to to a maximum of 99.

     Books bought at a trading post increase brains, based upon the number bought.  It is unwise,
     however to buy more than 1/10 of one's level in books at a time.

     Players over level 10000 are automatically retired.

     A blindness goes away in random time.

     Players with crowns are identified with a '*' before their character type.

   Inter-terminal Battle
     When two player's coordinates correspond, they may engage in battle.  In general, the player
     with the highest quickness gets the first hit.  If the two players are severely mismatched,
     the stronger player is drastically handicapped for the battle.  In order to protect from
     being stuck in an infinite loop, the player waiting for response may time out.  Options for
     battle are:

     fight        Inflicts damage upon other person.

     run away     Escape from battle.  Has a 75% chance of working.

     power blast  Battle spell.

     luckout      One-time chance to try to win against the foe.  Has a 10% chance of working.

     Sometimes waits for the other player may be excessive, because he/she may be battling a mon‐
     ster.  Upon slaying a player in battle the winner gets the other's experience and treasures.
     Rings do not work for inter-terminal battle.

AUTHORS
     Edward Estes, AT&T Information Systems, Skokie, IL

BUGS
     All screen formats assume at least 24 lines by at least 80 columns.  No provisions are made
     for when any of the data items get too big for the allotted space on the screen.

BSD                                       April 1, 2001                                       BSD
