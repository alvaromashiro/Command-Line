ADVENTURE(6)                             BSD Games Manual                            ADVENTURE(6)

NAME
     adventure — an exploration game

SYNOPSIS
     adventure [saved-file]

DESCRIPTION
     The object of the game is to locate and explore Colossal Cave, find the treasures hidden
     there, and bring them back to the building with you.  The program is self-descriptive to a
     point, but part of the game is to discover its rules.

     To terminate a game, enter “quit”; to save a game for later resumption, enter “suspend”.

BSD                                        May 31, 1993                                       BSD
