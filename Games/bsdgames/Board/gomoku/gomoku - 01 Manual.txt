GOMOKU(6)                                BSD Games Manual                               GOMOKU(6)

NAME
     gomoku — game of 5 in a row

SYNOPSIS
     gomoku [-bcdu] [-D debugfile] [inputfile]

DESCRIPTION
     gomoku is a two player game where the object is to get 5 in a row horizontally, vertically
     or diagonally on a 19 by 19 grid.  By convention, black always moves first.  With no argu‐
     ments, gomoku will display a playing board and prompt for moves from the user.  Valid moves
     are a letter for the column and a number for the row of an empty board location.  Entering
     ``quit" or ``resign" will end the game.  You can save the current state of the game by
     entering ``save" and supplying a file name when prompted.  The optional file inputfile can
     be used to restore a saved game.

     The options are:

     -b      This option sets background mode.  Input moves are read from standard input, the
             computer picks a move, and prints it to standard output.  The first input line
             should be either ``black" or ``white" to specify whether gomoku has the first move
             or not respectively.  This option was intended for game tournaments where a referee
             program handles the board display and pits one program against another.

     -c      Computer versus computer.  gomoku will play a game against itself.  This is mostly
             used for testing.

     -d      Print debugging information.  Repeating this option more than once yields more
             detailed information.

     -D debugfile
             Print the debug information to debugfile instead of to the standard output.

     -u      User versus user.  This is mostly used for testing.

AUTHOR
     Ralph Campbell

ACKNOWLEDGEMENTS
     The board display routines were based on the goref program written by Peter Langston.

BSD                                       August 4, 1994                                      BSD
