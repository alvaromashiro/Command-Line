WUMP(6)                                  BSD Games Manual                                 WUMP(6)

NAME
     wump — hunt the wumpus in an underground cave

SYNOPSIS
     wump [-h] [-a arrows] [-b bats] [-p pits] [-r rooms] [-t tunnels]

DESCRIPTION
     The game wump is based on a fantasy game first presented in the pages of People's Computer
     Company in 1973.  In Hunt the Wumpus you are placed in a cave built of many different rooms,
     all interconnected by tunnels.  Your quest is to find and shoot the evil Wumpus that resides
     elsewhere in the cave without running into any pits or using up your limited supply of
     arrows.

     The options are as follows:

     -a      Specifies the number of magic arrows the adventurer gets.  The default is five.

     -b      Specifies the number of rooms in the cave which contain bats.  The default is three.

     -h      Play the hard version -- more pits, more bats, and a generally more dangerous cave.

     -p      Specifies the number of rooms in the cave which contain bottomless pits.  The
             default is three.

     -r      Specifies the number of rooms in the cave.  The default cave size is twenty-five
             rooms.

     -t      Specifies the number of tunnels connecting each room in the cave to another room.
             Beware, too many tunnels in a small cave can easily cause it to collapse!  The
             default cave room has three tunnels to other rooms.

     While wandering through the cave you'll notice that, while there are tunnels everywhere,
     there are some mysterious quirks to the cave topology, including some tunnels that go from
     one room to another, but not necessarily back!  Also, most pesky of all are the rooms that
     are home to large numbers of bats, which, upon being disturbed, will en masse grab you and
     move you to another portion of the cave (including those housing bottomless pits, sure death
     for unwary explorers).

     Fortunately, you're not going into the cave without any weapons or tools, and in fact your
     biggest aids are your senses; you can often smell the rather odiferous Wumpus up to two
     rooms away, and you can always feel the drafts created by the occasional bottomless pit and
     hear the rustle of the bats in caves they might be sleeping within.

     To kill the wumpus, you'll need to shoot it with one of your magic arrows.  Fortunately, you
     don't have to be in the same room as the creature, and can instead shoot the arrow from as
     far as three or four rooms away!

     When you shoot an arrow, you do so by typing in a list of rooms that you'd like it to travel
     to.  If at any point in its travels it cannot find a tunnel to the room you specify from the
     room it's in, it will instead randomly fly down one of the tunnels, possibly, if you're real
     unlucky, even flying back into the room you're in and hitting you!

BSD                                        May 31, 1993                                       BSD
