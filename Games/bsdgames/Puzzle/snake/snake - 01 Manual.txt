SNAKE(6)                                 BSD Games Manual                                SNAKE(6)

NAME
     snake, snscore — display chase game

SYNOPSIS
     snake [-w width] [-l length] [-t]
     snscore

DESCRIPTION
     snake is a display-based game which must be played on a CRT terminal.  The object of the
     game is to make as much money as possible without getting eaten by the snake.  The -l and -w
     options allow you to specify the length and width of the field.  By default the entire
     screen is used.  The -t option makes the game assume you are on a slow terminal.

     You are represented on the screen by an I.  The snake is 6 squares long and is represented
     by s's with an S at its head.  The money is $, and an exit is #.  Your score is posted in
     the upper left hand corner.

     You can move around using the same conventions as vi(1), the h, j, k, and l keys work, as do
     the arrow keys.  Other possibilities include:

     sefc    These keys are like hjkl but form a directed pad around the d key.

     HJKL    These keys move you all the way in the indicated direction to the same row or column
             as the money.  This does not let you jump away from the snake, but rather saves you
             from having to type a key repeatedly.  The snake still gets all his turns.

     SEFC    Likewise for the upper case versions on the left.

     ATPB    These keys move you to the four edges of the screen.  Their position on the keyboard
             is the mnemonic, e.g.  P is at the far right of the keyboard.

     x       This lets you quit the game at any time.

     p       Points in a direction you might want to go.

     w       Space warp to get out of tight squeezes, at a price.

     To earn money, move to the same square the money is on.  A new $ will appear when you earn
     the current one.  As you get richer, the snake gets hungrier.  To leave the game, move to
     the exit (#).

     A record is kept of the personal best score of each player.  Scores are only counted if you
     leave at the exit, getting eaten by the snake is worth nothing.

     As in pinball, matching the last digit of your score to the number which appears after the
     game is worth a bonus.

     To see who wastes time playing snake, run snscore.

FILES
     /var/games/bsdgames/snakerawscores  database of personal bests
     /var/games/bsdgames/snake.log       log of games played

BUGS
     When playing on a small screen, it's hard to tell when you hit the edge of the screen.

     The scoring function takes into account the size of the screen.  A perfect function to do
     this equitably has not been devised.

BSD                                        May 31, 1993                                       BSD
