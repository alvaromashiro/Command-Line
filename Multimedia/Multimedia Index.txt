####################
# Multimedia Index #
####################

abc2abc - a simple abc checker/re-formatter/transposer
abc2midi - converts abc file to MIDI file(s)
abcmatch - search for specific sequences of notes in an abc file composed 
           of many tunes
aconnect - ALSA sequencer connection manager
alsamixer - soundcard mixer for ALSA soundcard driver, with ncurses interface
asciinema - terminal session recorder
bristol - a synthesiser emulation package
cava - Console-based Audio Visualizer for Alsa (MPD and Pulseaudio)
cdw - front-end for cdrecord, mkisofs, growisofs, mkudffs and other tools
dvdauthor - assembles multiple mpeg program streams into a suitable DVD filesystem
dvd+rw-mediainfo - display information about dvd drive and disk
dvd-slideshow  - creates a slideshow movie in DVD video format from a list 
                 of pictures and effects
ecasound - sample editor, multitrack recorder, fx-processor, etc.
espeak - a multi-lingual software speech synthesizer
festival - a text-to-speech system
ffmpeg - Tools for transcoding, streaming and playing of multimedia files
ffplay - FFplay media player
hasciicam - webcam ascii viewer
hydrogen - a simple drum machine/step sequencer
libdvdcss - needed to play DVD's; notes only; not included
melt - author, play, and encode multitrack audio/video compositions
midi2abc - program to convert MIDI format files to abc notation
mikmod - play soundtracker etc. modules on a Unix machine
MOC (mocp) - Console audio player
mps-youtube (mpsyt) - search and play YouTube music and videos
mpv - a media player
openmpt123 - command line module music player based on libopenmpt
pacmd - reconfigure a PulseAudio sound server during runtime
pactl - control a running PulseAudio sound server
pamix - ncurses based pulseaudio mixer for the commandline
pmidi - a midi file player for ALSA
pulsemixer - a python-based pulseaudio mixer
pyradio - Play internet radio stations from the command-line
radio - console radio application to control connected radio devices
schismtracker - tracked music editor based on Impulse Tracker
sox - Sound eXchange, the Swiss Army knife of audio manipulation
StreamPi - scripts to make live streaming and local recording easy with ffmpeg
termtosvg - renders your command line sessions as standalone SVG animations
timidity - MIDI-to-WAVE converter and player
v4l2-ctl - an application to control video4linux drivers
wodim - write data to optical disk media
yoshimi - a software music synthesiser
youtube-dl - download videos from youtube.com or other video platforms
zynaddsubfx - a software synthesizer
