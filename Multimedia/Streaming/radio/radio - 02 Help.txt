radio -- interactive ncurses radio application
usage:
  radio [ options ]

options:
  -h       print this text
  -d       enable debug output
  -m       mute radio
  -b 1-#   select tuner band (? lists available bands)
  -f freq  tune given frequency (also unmutes)
  -c dev   use given device        [default: /dev/radio0]
  -s       scan
  -S       scan + write radio.fmmap
  -i       scan, write initial ~/.radio config file to
           stdout and quit
  -l 0/1	loopback digital audio from radio  [default: 1]
  -r dev   record/capture device for loopback  [default: auto]
  -p dev   playback device for loopback  [default: default]
  -L ms    latency for loopback in ms [default: 500]
  -q       quit.  Useful with other options to control the
           radio device without entering interactive mode,
           i.e. "radio -qf 91.4"

(c) 1998-2001 Gerd Knorr <kraxel@bytesex.org>
(c) 2012 Hans de Goede <hdegoede@redhat.com>
interface by Juli Merino <jmmv@mail.com>
channel scan by Gunther Mayer <Gunther.Mayer@t-online.de>
