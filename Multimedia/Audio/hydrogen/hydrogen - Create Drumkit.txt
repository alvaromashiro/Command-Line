From: https://opensource.com/article/17/11/how-create-hydrogen-drumkit-fun-and-profit

How to create a Hydrogen drumkit for fun and profit
---------------------------------------------------
Hydrogen's versatile, intuitive interface lets you become an instant rock star
by recording virtually any rhythm and building your own drumkit.

Drum machines are fun. They can make some amazing beats, and they tend to have
an easy interface.

The first drum machine I ever used was the Alesis HR-16. It had 49 16-bit
patches and an inbuilt sequencer. That it wasn't rack-mountable annoyed me, but
then again, it fit into a messenger bag, so I was able to take it to studio
sessions easily.

The HR-16's interface was one of the best. Its only display was a two-line LCD
screen, and all its functions had dedicated buttons on the front of the
machine. To build up a new drum line, you selected a pattern slot, entered
recording mode, and played in a beat either in real time or beat-by-beat. The
unit held up to 100 patterns, and you could string together patterns into files
comprising as many as 100 songs.

ui.jpg

HR-16 user interface
--------------------
^The HR-16 drumkit user interface is simple and intuitive.

Today, I use the Hydrogen software drum machine. The concept is basically the
same: Enter a rhythm into the active pattern, and build a song of patterns.
It's simple and intuitive, and it makes you feel like a rock 'n roller in no
time.

The great thing about Hydrogen is that it isn't limited to a single chip of 49
16-bit sounds. You can build your own drumkits with your own sounds. That means
that you can simply record anything you want to make a rhythm from and sequence
it within Hydrogen. Amazingly, it's not even that hard.

Anatomy of a kit
----------------
A Hydrogen drumkit is actually just .tar using .h2drumkit as an extension.

$ file ./mydrumkit.h2drumkit
mydrumkit.h2drumkit: POSIX tar archive (GNU)

Contained within the TAR archive are the sounds that you want to use as your
drumkit and an XML file describing the drumkit to Hydrogen. This looks
something like this:

<instrument><exclude></exclude></instrument><instrument><exclude><pre><code class="language-xml">    <instrument>

        <id>0</id>

        <name>Kick drum</name>

        <volume>1</volume>

        <isMuted>false</isMuted>

        <pan_L>1</pan_L>

        <pan_R>1</pan_R>

        <randomPitchFactor>0</randomPitchFactor>

        <filterActive>false</filterActive>

        <filterCutoff>1</filterCutoff>

        <filterResonance>0</filterResonance>

        <Attack>0</Attack>

        <Decay>0</Decay>

        <Sustain>1</Sustain>

        <Release>1000</Release>

        <exclude />

        <layer>

            <filename>35kick.flac</filename>

            <min>0</min>

            <max>1</max>

            <gain>1</gain>

            <pitch>0</pitch>

            </layer>

    </instrument>


Building a drumkit, therefore, is as simple as:

 1. Gather the sound files you want in your kit
 2. Generate an XML file describing your kit
 3. Create a tar file and load it into Hydrogen

| 1. Sounds
`----------
Your sound files can come from anywhere. You can record your own vocal
beatboxing, the drumset in your bedroom, or sounds of nature, or you can
pillage freesound.org and whatever else you can imagine.

If your sound files are not in the FLAC format, convert them to FLAC with an
audio converter like sox, ffmpeg, or Audacity.

With sox:

    $ sox foo.wav foo.flac

With ffmpeg, add the -vn flag to ensure no video is included in the conversion
process:

    $ ffmpeg -i foo.wav -vn foo.flac

And in Audacity, load the source sound file and use the File > Export audio
menu selection. In the Export audio window, select FLAC as your output format.

Arranging the sound files
-------------------------
Technically, your drumkit can have sound files in any order you want, or in
whatever order they end up being sorted by your terminal listing. However, a
long time ago some very smart music technicians came up with a specification
for how sounds should be organised in the world of synthesis. They call the
spec General MIDI, or GM for short.

The advantage of General MIDI is that a musician can compose a song or drum
sequence with one sound bank, and then swap sound banks (or "drumkits," in
Hydrogen's terminology) and still have essentially the same thing, just with
different sounds. Your bass drum is still a bass drum, your snare is still a
snare, and your cymbal is still a cymbal, but the sample behind those sounds
has been swapped. You can write one drum sequence and use it in an
electro-metal intro version as well as a delicate acoustic ballad version of
the same song.

You don't have to follow GM, but it's not a bad idea.

To keep my drumkit sounds organized, I name the sounds in each kit according to
GM specification.

    $ ls mydrumkit/

    35kick.flac

    36kick.flac

    37click.flac

    38snare.flac

    40snare.flac

    41tom_low.flac

    ...

If I don't have a sound that the GM has a slot for, I just skip that slot.

This method also avoids wild variations in naming schemes. There are no kick
drum BIG.flac and bass drum deep.flac files in my kits.

When a kit that I make manages to populate the GM spec completely, I give it
the full designation, just so users know that the drumkit is truly
GM-compatible.

| 2. Create a drumkit.xml file
`-----------------------------
People sometimes get irrationally scared of XML, presumably because it
seemingly has unlimited tags, with no clear definition of what tags are
required or even what they mean.

The drumkit.xml file in a Hydrogen drumkit is mostly repetitive and is simple
enough to generate by hand. All you really need is a text editor and the
willingness to copy and paste – a lot.

The header portion of your drumkit definition opens a drumkit_info element and
provides metadata about the kit:

    <drumkit_info>

        <name>

          mydrumkit

        </name>

        <author>

          Seth

        </author>

        <info>

          This is my first drumkit. It contains sounds released under

          the CC-0 license.

        </info>

        <license>

          CC-0

        </license>

        <instrumentList>

This is entered once, and only once, per kit, at the top of your file.

For each sound file, assuming one sound file represents one instrument, an
instrument block is required. The basic instrument block starts with this:

    <instrument>

        <id>0</id>

        <name>Kick drum</name>

        <volume>1</volume>

        <isMuted>false</isMuted>

        <pan_L>1</pan_L>

        <pan_R>1</pan_R>

        <randomPitchFactor>0</randomPitchFactor>

        <filterActive>false</filterActive>

        <filterCutoff>1</filterCutoff>

        <filterResonance>0</filterResonance>

        <Attack>0</Attack>

        <Decay>0</Decay>

        <Sustain>1</Sustain>

        <Release>1000</Release>

        <exclude />

Once you get the hang of creating drumkits, there are some neat tricks for
using multiple sound files for one instrument that you can explore. A cymbal,
for instance, has a unique sound quality when it's tapped compared to when it's
pounded with a drumstick, but it's still the same physical instrument.

For now, though, keep your instruments simple and use only one file per
instrument. Sound files are referenced in the instrument's layer block:

    <layer>

      <filename>35kick.flac</filename>

      <min>0</min>

      <max>1</max>

      <gain>1</gain>

      <pitch>0</pitch>

    </layer>

    </instrument>

Repeat that process for each sound file, and then close your instrumentList and
drumkit_info tags:

</instrumentList>

</drumkit_info>

That's all the XML you need to know!

| 3. Create a tar archive
`------------------------
The final step in this process is to wrap your sounds and XML in an archive.

If you haven't already done so, place your sound files and your drumkit.xml
file into a dedicated directory. Assume the directory is called mydrumkit
(although I do hope you come up with something more creative).

Run the tar command:

    $ tar cvf mydrumkit.tar mydrumkit

Then rename your TAR archive so Hydrogen recognises it:

    $ mv mydrumkit.tar mydrumkit.h2drumkit

You're done! That's all there is to it. You've just made your very own custom
kit.

Loading your kit
----------------
To use your drumkit in Hydrogen, you must import it.

To import a drumkit, open Hydrogen and click on the Instruments menu and select
Import Library:

menu-hydrogen-import.jpg

importing drumkit file in hydrogen

^Importing your drumkit into Hydrogen.

[menu-hyrdr]In the Sound Library Import window, click the Local file tab. Click
the Browse button on the right.

Select your drumkit file to load it. The import process takes only a second, so
don't be surprised if it seems to happen too quickly.

To use your kit, right-click on your drumkit in the Sound Library tab of the
Hydrogen window and select Load:

load.jpg

loading your drumkit file into hydrogen

^To use your drumkit, load it into the Hydrogen sound library.

[load]If you already had data entered into a Hydrogen pattern, your data will
still be there after switching kits. If you followed GM, your sequence should
sound basically the same, just played on a different drum kit!

Automation
----------
A few years ago, a friend and I created 99 Hydrogen drumkits over the course of
a few weeks. Organising more than 1,000 sound files was something we had to do
by hand (and ear), but the last thing we wanted to do after all that work was
generate 99 drumkit.xml files manually. Instead, I wrote a simple Python script
to do the repetitive stuff for us.

The script is called genhydro, and its sole purpose is to generate a
drumkit.xml file from a directory full of FLAC files, and create an archive of
the directory.

You can find the script over at GitLab. Feel free to use it if you don't want
to generate the XML by hand.

Benefiting from the hard work of others

Hydrogen has a strong and creative community. You can download drumkits from
other users using the Import Library window.

Additionally, you can download my 99 drumkits from Slackermedia.

Now go make some great beats!


About the author

image from https://openclipart.org/detail/196235/penguin-profile-medalion
Seth Kenlon - Seth Kenlon is an independent multimedia artist, free culture
advocate, and UNIX geek. He has worked in the film and computing industry,
often at the same time. He is one of the maintainers of the Slackware-based
multimedia production project, http://slackermedia.info
More about me
