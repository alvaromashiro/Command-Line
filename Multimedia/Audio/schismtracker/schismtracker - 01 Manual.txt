SCHISMTRACKER(1)                     General Commands Manual                     SCHISMTRACKER(1)

NAME
       schismtracker - tracked music editor based on Impulse Tracker

SYNOPSIS
       schismtracker [options] [directory] [file]

DESCRIPTION
       schismtracker  is  a  tracked  music module editor that aims to match the look and feel of
       Impulse Tracker as closely as possible. It can load most common tracker formats,  supports
       saving as IT and S3M, and can also export to WAV and AIFF.

OPTIONS
       -a, --audio-driver=DRIVER[:DEVICE]
              Audio device configuration. driver is the SDL driver to use, e.g.  alsa (ALSA), dma
              or dsp (OSS); device is the  name  of  the  device  itself,  for  example  hw:2  or
              /dev/dsp1.

       -v, --video-driver=DRIVER
              SDL  video driver, such as x11, dga, or fbcon. Note that this is different from the
              video driver setting within the program, and is unlikely to be useful.

       --video-yuvlayout=LAYOUT
              Specific YUV layout to use: YUY2, YV12, RGBA, etc.   This  is  probably  best  left
              alone under normal circumstances.

       --video-size=WIDTHxHEIGHT
              Set the size of the video display.

       --video-stretch=VALUE
              Fix the aspect ratio. (Probably does nothing!)

       --video-gl-path=PATH
              Specify path of OpenGL library.

       --video-depth=DEPTH
              Specify display depth, in bits.

       --video-fb-device=DEVICE
              Specify path to framebuffer. Typical value is /dev/fb0.

       --network, --no-network
              Enable/disable networking (on by default). Used for MIDI over IP.

       --classic, --no-classic
              Start Schism Tracker in "classic" mode, or don't. This is mostly cosmetic, although
              it does change the program's behavior slightly in a few places.

       --display=DISPLAY
              X11 display to use.

       -f, -F, --fullscreen, --no-fullscreen
              Enable/disable fullscreen mode at startup.

       -p, -P, --play, --no-play
              Start playing after loading song on command line.

       --diskwrite=FILENAME
              Render output to a file, and then exit. WAV or AIFF writer is  auto-selected  based
              on  file  extension.  Include  %c  somewhere  in  the  name  to  write each channel
              separately. This is meaningless if no initial filename is given.

       --font-editor, --no-font-editor
              Run the font editor (itf). This can also be accessed by pressing Shift-F12.

       --hooks, --no-hooks
              Run hooks. Enabled by default.

       --debug=FLAGS
              Enable some debugging flags (separated by commas).  You probably don't need this.

       --version
              Display version information and build date.

       -h, --help
              Print a summary of available options.

       A filename supplied on the command line will  be  loaded  at  startup.   Additionally,  if
       either a file or directory name is given, the default module, sample, and instrument paths
       will be set accordingly.

USAGE
       A detailed discussion of how to use Schism  Tracker  is  far  beyond  the  scope  of  this
       document,  but  here is a very brief rundown of the basics.  Context-sensitive help can be
       accessed at any time while running the program by pressing F1.

       The F3 key will bring you to the sample list. Press enter here to  open  a  file  browser,
       navigate  in  the list using the up/down arrow keys, and hit enter again to load a sample.
       You will likely want to get some samples to work with. You can also  "rip"  from  existing
       modules; see for example http://www.modarchive.org/ for a very large selection of modules.
       (Keep in mind, however, that some authors don't appreciate having their samples ripped!)

       Now that you've loaded a sample, press F2 to get to the pattern editor. This is where  the
       majority  of  the composition takes place. In short, the song is laid out vertically, with
       each row representing 1/16 note; to play multiple notes simultaneously, they are placed in
       different  channels.   The  four  sub-columns of each channel are the note, sample number,
       volume, and effect. A list of effects is available in the pattern editor help, but you can
       safely  ignore  that  column  for  now.  Assuming  a US keymap, notes are entered with the
       keyboard as follows:
           (Note)        C# D#    F# G# A#    C# D#    F# G# A#    C# D#
                      | | || | | | || || | | | || | | | || || | | | || | |
                      | | || | | | || || | | | || | | | || || | | | || | |
           (What you  | |S||D| | |G||H||J| | |2||3| | |5||6||7| | |9||0| |
            type)     | '-''-' | '-''-''-' | '-''-' | '-''-''-' | '-''-' |
                      | Z| X| C| V| B| N| M| Q| W| E| R| T| Y| U| I| O| P|
                      '--'--'--'--'--'--'--'--'--'--'--'--'--'--'--'--'--'
           (Note)       C  D  E  F  G  A  B  C  D  E  F  G  A  B  C  D  E
                       (Octave 0)           (Octave 1)           (Octave 2)

       The "/" and "*" keys on the numeric keypad change  octaves,  and  the  current  octave  is
       displayed near the top of the screen. Try typing "qwerty" into the pattern - it will enter
       an ascending note sequence, and you'll hear the notes  as  they're  entered.  (of  course,
       assuming you loaded a sample!) Press F6 to play your pattern, and F8 to stop.

       Other  important  keys  for  the pattern editor include Ins/Del to shift notes up and down
       within a channel, Shift-Arrows to mark a block, Alt-C/Alt-P to copy and paste,  and  Alt-U
       to  clear  the mark. There are well over a hundred key bindings for the pattern editor; it
       is well worth the effort to learn them all eventually.

       Now that you have something in your pattern, you'll need to set up an orderlist. Press F11
       to  switch  to the orderlist page, and type 0 to add the pattern you created. Now press F5
       to start playing. The song will begin at the first order, look up the pattern  number  and
       play that pattern, then advance to the next order, and so forth.

       Of  course,  having only one pattern isn't all that interesting, so go back to the pattern
       editor and press the + key to change to the next pattern. Now you can write  another  four
       bars  of  music  and  add the new pattern to the orderlist, and the next time you play the
       song, your two patterns will play in sequence.

       You may wish to give your song a title; press F12 and type a name in the box at  the  top.
       You  can  also  adjust the tempo and a number of other settings on this page, but for now,
       most of them are fine at their default values.

       To save your new song, press F10, type a filename, and hit enter. You can  load  it  again
       later by pressing F9.

       This  tutorial has deliberately omitted the instrument editor (on F4), for the purposes of
       brevity and simplicity. You may want to experiment with it once you have a  feel  for  how
       the program works. (Select "instruments" on F12 to enable instrument mode.)

HISTORY
       Storlek  began  studying  Impulse  Tracker's  design in 2002, noting subtle details of the
       design and implementation. Posts on the Modplug forums  about  rewriting  Impulse  Tracker
       were met with ridicule and mockery. "It can't be done," they said.

       Schism Tracker v0.031a was released in July 2003, though very little worked at that point,
       and it was more of a player with primitive editing capabilities.  File  saving  was  hard-
       coded  to  write  to  "test.it"  in  the current directory, and there was no way to load a
       sample.

       The first version that was more or less usable was 0.15a, from December 2004.

       From 2005 through 2009, Mrs. Brisby did most of the development, and implemented  tons  of
       features, including MIDI support, mouse support, and disk writing.

       Storlek  "took over" development again in 2009, and incrementally rewrote much of the code
       through 2015.

       In 2016, Schism Tracker was moved to GitHub under shared maintainership. Since then,  many
       people have contributed improvements and bug fixes to the codebase.

FILES
       ~/.schism/config
              Program  settings,  stored in an INI-style format. Most options are accessible from
              within Schism Tracker's interface, but there are a few "hidden" options.

       ~/.schism/startup-hook, ~/.schism/exit-hook, ~/.schism/diskwriter-hook
              Optional files to execute upon certain events. (Must be executable)

       ~/.schism/fonts/
              font.cfg, and any .itf files found in this directory, are  displayed  in  the  file
              browser of the font editor.

   Supported file formats
       MOD    Amiga modules (with some obscure variants such as FLT8)

       669    Composer 669 / Unis669

       MTM    MultiTracker

       S3M    Scream Tracker 3 (including Adlib support)

       XM     Fast Tracker 2

       IT     Impulse Tracker (including old instrument format)

       MDL    Digitrakker 3

       IMF    Imago Orpheus

       OKT    Amiga Oktalyzer

       SFX    Sound FX

       MUS    Doom engine (percussion missing)

       FAR    Farandole Composer

       STM    Scream Tracker 2 (partial functionality)

       ULT    UltraTracker (partial functionality)

       S3I    Scream Tracker 3 sample

       WAV    Microsoft WAV audio

       AIFF   Audio IFF (Apple)

       8SVX   Amiga 8SVX sample

       ITS    Impulse Tracker sample

       AU     Sun/NeXT Audio

       RAW    Headerless sample data

       PAT    Gravis UltraSound patch

       XI     Fast Tracker 2 instrument

       ITI    Impulse Tracker instrument

       Schism  Tracker  is  able  to  save modules in IT and S3M format, sample data as ITS, S3I,
       AIFF, AU, WAV, and RAW, and instruments as ITI. Additionally, it can  render  to  WAV  and
       AIFF (optionally writing each channel to a separate file), and can export MID files.

AUTHORS
       Schism  Tracker  was  written by Storlek and Mrs. Brisby, with player code from Modplug by
       Olivier Lapicque. Based on Impulse Tracker by Jeffrey Lim.

       Additional code and data have been contributed by many others; refer to the  file  AUTHORS
       in the source distribution for a more complete list.

       The  keyboard  diagram  in  this  manual  page  was  adapted  from  the  one  used  in the
       documentation for Impulse Tracker, which in turn borrowed it from Scream Tracker 3.

COPYRIGHT
       Copyright  ©  2003-2018  Storlek,  Mrs.  Brisby  et  al.  Licensed  under  the   GNU   GPL
       <http://gnu.org/licenses/gpl.html>.  This  is  free  software:  you are free to change and
       redistribute it. There is NO WARRANTY, to the extent permitted by law.

BUGS
       They almost certainly exist. Post on https://github.com/schismtracker/schismtracker/issues
       if you find one.  Agitha shares her happiness with benefactors of the insect kingdom.

INTERNETS
       http://schismtracker.org/ - main website
       #schismtracker on EsperNet - IRC channel

SEE ALSO
       chibitracker(1), milkytracker(1), protracker(1), renoise(1), ocp(1), xmp(1)

                                           Jan 18, 2018                          SCHISMTRACKER(1)
