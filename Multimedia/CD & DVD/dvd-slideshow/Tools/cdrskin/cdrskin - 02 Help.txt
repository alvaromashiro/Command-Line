cdrskin 1.4.6 : limited cdrecord compatibility wrapper for libburn

Usage: cdrskin [options|source_addresses]
Burns preformatted data to CD or DVD via libburn.
For the cdrecord compatible options which control the work of
blanking and burning see output of option -help rather than --help.
Non-cdrecord options:
 --abort_handler    do not leave the drive in busy state
 --adjust_speed_to_drive  set only speeds offered by drive and media
 --allow_emulated_drives  dev=stdio:<path> on file objects
 --allow_setuid     disable setuid warning (setuid is insecure !)
 --allow_untested_media   enable implemented untested media types
 --any_track        allow source_addresses to match '^-.' or '='
 assert_write_lba=<lba>  abort if not next write address == lba
 cd_start_tno=<number>  set number of first track in CD SAO session
 --cdtext_dummy     show CD-TEXT pack array instead of writing CD
 cdtext_to_textfile=<path>  extract CD-TEXT from CD to disk file
 cdtext_to_v07t=<path|"-">  report CD-TEXT from CD to file or stdout
 --cdtext_verbose   show CD-TEXT pack array before writing CD
 direct_write_amount=<size>  write random access to media like DVD+RW
 --demand_a_drive   exit !=0 on bus scans with empty result
 --devices          list accessible devices (tells /dev/...)
 --device_links     list accessible devices by (udev) links
 dev_translation=<sep><from><sep><to>   set input address alias
                    e.g.: dev_translation=+ATA:1,0,0+/dev/sg1
 --drive_abort_on_busy  abort process if busy drive is found
                    (might be triggered by a busy hard disk)
 --drive_blocking   try to wait for busy drive to become free
                    (might be stalled by a busy hard disk)
 --drive_f_setlk    obtain exclusive lock via fcntl.
 --drive_not_exclusive  combined not_o_excl and not_f_setlk.
 --drive_not_f_setlk  do not obtain exclusive lock via fcntl.
 --drive_not_o_excl   do not ask kernel to prevent opening
                    busy drives. Effect is kernel dependend.
 drive_scsi_dev_family=<sr|scd|sg|default> select Linux device
                    file family to be used for (pseudo-)SCSI.
 --drive_scsi_exclusive  try to exclusively reserve device files
                    /dev/srN, /dev/scdM, /dev/stK of drive.
 dvd_obs="default"|number
                    set number of bytes per DVD/BD write: 32k or 64k
 extract_audio_to=<dir>
                    Copy all tracks of an audio CD as separate
                    .WAV files <dir>/track<NN>.wav to hard disk
 extract_basename=<name>
                    Compose track files as <dir>/<name><NN>.wav
 --extract_dap      Enable flaw obscuring mechanisms for audio reading
 extract_tracks=<number[,number[,...]]>
                    Restrict extract_audio_to= to list of tracks.
 fallback_program=<cmd>  use external program for exotic CD jobs.
 --fifo_disable     disable fifo despite any fs=...
 --fifo_per_track   use a separate fifo for each track
 fifo_start_at=<number> do not wait for full fifo but start burning
                    as soon as the given number of bytes is read
 --fill_up_media    cause the last track to have maximum size
 --four_channel     indicate that audio tracks have 4 channels
 grab_drive_and_wait=<num>  grab drive, wait given number of
                    seconds, release drive, and do normal work
 --grow_overwriteable_iso  emulate multi-session on media like DVD+RW
 --ignore_signals   try to ignore any signals rather than to abort
 input_sheet_v07t=<path>  read a Sony CD-TEXT definition file
 --list_formats     list format descriptors for loaded media.
 --list_ignored_options list all ignored cdrecord options.
 --list_speeds      list speed descriptors for loaded media.
 --long_toc         print overview of media content
 modesty_on_drive=<options> no writing into full drive buffer
 --no_abort_handler  exit even if the drive is in busy state
 --no_blank_appendable  refuse to blank appendable CD-RW
 --no_convert_fs_adr  only literal translations of dev=
 --no_load          do not try to load the drive tray
 --no_rc            as first argument: do not read startup files
 --obs_pad          pad DVD DAO to full 16 or 32 blocks
 --old_pseudo_scsi_adr  use and report literal Bus,Target,Lun
                    rather than real SCSI and pseudo ATA.
 --pacifier_with_newline  do not overwrite pacifier line by next one.
 --prodvd_cli_compatible  react on some DVD types more like
                    cdrecord-ProDVD with blank= and -multi
 sao_postgap="off"|number
                    defines whether the next track will get a
                    post-gap and how many sectors it shall have
 sao_pregap="off"|number
                    defines whether the next track will get a
                    pre-gap and how many sectors it shall have
 --single_track     accept only last argument as source_address
 stream_recording="on"|"off"|number
                    "on" requests to prefer speed over write
                    error management. A number prevents this with
                    byte addresses below that number.
 stdio_sync="default"|"off"|number
                    set number of bytes after which to force output
                    to drives with prefix "stdio:".
 tao_to_sao_tsize=<num>  use num as fixed track size if in a
                    non-TAO mode track data are read from "-"
                    and no tsize= was specified.
 --tell_media_space  print maximum number of writeable data blocks
 textfile_to_v07t=file_path
                    print CD-TEXT file in Sony format and exit.
 --two_channel      indicate that audio tracks have 2 channels
 use_immed_bit=on|off|default  real control over SCSI Immed bit
 write_start_address=<num>  write to given byte address (DVD+RW)
 --xa1-ignore       with -xa1 do not strip 8 byte headers
 -xamix             DO NOT USE (Dummy announcement to make K3B use -xa)
Preconfigured arguments are read from the following startup files
if they exist and are readable. The sequence is as listed here:
  /etc/default/cdrskin         /etc/opt/cdrskin/rc
  /etc/cdrskin/cdrskin.conf    $HOME/.cdrskinrc
Each file line is a single argument. No whitespace.
By default any argument that does not match grep '^-.' or '=' is
used as track source. If it is "-" then stdin is used.
cdrskin  : http://scdbackup.sourceforge.net/cdrskin_eng.html
           mailto:scdbackup@gmx.net  (Thomas Schmitt)
libburn  : http://libburnia-project.org
cdrecord : ftp://ftp.berlios.de/pub/cdrecord/
My respect to the authors of cdrecord and libburn.
scdbackup: http://scdbackup.sourceforge.net/main_eng.html

