[dvd-menu]            dvd-menu 0.8.6-1
[dvd-menu]            Licensed under the GNU GPL
[dvd-menu]            Copyright 2003-2015 by Scott Dylewski
[dvd-menu]            
dvd-menu is part of dvd-slideshow 0.8.6-1
dvd-menu
http://freshmeat.net/dvdslideshow/
Copyright 2003 Scott Dylewski <scott at dylewski.com>
 
Usage: 
  dvd-menu -t <titleset1> ... -t <titlesetN>
  -f <xmlfile1> ... -f <xmlfileN> [-o output_directory]
  [-n "menu title"] [-e <embed image>] [-L]
  [-b <background jpeg>] [ -a <menu audio file> ] [-p]
  [-c] [-D] [-W] [-mp2] [-iso] [-w] [-theme <themefile>]  [-nomenu] 

Description: 
	Creates a simple vmgm menu with buttons that
	link to the mpeg files in the specified 
	dvdauthor-compatible xml files. 
	
Options: 
 -t <titleset1> -t <titleset2> ... -t <titlesetN>
	Name of each title.  Each string will be used for 
	the button menu corresponding to each xmlfile.
	This sets the order of the menu buttons!

 -f <xmlfile1> <xmlfile2> ...
	Name of each titleset xml file.  You can use
	dvd_slideshow to generate one for each slideshow
	or you can create your own for a simple mpeg2
	file.

 [-o <output directory>]
	Directory where the output dvd file system resides

 [-n "dvd_title"]
        This string will be placed on the top of the
        menu page.

 [-b <background jpeg>]
	Image to put in the background of the menu.

 [-e <embedded image>]
	Embed a jpeg image in the middle of the screen.
	The image shows up as a thumnail to the left of the
	menu buttons.
	Only one image is allowed.

 [-a <audiofile>]
	Audio file to use in the background of the
	menu.  The full audio file will keep looping.
	Supports mp3, ogg, or wav.

 [-mp2]
        Use MP2 audio by default.  AC3 seems to be more stable
	in hardware players, but requires ffmpeg.

 [-p]
        Use PAL format instead of NTSC (untested initial support)

 [-c]
        Use continuous method.  Each video plays one after another.

 [-W]
        Do not generate 50% white background behind menu text.

 [-w]
        Use widescreen mode (16:9) instead of default 4:3 aspect ratio.

 [-L]
        Use low quality mode for compatibility with the same option
	in dvd-slideshow.  This renders half-resolution menus.

 [-D]
        Do not call dvdauthor at the end.  Useful when you want
	to edit your own xml file and pass it to dvdauthor by hand.

 [-theme themename]
	Use the given theme when setting variables/colors/etc.  Themes are
	installed in /opt/dvd-slideshow/themes or in a local directory
	~/.dvd-slideshow/themes
 
 [-vcd]|[-svcd]  [ALPHA!]
        Create vcd .cue and .bin files (requires vcdimager)

 [-iso]
        Create iso image with genisoimage when finished.

 [-nomenu]
        Do not create a menu.  Useful for just finishing the 
	dvdauthor xml file from one video that you want to just
	start playing when the user puts it into a dvd player.

 [-nocleanup]
        Save files in temp directory for debugging.

 -h or -help 
   Prints this help. 

 -v or -version 
   Prints this help. 

Requires:
	dvdauthor 0.6.11
	sox >= 14.0.0
	mjpegtools  (mp2enc, mpeg2enc, ppmtoy4m)
	ImageMagick (convert)
	ffmpeg

Variables:
	dvd-menu will read the following variables out of your ~/.dvd-slideshow/dvd-slideshowrc file.
	The default values are shown

	menu_bg_color=steelblue  # imagemagick colors or a hex #rrggbb value.
	menu_title_font_size=40
	menu_title_white_height=125
	menu_title_location_x=80
	menu_title_location_y=50
	menu_panel_height=28  # height of each button.  Default is slightly bigger than font size
	menu_panel_width=400  # goes all the way to the right of the screen
	menu_panel_location_x=420
	menu_panel_location_y=175  # distance of the top of the first menu item to the top of the screen
	menu_button_font_size=21
	menu_title_justify=left
	mpeg_encodr=ffmpeg   # or mpeg2enc
