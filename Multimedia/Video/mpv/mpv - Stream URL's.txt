######################
# mpv - Stream URL's #
######################

MPV can be used to stream URL's. This is because mpv uses 'youtube-dl'
to parse them by default.

    mpv https://...
    
See https://rg3.github.io/youtube-dl/supportedsites.html for list of
youtube-dl supported sites.

Supported Protocols:

 cdda://
 rtmp://
 rtsp://
 http://
 https://
 mms://
 mmst://
 mmsh://
 mmshttp://
 rtp://
 httpproxy://
 hls://
 rtmpe://
 rtmps://
 rtmpt://
 rtmpte://
 rtmpts://
 srtp://
 data://
 lavf://
 ffmpeg://
 udp://
 ftp://
 tcp://
 tls://
 unix://
 sftp://
 md5://
 concat://
 avdevice://
 av://
 smb://
 file://
 dvdread://
 file://
 dvd://
 dvdnav://
 file://
 bd://
 br://
 bluray://
 bdnav://
 brnav://
 bluraynav://
 memory://
 hex://
 null://
 mf://
 edl://
 rar://
 file://
 fd://
 fdclose://
